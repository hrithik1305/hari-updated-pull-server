from django.conf import settings
from decouple import config

# Email 
EMAIL_USE_TLS = True
EMAIL_HOST_USER = config('EMAIL_HOST_USER', default='info@pickrr.com')
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_PASSWORD = config('EMAIL_HOST_PASSWORD', default='abc123')
EMAIL_PORT = 587
SEND_GRID_KEY = config('SEND_GRID_KEY', default='')
DEVELOPER_EMAILS = ['ruhan@pickrr.com'] if settings.DEBUG else ['laxman.@pickrr.com', 'ruhan@pickrr.com']


# SMS
KALEYRA_SID = config('KALEYRA_SID', default='')
KALEYRA_API_KEY = config('KALEYRA_API_KEY', default='')
KALEYRA_BASE_URL = 'https://api.kaleyra.io'
KALEYRA_SMS_SENDER_ID = 'IPICKR'
WEBHOOK_KALEYRA = 'https://9d22-203-110-85-74.ngrok.io/notification/webhook/' if settings.DEBUG else 'https://pull.pickrr.com/notification/webhook/'


USER_NOTIFICATION_COLLECTION_NAME = 'user_notification'
NOTIFICATION_LOGS_COLLECTION_NAME = 'notifiaction_logs'