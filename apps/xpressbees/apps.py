from django.apps import AppConfig


class XpressbeesConfig(AppConfig):
    name = "apps.xpressbees"
