from django.conf import settings
import json
from datetime import timedelta, datetime, timezone
from dateutil.parser import parse
import pytz
from decouple import config

from .preparator import PrepareTrackingEventsData
from .constants import (
    EMAIL_TEMPLATES, STATUS_SUBJECT_MAPS,
    WP_TEMPLATE_NAME_MAPS
)
from .helpers import (
    EmailHelpers, get_cleaned_phone_number, 
    NotificationHelpers, exception_handler_emailer,
    ComplexEncoder
)
from apps.common.utils import MakeAPICall
from .dao import NotificationDao
from apps.common.mongo_utils import (
    get_db_collection, batch_document_iterator,
    batch_documents_with_agg
)
from apps.common.redis import (
    check_redis_connection, get_from_cache,
    update_the_cache
)
from pickrr_tracker.loggers import notification_logger

class NotificationServices:
        
    @staticmethod
    def update_wp_webhooks_data(request):
        notification_logger.info({
            "type": "Whatsapp Webhook Request Data",
            "obj_data": json.dumps(request, cls=ComplexEncoder),
        })
        try:
            id = request.query_params.get('id') or request.query_params.get('ids')
            status = request.query_params['status']
            message = request.query_params['message']
            
            filters = {
                "message_id": id
            }
            
            notification_log = NotificationDao.get_notification_logs(filters)
            
            if notification_log:
                updated_obj = {}
                updated_obj['delivery_status'] = status.capitalize()
                updated_obj['remarks'] = message.title()
                NotificationDao.update_logs(filters, updated_obj)
        except Exception as error:
            notification_logger.info({
                "err": str(error),
                "type": "Whatsapp Webhook Error",
                "obj_data": json.dumps(request, cls=ComplexEncoder),
            })
    
    @staticmethod
    def update_sms_webhooks_data(request):
        notification_logger.info({
            "type": "SMS Webhook Request Data",
            "obj_data": json.dumps(request.body, cls=ComplexEncoder),
        })
        try:
            req_body = json.loads(request.body)
            
            message_id = req_body['message_id']
            status = req_body['status'].capitalize()
            description = req_body['description'].title()
            
            filters = {
                "message_id": message_id
            }

            notification_log = NotificationDao.get_notification_logs(filters)
            
            if notification_log:
                updated_obj = {}
                updated_obj['delivery_status'] = status
                updated_obj['remarks'] = description
                NotificationDao.update_logs(filters, updated_obj)
        
        except Exception as error:
            notification_logger.info({
                "err": str(error),
                "type": "SMS Webhook Error",
                "obj_data": json.dumps(request.body, cls=ComplexEncoder),
            })
    

class NotificationSenders(NotificationHelpers):
    from .tasks import send_email
    
    URL = f"{settings.KALEYRA_BASE_URL}/v1/{settings.KALEYRA_SID}/messages"
    HEADERS = {
        'content-type': 'application/json',
        'api-key': settings.KALEYRA_API_KEY
    }
    


    @classmethod
    def email(cls, obj):
        try:
            if not obj.get('to_email'):
                return False
            
            current_status = obj['current_status']
            
            if not EMAIL_TEMPLATES.get(current_status):
                return False
                        
            body = EmailHelpers.generate_email_body_template(template=EMAIL_TEMPLATES[current_status], context=obj)

            kwargs = {
                "email_list": [obj['to_email']],
                "subject": STATUS_SUBJECT_MAPS[current_status],
                "body": body
            }
            
            cls.send_email.delay(**kwargs)
            
            return True
        except Exception as error:
            notification_logger.info({
                "err": str(error),
                "type": "Email Error",
                "obj_data": json.dumps(obj, cls=ComplexEncoder),
            })
            return False


    @classmethod
    def sms(cls, obj):
        if not obj.get('to_number'):
            return False
        
        event_func = cls.get_event_template_func(obj['current_status'], "SMS")
        
        if not event_func:
            return False
        
        payload = {
            "to": get_cleaned_phone_number(obj['to_number']),
            "body": event_func(obj),
            "sender": settings.KALEYRA_SMS_SENDER_ID,
            "type": "TXN",
            "callback": {
                "url": settings.WEBHOOK_KALEYRA,
                "method": "POST",
                "header": {
                    "x-api-key": settings.KALEYRA_API_KEY,
                    "x-randoem-id": "232y2uey"
                },
                "retry":{
                    "count":"3",
                    "interval":["10", "10","10"]
                }
            }
        }
        api_call = MakeAPICall(cls.URL, payload=payload, headers=cls.HEADERS)
        response = api_call.post(obj, type="SMS Sending Error")
                
        return response
        

    @classmethod
    def whatsapp(cls, obj):
        if not obj.get('to_number'):
            return False
        
        event_func = cls.get_event_template_func(obj['current_status'], "WP")

        if not event_func:
            return False
        
        payload = {
            "to": get_cleaned_phone_number(obj['to_number']),
            "channel": "whatsapp",
            "type": "template",
            "template_name": WP_TEMPLATE_NAME_MAPS.get(obj['current_status']) or '',
            "params": event_func(obj),
            "from": "919870491002",
            "callback_url": settings.WEBHOOK_KALEYRA
        }
        api_call = MakeAPICall(cls.URL, payload=payload, headers=cls.HEADERS)
        response = api_call.post(obj, type="WP Sending Error")
        
        return response
  

class UpdaterNotificationDBData:
    
    @staticmethod
    def create_logs(obj, channel_type):
        payload = {
            "email": obj['email'],
            "tracking_id": obj['tracking_id'],
            "order_id": obj['order_id'],
            "contact_number": obj['to_number'],
            "notification_channel": channel_type,
            "event_type": obj['current_status'],
            "is_active": True
        }
        logs_id = NotificationDao.create_logs(payload)
        return logs_id
    
    @staticmethod
    def create_email_logs(obj, channel_type):
        payload = {
            "email": obj['email'],
            "tracking_id": obj['tracking_id'],
            "order_id": obj['order_id'],
            "contact_number": obj['to_email'],
            "notification_channel": channel_type,
            "event_type": obj['current_status'],
            "attempted_at": datetime.utcnow(),
            "delivery_status": "Sent",
            "consumed_coins": 0,
            "is_active": True
        }
        NotificationDao.create_logs(payload)
    
    @staticmethod
    def update_logs(response, obj, logs_id, channel_type):
        
        response_data = response.get('data') or []
        response_error = response.get('error') # {}
        
        message_id = response_data[0]['message_id'] if response_data else ''
        delivery_status = 'Error' if response_error else 'Sent'
        consumed_coins = obj['channel_charge_data'][channel_type]
        
        updated_dict = {
            "kwargs": {}
        }
        
        updated_dict['message_id'] = message_id
        updated_dict['delivery_status'] = delivery_status
        updated_dict['consumed_coins'] = consumed_coins
        updated_dict['attempted_at'] = datetime.utcnow()
        
        if response_error:
            updated_dict['kwargs'][channel_type] = response_error
        filter = {
            "_id": logs_id
        }
        NotificationDao.update_logs(filter, updated_dict)

      
    @staticmethod
    def deduct_coins(obj, successfully_sent_channel_msg):
        
        channel_charge_data = obj['channel_charge_data']        
        notification_user = obj['notification_user']        
        total_coins = notification_user['total_coins']
        chagred_sms_coins = channel_charge_data.get('sms') if successfully_sent_channel_msg.get('sms') else 0
        chagred_wp_coins = channel_charge_data.get('wp') if successfully_sent_channel_msg.get('wp') else 0
        available_coins = float(total_coins) - (float(chagred_sms_coins) + float(chagred_wp_coins))
        
        filters = {
            "_id": notification_user.get('_id'),
        }
        updated_dict = {}
        updated_dict['total_coins'] = float(round(available_coins, 2))
        
        NotificationDao.update_user_data(filters, updated_dict)

      
class EventNotifications(NotificationSenders, UpdaterNotificationDBData):
    
    @classmethod
    def get_channel_func(cls, channel_key):
        channel_func = {
            "sms": cls.sms,
            "wp": cls.whatsapp,
            "email": cls.email
        }
        func = channel_func.get(channel_key, None)
        return func

  
    @classmethod
    def call_evnt_channel_func(cls, obj):
        channel_list = obj['channel_list']
        
        successfully_sent_channel_msg = {}
        for channel in channel_list:
            channel_func = cls.get_channel_func(channel)

            if channel_func:
                if channel == 'email': 
                    response = channel_func(obj)
                    if response:
                        cls.create_email_logs(obj, channel)
                elif obj['has_balance']:
                    logs_id =  cls.create_logs(obj, channel)
                    response = channel_func(obj)
                    if response:
                        successfully_sent_channel_msg[channel] = True
                        cls.update_logs(response, obj, logs_id, channel)

        if obj['has_balance']:
            cls.deduct_coins(obj, successfully_sent_channel_msg)


    @classmethod
    @exception_handler_emailer("Order Placed Event Error")
    def order_placed(cls, obj):
        cls.call_evnt_channel_func(obj)
    

    @classmethod
    @exception_handler_emailer("Order Dispatched Event Error")
    def order_dispathced(cls, obj):
        cls.call_evnt_channel_func(obj)
        
    
    @classmethod
    @exception_handler_emailer("Out for delivery Event Error")
    def out_for_delivery(cls, obj):
        
        # Check for early delivery
        if obj['edd_stamp'] != 'Soon' and parse(obj['edd_stamp']) > datetime.now():
            obj['current_status'] = 'OOE'

        cls.call_evnt_channel_func(obj)
    

    @classmethod
    @exception_handler_emailer("Delivered Event Error")
    def delivered(cls, obj):
        cls.call_evnt_channel_func(obj)
     
   
    @classmethod
    @exception_handler_emailer("Order In Transit Error")
    def order_in_transit(cls, obj):
        cls.call_evnt_channel_func(obj)
   

    @classmethod
    @exception_handler_emailer("NDR Event Error")
    def ndr(cls, obj):
        cls.call_evnt_channel_func(obj)


    @classmethod
    @exception_handler_emailer("Order Cancel Event Error")
    def order_cancel(cls, obj):
        cls.call_evnt_channel_func(obj)


    @classmethod
    @exception_handler_emailer("Delay Delivery Event Error")
    def delivery_delay(cls, obj):
        cls.call_evnt_channel_func(obj)


    @classmethod
    @exception_handler_emailer("Pickedup Dealy Event Error")
    def picked_up_delayed(cls, obj):
        cls.call_evnt_channel_func(obj)
      

def get_event_function(key):
    event_func = {
        "OP": EventNotifications.order_placed,
        "PP": EventNotifications.order_dispathced,
        "OO": EventNotifications.out_for_delivery,
        "DL": EventNotifications.delivered,
        "NDR": EventNotifications.ndr,
        "OT": EventNotifications.order_in_transit,
        "OC": EventNotifications.order_cancel,
        "DD": EventNotifications.delivery_delay,
        "PUD": EventNotifications.picked_up_delayed,
    }
    func = event_func.get(key, None)
    return func

def get_event_obj_data(tracking_obj, email, notification_user, is_dd=False):
    event_obj = PrepareTrackingEventsData.prepare_events_data(tracking_obj)
    if is_dd:
        event_obj['current_status'] = 'DD'
    event_obj['email'] = email
    
    result = NotificationHelpers.get_channels_charge(event_obj, notification_user)
    
    if not result:
        return
    
    channel_charge_data, notification_logs_count = result
    
    if event_obj['current_status'] == 'OT' and NotificationHelpers.check_notification_for_ot(notification_logs_count):
        return []
    
    has_balance = NotificationHelpers.balance_check(channel_charge_data, notification_user)
    channel_list = NotificationHelpers.get_event_channels_list(event_obj['current_status'], notification_user)
    
    event_obj['channel_list'] = channel_list
    event_obj['channel_charge_data'] = channel_charge_data
    event_obj['notification_user'] = notification_user
    event_obj['has_balance'] = has_balance
    event_obj['tracking_url'] = NotificationHelpers.get_tracking_url(event_obj)
    
    result = [channel_list, event_obj]
        
    return result
            

@exception_handler_emailer("Send Notification Error")
def send_notification(tracking_obj):

    email = tracking_obj.get('user_email')
    if not email:
        return
    
    if not tracking_obj.get('client_order_id'):
        return

    filters = {
        "email": email,
        "is_active": True
    }
    notification_user = NotificationDao.get_notification_user(filters)

    if not notification_user:
        return False
    
    event_obj_data =  get_event_obj_data(tracking_obj, email, notification_user)
    if not event_obj_data:
        return False
    
    channel_list, event_obj = event_obj_data
    event_obj['current_status'] = tracking_obj['status'].get('current_status_type')
    
    event_func = get_event_function(event_obj['current_status'])
    
    if event_func and channel_list:
        event_func(event_obj)
        

def check_last_5_awb(awb):
    awbs = json.loads(get_from_cache('awbs')) if get_from_cache('awbs') else []
    
    if len(awbs) > 5:
        return False
    
    if len(awbs) == 5 and awb not in awbs:
        return False
    
    if len(awbs) != 5:
        awbs.append(awb)
        update_the_cache('awbs', json.dumps(awbs))
    
    return True
    

def prepare_tracking_events_data_and_send_notification(pickrr_response):
    if not check_redis_connection():
        return
    
    tracking_id = pickrr_response["awb"]
    track_arr = pickrr_response["tracking_array"]
    
    if not track_arr:
        return
    
    if not check_last_5_awb(tracking_id):
        return 
        
    latest_event = track_arr[0]
    
    current_status = {}
    
    current_status['current_status_type'] = latest_event.get("scan_type", "")
    current_status['current_status_time'] = latest_event.get("scan_datetime", "")
    current_status['current_status_body'] = latest_event.get("scan_status", "")
    current_status['current_status_location'] = latest_event.get("scan_location", "")    
    
    track_info_col = get_db_collection(config("TRACKING_INFO_COLLECTION_NAME"))
    
    tracking_redis_key = f"com_{tracking_id}"
    cache_data = get_from_cache(tracking_redis_key)
            
    if check_redis_connection() and cache_data:
        res = json.loads(cache_data)
        res['status'] = current_status
        send_notification(res)
        return
      
    res = track_info_col.aggregate([
            {
                '$match': {
                    'tracking_id': tracking_id
                }
            }, {
                '$project': {
                    'info': {
                        'from_state': 1, 
                        'invoice_value': 1, 
                        'from_name': 1, 
                        'to_address': 1, 
                        'to_state': 1, 
                        'courier_name': 1, 
                        'to_pincode': 1, 
                        'to_email': 1, 
                        'from_email': 1, 
                        'to_city': 1, 
                        'to_name': 1, 
                        'from_phone_number': 1, 
                        'from_city': 1, 
                        'to_phone_number': 1, 
                        'from_pincode': 1, 
                        'from_address': 1
                    }, 
                    'status': 1, 
                    'courier_parent_name': 1, 
                    'courier_used': 1, 
                    'company_name': 1, 
                    'edd_stamp': 1, 
                    'is_cod': 1, 
                    'item_list': 1, 
                    'tracking_id': 1, 
                    'client_order_id': 1, 
                    'pickrr_order_id': 1, 
                    'courier_tracking_id': 1, 
                    'user_email': 1, 
                    'product_name': 1
                }
            }
    ])
    res = list(res)

    if not res:
        return
    res = res[0]
    res['status'] = current_status
    json_data = json.dumps(res, cls=ComplexEncoder)
    update_the_cache(tracking_redis_key, json_data, 10)
    send_notification(res)
    
    return
    

class DeliveryDelay:
    
    @staticmethod
    @exception_handler_emailer("Delivery Delay Send Notification Error")
    def send_notification(tracking_obj):
        email = tracking_obj['user_email']
        notification_user = tracking_obj['notification_user']
        
        event_obj_data =  get_event_obj_data(tracking_obj, email, notification_user, is_dd=True)
        
        if not event_obj_data:
            return False
                
        channel_list, event_obj = event_obj_data
            
        event_func = get_event_function(event_obj['current_status'])
        event_obj['edd_stamp'] = (event_obj['edd_stamp'] + timedelta(days=1)).strftime("%d-%m-%Y")

        if event_func and channel_list:
            event_func(event_obj)
        


    
    @classmethod
    @exception_handler_emailer("Delivery Delay Error")
    def fetch_delay_delivery_orders(cls):
        
        filters = {
            "is_active": True,
            "delivery_delay": {
                "$ne": []
            }
        }
        
        track_info_col = get_db_collection(settings.USER_NOTIFICATION_COLLECTION_NAME)
        cursor_data  = track_info_col.find(filters)
        
        email_list = []
        user_config_data = {}
        
        for item in cursor_data:
            email_list.append(item['email'])
            user_config_data[item['email']] = item            
            
        LAST_DAYS = 20
        start_date = datetime.now() - timedelta(days=LAST_DAYS)
        
        tz = pytz.timezone("UTC")
        start_date = datetime.fromtimestamp(
            start_date.timestamp(), tz
        )
        edd_to_date = datetime.now()
        edd_to_date = datetime(year=edd_to_date.year,month=edd_to_date.month,day=edd_to_date.day,hour=18,minute=29,second=59,microsecond=999999,tzinfo=timezone.utc)
        
        edd_from_date = edd_to_date - timedelta(days=1)
        edd_from_date = edd_from_date + timedelta(microseconds=1)
        updated_at_from_date = edd_from_date - timedelta(days=3)
        
        filters = [
            {
                '$match':{
                    'user_email': {
                        '$in': email_list,
                    }, 
                    'client_order_id': {
                        '$exists': True, 
                        '$ne': None
                    },
                    'status.current_status_type': {
                        '$in': [
                            'OT'
                        ]
                    }
                }
            },
            {
                '$addFields': {
                    'edd_stamp': {
                        '$dateFromString': {
                            'dateString': '$edd_stamp', 
                            'onError': '$edd_stamp', 
                            'timezone': '+05:30'
                        }
                    }
                }
            }, 
            {
                '$match': {
                    'edd_stamp': {
                        '$lte': edd_to_date,
                        '$gte': edd_from_date
                    }, 
                    'updated_at': {
                        '$lte': edd_to_date,
                        '$gte': updated_at_from_date
                    },
                }
            },
            {
                '$project': {
                    'breadth': 0, 
                    'weight': 0, 
                    'item_tax_percentage': 0, 
                    'web_address': 0, 
                    'billing_zone': 0, 
                    'dispatch_mode': 0, 
                    'logo': 0, 
                    'sku': 0, 
                    'user_id': 0, 
                    'order_type': 0, 
                    'hsn_code': 0, 
                    'last_update_from': 0, 
                    'courier_input_weight': 0, 
                    'user_pk': 0, 
                    'length': 0, 
                    'ops_profile': 0, 
                    'err': 0, 
                    'client_extra_var': 0
                }
            }
        ]
                
        data = batch_documents_with_agg(filters, config("TRACKING_INFO_COLLECTION_NAME"))

        logs_data = []

        for item in data:            
            item['notification_user'] = user_config_data[item['user_email']]
            logs_data.append({
                "awb": item['tracking_id'],
                "updated_at": item['updated_at'].strftime("%m/%d/%Y, %H:%M:%S"),
                "edd_stamp": item['edd_stamp'].strftime("%m/%d/%Y, %H:%M:%S"),
                "channel_list": item['notification_user']['delivery_delay'],
                "to_email": item['info'].get('to_email'),
                "to_number": item['info'].get('to_phone_number'),
                "user_email": item['user_email']
            })
            cls.send_notification(item)
            
        notification_logger.info({
            "type": "Delivery Delay Data",
            "logs_data": json.dumps(logs_data, cls=ComplexEncoder),
        })

        return {"success": True, "message": "Delivery Delay trigger done"}
        