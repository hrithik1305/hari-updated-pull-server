import graphene
from pymongo import ReturnDocument
from datetime import datetime
from django.conf import settings
import copy

from apps.common.mongo_utils import (
    get_db_collection    
)
from .constants import NOTIFICATION_USER_DEFAULT_DICT


class NotificationUsersType(graphene.ObjectType):
    _id = graphene.String()


class NotificationUsersInput(graphene.InputObjectType):
    order_placed = graphene.List(graphene.String)
    picked_up_delayed = graphene.List(graphene.String)
    order_picked_up = graphene.List(graphene.String)
    order_in_transit = graphene.List(graphene.String)
    delivery_delay = graphene.List(graphene.String)
    out_for_delivery_sd = graphene.List(graphene.String)
    out_for_delivery_er = graphene.List(graphene.String)
    order_delivered = graphene.List(graphene.String)
    order_not_delivered = graphene.List(graphene.String)
    order_cancelled = graphene.List(graphene.String)
    

class UpdateNotificationUserMutation(graphene.Mutation):
    class Arguments:
        notification_data = NotificationUsersInput(required=True)

    notification = graphene.Field(NotificationUsersType)

    @staticmethod
    def mutate(_, info, notification_data=None):
        from .helpers import NotificationHelpers
        
        user_email = info.context.META.get('HTTP_X_EMAIL')
        notification_user_cursor = get_db_collection(settings.USER_NOTIFICATION_COLLECTION_NAME)
        notification_user_dict = NOTIFICATION_USER_DEFAULT_DICT.copy()
                
        updated_dict = {}
        
        for key, value in notification_data.items():
            if key in notification_user_dict:
                updated_dict[key] = value
            
        old_data = notification_user_cursor.find_one({'email': user_email}, {"_id": 0})

        old_logs = []
        if not old_data:
            old_data = notification_user_dict

        old_logs = old_data['logs']
        del old_data['logs']
        old_logs.insert(0, {
            "timestamp": str(datetime.now()),
            "data": copy.deepcopy(old_data)
        }) 
        
        old_data.update({
            "logs": old_logs
        })
        old_data.update(updated_dict)
        free_sms_events, free_wp_events = NotificationHelpers.get_free_event_list_frm_db(old_data)

        old_data['email'] = user_email
        old_data['free_sms_events'] = free_sms_events
        old_data['free_wp_events'] = free_wp_events
        
        res = notification_user_cursor.find_one_and_update(
            {"email": user_email}, 
            {"$set": old_data}, 
            upsert=True,
            projection={'_id': True},
            return_document=ReturnDocument.AFTER
        )
        
        return UpdateNotificationUserMutation(notification={"_id": res.get('_id')})
    
    
class Mutation(graphene.ObjectType):
    update_notification = UpdateNotificationUserMutation.Field()
    
