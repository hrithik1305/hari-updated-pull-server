from django.conf import settings
from apps.common.mongo_utils import create_document_to_mongo_db_using_pymongo


class CreateDump:
    COLLECTION_NAME = settings.USER_NOTIFICATION_COLLECTION_NAME
    LOGS_COLLECTION_NAME = settings.NOTIFICATION_LOGS_COLLECTION_NAME
    
    def create_logs(self):
        obj = {
            "tracking_id": "",
            "email": "g.mangla911@gmail.com",
            "order_id": "",
            "contact_number": "",
            "message_id": "",
            "notification_channel": "",
            "event_type": "",
            "delivery_status": "",
            "consumed_coins": "",
            "attempted_at": "",
            "remarks": "",
            "is_active": True,
            "kwargs": {},
        }
        obj_id = create_document_to_mongo_db_using_pymongo(obj, self.LOGS_COLLECTION_NAME)
        return obj_id
    
    def create_notification_config(self):
        obj = {
            "email": "ops@dgriders.com",
            "order_placed": ["sms","wp","email"],
            "order_picked_up": ["sms","wp","email"],
            "picked_up_delayed": ["sms","wp","email"],
            "order_in_transit": ["sms","wp","email"],
            "delivery_delay": ["sms","wp","email"],
            "out_for_delivery_sd": ["sms","wp","email"],
            "out_for_delivery_er": ["sms","wp","email"],
            "order_delivered": ["sms","wp","email"],
            "order_not_delivered": ["sms","wp","email"],
            "order_cancelled": ["sms","wp","email"],
            "free_wp_events": ["order_placed"],
            "free_sms_events": ["order_placed", "order_picked_up"],
            "delay_delivery_in_days": 1,
            "total_coins": 100,
            "sms_coins": 0.15,
            "wp_coins": 0.45,
            "email_coins": 0,
            "free_sms": 2,
            "free_wp": 1,
            "free_email": -1,
            "is_active": True,
            "kwargs": {},
            "logs": [],
            "coins_to_rupee": 0.15
        }
        
        obj_id = create_document_to_mongo_db_using_pymongo(obj, self.COLLECTION_NAME)
        # values_list = {}
        # filter = {
        #     "email": "g.mangla911@gmail.com",
        #     "is_active": True
        # }
        # document = get_single_document(filter, values=None, collection_name=self.COLLECTION_NAME)

        
        return obj_id
    
