from django.conf import settings
import sendgrid
from celery import shared_task
from django.utils.safestring import SafeText

@shared_task
def send_email(**kwargs):
    """
    kwargs: 
        email_list: [], 
        subject: "",
        body: "" or <class 'django.utils.safestring.SafeString'>,
        cc: [],
        bcc: [],
    """
    from .helpers import EmailHelpers
    try:
        SEND_GRID_KEY = settings.SEND_GRID_KEY
        
        email_list = kwargs.get('email_list') or []
        cc_email_list = kwargs.get('cc') or []
        bcc_email_list = kwargs.get('bcc') or []
        
        subject = kwargs.get('subject') or "Mail from pickrr"
        
        body = kwargs.get('body') or ""
        body = body if isinstance(body, SafeText) else EmailHelpers.generate_email_body_template(context={'body': body})

        email_list = EmailHelpers.get_email_list(email_list)
        cc_email_lists = EmailHelpers.get_email_list(cc_email_list)
        bcc_email_lists = EmailHelpers.get_email_list(bcc_email_list)

        data = {
            "personalizations": [
                {
                    "to": email_list,
                    "subject": subject  
                }
            ],
            "from": {
                "email": "info@pickrr.com"
            },
            "content": [
                {
                    "type": "text/html",
                    "value": body
                }
            ]
        }
        if cc_email_lists:
            data["personalizations"][0].update({
                "cc": cc_email_lists
            })
        if bcc_email_lists:
            data["personalizations"][0].update({
                "bcc": bcc_email_lists
            })
        sg = sendgrid.SendGridAPIClient(api_key=SEND_GRID_KEY)
        sg.client.mail.send.post(request_body=data)
    except Exception as e:
        send_email.retry(exc=e, max_retries=1)


@shared_task
def delay_delivery_task():
    from .services import DeliveryDelay
    DeliveryDelay.fetch_delay_delivery_orders()