from django.template.loader import render_to_string
from django.conf import settings
from functools import wraps
import json

from .preparator import  GetSMSTemplates, GetWPTemplates
from .constants import EVENTS_STATUS_MAP, BASE_TRACKING_URL
from apps.common.mongo_utils import batch_documents_with_agg

class ComplexEncoder(json.JSONEncoder):
     def default(self, obj):
         try:
             return json.JSONEncoder.default(self, obj)
         except:
             return str(obj)

class EmailHelpers:
    @staticmethod
    def generate_email_body_template(context={}, template='email_template.html'):
        try:
            rendered = render_to_string(template, context)
            return rendered
        except:
            return context.get('body')

    @staticmethod
    def get_email_list(emails_list):
        try:
            email_list = [x.lower() for x in emails_list if x]
            email_list = list(set(email_list))
            email_list = [{"email": email} for email in email_list]
            return email_list
        except:
            return []
        

def exception_handler_emailer(subject=""):
    from .tasks import send_email

    def inner_function(function):
        @wraps(function)
        def wrapper(*args, **kwargs):    
            try:
                function(*args, **kwargs)
            except Exception as e:
                send_email.delay(**{
                    "email_list": settings.DEVELOPER_EMAILS,
                    "subject": subject or f"{function.__name__} Error",
                    "body": str(e)
                })
        return wrapper
    return inner_function


def get_cleaned_phone_number(number):
    number = number.replace('"','').replace(' ','')
    number = number.lstrip('0') or '0'
    number = number.replace('-','')
    
    if '+91' in number:
        pass
    elif len(number) == 12 and '91' in number:
        number = "+" + number
    else:
        number = '+91' + number
    return number


class NotificationHelpers:

    @staticmethod
    def get_event_template_func(current_status, type):
        template_cls = GetWPTemplates if type == 'WP' else GetSMSTemplates

        event = EVENTS_STATUS_MAP.get(current_status)
        
        try:
            event_func = getattr(template_cls, event, None)
            return event_func
        except:
            return None    
     
   
    @staticmethod
    def get_tracking_url(obj):
        
        tracking_id = obj['tracking_id']
        URL = f"{BASE_TRACKING_URL}/#?tracking_id={tracking_id}"
        
        return URL
    
    @staticmethod
    def get_event_channels_list(current_status, notification_data):
        event = EVENTS_STATUS_MAP.get(current_status)
        
        if not event:
            return None
        
        return notification_data.get(event)

    @staticmethod
    def get_free_event_list_frm_db(notification_user_data):
        free_events_for_sms = []
        free_events_for_wp = []
        for key, value in notification_user_data.items():
            if type(value) == list:
                if 'sms' in value:
                    free_events_for_sms.append(key)
                if 'wp' in value:
                    free_events_for_wp.append(key)
        
        free_events_for_sms = free_events_for_sms[:notification_user_data['free_sms']]
        free_events_for_wp = free_events_for_wp[:notification_user_data['free_wp']]
        return free_events_for_sms, free_events_for_wp
    
    @classmethod
    def get_free_event_list(cls, notification_user_data):
               
        if notification_user_data.get('free_wp_events') and notification_user_data.get('free_sms_events'):
            free_events_for_sms = notification_user_data['free_sms_events']
            free_events_for_wp = notification_user_data['free_wp_events']

            return free_events_for_sms, free_events_for_wp

        return cls.get_free_event_list_frm_db(notification_user_data)
        
        
    @staticmethod
    def get_channel_wise_charge(channel_type, logs_count, notification_data):
        channel_sent_count = logs_count.get(channel_type) or 0
        
        if channel_sent_count > 0:
            return notification_data[f"{channel_type}_coins"]
        
        return 0

    @staticmethod
    def get_notification_logs_count(event_obj):
        filters = [
            {
                '$match': {
                    'event_type': event_obj['current_status'], 
                    'tracking_id': event_obj['tracking_id'], 
                    'email': event_obj['email']
                }
            }, {
                '$group': {
                    '_id': '$notification_channel', 
                    'count': {
                        '$sum': 1
                    }
                }
            }
        ]
        
        cursor = batch_documents_with_agg(filters, settings.NOTIFICATION_LOGS_COLLECTION_NAME)
        notification_logs_count = {item['_id']: item['count'] for item in cursor}
        
        return notification_logs_count


    @classmethod
    def get_channels_charge(cls, event_obj, notification_user_data):

        current_status = event_obj['current_status']
    
        notification_logs_count = cls.get_notification_logs_count(event_obj)
        
        free_events_for_sms,free_events_for_wp =  cls.get_free_event_list(notification_user_data)
        
        channel_charge = {
            "sms": 0,
            "wp": 0,
        }
        
        event = EVENTS_STATUS_MAP.get(current_status)
        
        if not event:
            return
        
        if 'sms' in notification_user_data[event]:
            channel_charge['sms'] = notification_user_data['sms_coins'] if event not in free_events_for_sms else cls.get_channel_wise_charge(
                "sms", notification_logs_count, notification_user_data
            )
            
        if 'wp' in notification_user_data[event]:
            channel_charge['wp'] = notification_user_data['wp_coins'] if event not in free_events_for_wp else cls.get_channel_wise_charge(
                "wp", notification_logs_count, notification_user_data
            )

        return channel_charge, notification_logs_count


    @staticmethod
    def balance_check(channel_charge_data, notification_data):
        total_coins = notification_data['total_coins']
        
        total_charges = 0
        for _, value in channel_charge_data.items():
            total_charges += value

        return total_coins >= total_charges


    @staticmethod
    def check_notification_for_ot(notification_logs_count):
        """ 
        If sms and wp both already sent
        then we don't need to send notification again
        """
        sent_count = 0
        for _, count in notification_logs_count.items():
            if count > 0:
                sent_count+=1
        return sent_count >= 2

        