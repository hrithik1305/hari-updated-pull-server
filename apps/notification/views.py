from rest_framework.response import Response
from rest_framework import views
from graphene_django.views import GraphQLView
from django.http import HttpResponse

from .services import NotificationServices


class TokenAuthGraphQLView(GraphQLView):
    def dispatch(self, request, *args, **kwargs):
        auth_email = request.META.get('HTTP_X_EMAIL')
        if auth_email:
            return super().dispatch(request, *args, **kwargs)
        else:
            return HttpResponse('Authorization Error', status=401)

class NotificationWebhooks(views.APIView):
    
    def get(self, request):
        NotificationServices.update_wp_webhooks_data(request)
        return Response({"success": True})
    

    def post(self, request):
        NotificationServices.update_sms_webhooks_data(request)
        return Response({"success": True})


