from apps.common.utils import GETRequest
from pickrr_tracker.loggers import delhivery_logger

from .utils import get_pikrr_response_delhivery, get_token


def tracker(courier_type: str, waybills: list) -> dict:
    """tracker('delhivery', ["742081148512", "742081149175"])"""
    params = {
        "token": get_token(courier_type),
        "waybill": ",".join(waybills),
    }
    url = "https://track.delhivery.com/api/v1/packages/json/"
    request = GETRequest(url=url, params=params)
    response = request.send()
    delhivery_logger.info(
        {"url": url, "params": params, "courier_response": str(response)}
    )
    if response["is_success"] and not response["data"]["courier_response"].get(
        "Error"
    ):
        response["data"]["courier_response"] = {
            j["AWB"]: j
            for shipment in response["data"]["courier_response"][
                "ShipmentData"
            ]
            for i, j in shipment.items()
        }
        try:
            response["data"]["pickrr_response"] = get_pikrr_response_delhivery(
                response["data"]["courier_response"]
            )
        except Exception as e:
            response["is_success"] = False
            response["data"]["pickrr_response"] = {"err": str(e)}
            response["msg"] = str(e)
            delhivery_logger.error(
                {"err": str(e), "waybill": ",".join(waybills)}
            )
    else:
        msg = response["data"]["courier_response"]["Error"]
        response["is_success"] = False
        response["data"]["pickrr_response"] = {"err": msg}
        response["msg"] = msg
    return response
