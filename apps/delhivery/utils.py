import json
from datetime import datetime

from django.conf import settings
from sentry_sdk import capture_exception

from pickrr_tracker.loggers import delhivery_logger


def get_token(courier_type: str) -> str:
    credentials_map = {
        "delhivery": settings.DELHIVERY_TOKEN,
        "delhivery_ncr_mps": settings.DELHIVERY_NCR_MPS_TOKEN,
        "delhivery_non_est": settings.DELHIVERY_NON_EST_TOKEN,
        "delhivery_express": settings.DELHIVERY_EXPRESS_TOKEN,
        "delhivery_reverse": settings.DELHIVERY_REVERSE_TOKEN,
        "delhivery_air": settings.DELHIVERY_AIR_TOKEN,
        "delhivery_bulk": settings.DELHIVERY_BULK_TOKEN,
        "delhivery_bulk_mps": settings.DELHIVERY_BULK_MPS_TOKEN,
        "delhivery_5kg": settings.DELHIVERY_5KG_TOKEN,
        "delhivery_5kg_mps": settings.DELHIVERY_5KG_MPS_TOKEN,
        "delhivery_air_zookr": settings.DELHIVERY_AIR_ZOOKR_TOKEN,
        "delhivery_dg_zookr": settings.DELHIVERY_DG_ZOOKR_TOKEN,
        "weshyp_delhivery_air_bulk": settings.WESHYP_DELHIVERY_AIR_BULK_TOKEN,
        "weshyp_delhivery_air_express": settings.WESHYP_DELHIVERY_AIR_EXPRESS_TOKEN,
        "weshyp_delhivery_express": settings.WESHYP_DELHIVERY_EXPRESS_TOKEN,
        "delhivery_heavy": settings.DELHIVERY_HEAVY_TOKEN,
        "delhivery_heavy_mps": settings.DELHIVERY_HEAVY_MPS_TOKEN,
        "delhivery_20kg": settings.DELHIVERY_20KG_TOKEN,
        "delhivery_20kg_mps": settings.DELHIVERY_20KG_MPS_TOKEN,
    }
    return credentials_map[courier_type]


DELHIVERY_MAPPER = {
    "FMOFP-101_UD": {
        "courier_remark": "Out for Pickup",
        "scan_type": "OFP",
        "pickrr_sub_status_code": "",
    },
    "FMEOD-101_UD": {
        "courier_remark": "Incomplete shipper pickup address and shipper not contactable",
        "scan_type": "PPF",
        "pickrr_sub_status_code": "AI",
    },
    "FMEOD-102_UD": {
        "courier_remark": "Pick up Attempted within window and shipment not ready",
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SNR",
    },
    "FMEOD-103_UD": {
        "courier_remark": "Shipper is closed",
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SC",
    },
    "FMEOD-104_UD": {
        "courier_remark": "Vehicle breakdown",
        "scan_type": "PPF",
        "pickrr_sub_status_code": "NA",
    },
    "FMEOD-106_UD": {
        "courier_remark": "Pickup not attempted",
        "scan_type": "PPF",
        "pickrr_sub_status_code": "NA",
    },
    "FMEOD-107_UD": {
        "courier_remark": "Damaged Package",
        "scan_type": "PPF",
        "pickrr_sub_status_code": "DAM",
    },
    "FMEOD-108_UD": {
        "courier_remark": "Vehicle capacity constraint",
        "scan_type": "PPF",
        "pickrr_sub_status_code": "NA",
    },
    "FMEOD-109_UD": {
        "courier_remark": "Improper / missing regulatory paper work",
        "scan_type": "PPF",
        "pickrr_sub_status_code": "REGU",
    },
    "FMEOD-111_UD": {
        "courier_remark": "Pickup request schedule but no request from shipper",
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SNR",
    },
    "FMEOD-112_UD": {
        "courier_remark": "Duplicate pickup request",
        "scan_type": "PPF",
        "pickrr_sub_status_code": "DUP",
    },
    "FMEOD-110_UD": {
        "courier_remark": "Vendor Shifted",
        "scan_type": "PPF",
        "pickrr_sub_status_code": "AI",
    },
    "FMEOD-118_UD": {
        "courier_remark": "Seller cancelled order",
        "scan_type": "PPF",
        "pickrr_sub_status_code": "CANC",
    },
    "X-UCI_UD": {
        "courier_remark": "Consignment Manifested",
        "scan_type": "OM",
        "pickrr_sub_status_code": "",
    },
    "PNP-101_UD": {
        "courier_remark": "Package not picked/received from client'",
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SNR",
    },
    "X-PNP_UD": {
        "courier_remark": "Package not picked/recieved from client'",
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SNR",
    },
    "X-PPOM_UD": {
        "courier_remark": "Shipment Picked Up from Client Location'",
        "scan_type": "PP",
        "pickrr_sub_status_code": "",
    },
    "X-PPONM_UD": {
        "courier_remark": "Shipment Picked Up from Client Location but Data Not Recieved'",
        "scan_type": "PP",
        "pickrr_sub_status_code": "",
    },
    "X-PROM_UD": {
        "courier_remark": "Shipment Picked Up at Origin Center'",
        "scan_type": "SHP",
        "pickrr_sub_status_code": "",
    },
    "X-PRONM_UD": {
        "courier_remark": "Shipment Picked Up at Origin Center but Data Not Recieved'",
        "scan_type": "SHP",
        "pickrr_sub_status_code": "",
    },
    "X-PIOM_UD": {
        "courier_remark": "Shipment Recieved at Origin Center'",
        "scan_type": "SHP",
        "pickrr_sub_status_code": "",
    },
    "X-IBD3F_UD": {
        "courier_remark": "Received at destination city",
        "scan_type": "RAD",
        "pickrr_sub_status_code": "",
    },
    "X-IBD1F_UD": {
        "courier_remark": "Bag incoming at destination city",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "X-DDD3FD_UD": {
        "courier_remark": "Out for delivery",
        "scan_type": "OO",
        "pickrr_sub_status_code": "",
    },
    "X- LM1D_UD": {
        "courier_remark": "Out for delivery",
        "scan_type": "OO",
        "pickrr_sub_status_code": "",
    },
    "X-DDD3F_UD": {
        "courier_remark": "Out for delivery",
        "scan_type": "OO",
        "pickrr_sub_status_code": "",
    },
    "X-DDD2FD_UD": {
        "courier_remark": "Out for delivery",
        "scan_type": "OO",
        "pickrr_sub_status_code": "",
    },
    "X-DDD4FD_UD": {
        "courier_remark": "Out for delivery",
        "scan_type": "OO",
        "pickrr_sub_status_code": "",
    },
    "X-DDO3F_UD": {
        "courier_remark": "Out for delivery",
        "scan_type": "OO",
        "pickrr_sub_status_code": "",
    },
    "X-DDD1FD_UD": {
        "courier_remark": "Out for delivery",
        "scan_type": "OO",
        "pickrr_sub_status_code": "",
    },
    "X-DDD1LF_UD": {
        "courier_remark": "Out for delivery",
        "scan_type": "OO",
        "pickrr_sub_status_code": "",
    },
    "X-DDD3LF_UD": {
        "courier_remark": "Out for delivery",
        "scan_type": "OO",
        "pickrr_sub_status_code": "",
    },
    "EOD-600_DL": {
        "courier_remark": "Delivered to Courier",
        "scan_type": "DL",
        "pickrr_sub_status_code": "",
    },
    "EOD-141_DL": {
        "courier_remark": "Delivered to consignee - OTP & QR Verified Delivery",
        "scan_type": "DL",
        "pickrr_sub_status_code": "",
    },
    "EOD-145_DL": {
        "courier_remark": "Delivered to consignee - QR Verified",
        "scan_type": "DL",
        "pickrr_sub_status_code": "",
    },
    "EOD-135_DL": {
        "courier_remark": "Delivered to consignee - OTP Verified delivery",
        "scan_type": "DL",
        "pickrr_sub_status_code": "",
    },
    "EOD-143_DL": {
        "courier_remark": "Delivered to others as instructed by consignee - QR Verified",
        "scan_type": "DL",
        "pickrr_sub_status_code": "",
    },
    "EOD-37_DL": {
        "courier_remark": "Delivered at Mailroom/Security",
        "scan_type": "DL",
        "pickrr_sub_status_code": "",
    },
    "EOD-136_DL": {
        "courier_remark": "Delivered without verification",
        "scan_type": "DL",
        "pickrr_sub_status_code": "",
    },
    "EOD-144_DL": {
        "courier_remark": "Delivered at Mailroom/Security - QR Verified",
        "scan_type": "DL",
        "pickrr_sub_status_code": "",
    },
    "EOD-38_DL": {
        "courier_remark": "Delivered to consignee",
        "scan_type": "DL",
        "pickrr_sub_status_code": "",
    },
    "EOD-36_DL": {
        "courier_remark": "Delivered to other as instructed by consignee",
        "scan_type": "DL",
        "pickrr_sub_status_code": "",
    },
    "SC-101_DL": {
        "courier_remark": "Delivered to consignee [Self Collect]",
        "scan_type": "DL",
        "pickrr_sub_status_code": "",
    },
    "EOD-40_UD": {
        "courier_remark": "Payment Mode / Amt Dispute",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNR",
    },
    "EOD-43_UD": {
        "courier_remark": "Self Collect",
        "scan_type": "UD",
        "pickrr_sub_status_code": "ODA",
    },
    "EOD-3_UD": {
        "courier_remark": "Asked to reschedule on",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CD",
    },
    "EOD-6_UD": {
        "courier_remark": "Customer Refused to accept/Order Cancelled",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CR",
    },
    "EOD-69_UD": {
        "courier_remark": "Customer asked for open delivery",
        "scan_type": "UD",
        "pickrr_sub_status_code": "OPDEL",
    },
    "EOD-134_UD": {
        "courier_remark": "Consignee asked for card/wallet on delivery payment",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNR",
    },
    "EOD-105_UD": {
        "courier_remark": "Consignment seized by consignee",
        "scan_type": "UD",
        "pickrr_sub_status_code": "OTH",
    },
    "EOD-104_UD": {
        "courier_remark": "Entry restricted area",
        "scan_type": "UD",
        "pickrr_sub_status_code": "REST",
    },
    "EOD-86_UD": {
        "courier_remark": "Not attempted",
        "scan_type": "UD",
        "pickrr_sub_status_code": "SD",
    },
    "EOD-15_UD": {
        "courier_remark": "Consignee moved/shifted",
        "scan_type": "UD",
        "pickrr_sub_status_code": "AI",
    },
    "EOD-11_UD": {
        "courier_remark": "Consignee unavailable",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNA",
    },
    "EOD-16_UD": {
        "courier_remark": "Payment not ready with customer",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNR",
    },
    "EOD-133_UD": {
        "courier_remark": "Cheque Data Incorrect",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNR",
    },
    "EOD- 146_UD": {
        "courier_remark": "No Id Proof",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CI",
    },
    "EOD-111_UD": {
        "courier_remark": "Consignee opened the package and refused to accept",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CR",
    },
    "EOD-74_UD": {
        "courier_remark": "Bad/Incomplete Address",
        "scan_type": "UD",
        "pickrr_sub_status_code": "AI",
    },
    "SC-102_UD": {
        "courier_remark": "ODA Shipments",
        "scan_type": "UD",
        "pickrr_sub_status_code": "ODA",
    },
    "SC-103_UD": {
        "courier_remark": "Self Collect requested by customer",
        "scan_type": "UD",
        "pickrr_sub_status_code": "ODA",
    },
    "SC-106_UD": {
        "courier_remark": "Package marked for self collect",
        "scan_type": "UD",
        "pickrr_sub_status_code": "ODA",
    },
    "SC-104_UD": {
        "courier_remark": "Bad Address",
        "scan_type": "UD",
        "pickrr_sub_status_code": "AI",
    },
    "X-SC_UD": {
        "courier_remark": "Reached out to customer for Self Collect",
        "scan_type": "UD",
        "pickrr_sub_status_code": "ODA",
    },
    "COVID19 - 001_UD": {
        "courier_remark": "Corona Containment /hotspot area",
        "scan_type": "UD",
        "pickrr_sub_status_code": "REST",
    },
    "COVID19 - 002_UD": {
        "courier_remark": "Corona No E-Passes",
        "scan_type": "UD",
        "pickrr_sub_status_code": "REST",
    },
    "COVID19 - 003_UD": {
        "courier_remark": "Corona Police Shut Down",
        "scan_type": "UD",
        "pickrr_sub_status_code": "REST",
    },
    "COVID19 - 004_UD": {
        "courier_remark": "Corona Limited Dispatch Timings",
        "scan_type": "UD",
        "pickrr_sub_status_code": "REST",
    },
    "COVID19 - 005_UD": {
        "courier_remark": "Corona No Forward Vehicle Pass",
        "scan_type": "UD",
        "pickrr_sub_status_code": "REST",
    },
    "COVID19 - 006_UD": {
        "courier_remark": "Corona Manpower Shortage",
        "scan_type": "UD",
        "pickrr_sub_status_code": "REST",
    },
    "COVID19 - 007_UD": {
        "courier_remark": "Corona Client Closed",
        "scan_type": "UD",
        "pickrr_sub_status_code": "REST",
    },
    "COVID19 - 011_UD": {
        "courier_remark": "Shipment moved In Red Zone",
        "scan_type": "UD",
        "pickrr_sub_status_code": "REST",
    },
    "COVID19 - 012_UD": {
        "courier_remark": "Shipment moved out of Red Zone",
        "scan_type": "UD",
        "pickrr_sub_status_code": "REST",
    },
    "X-DLO2F_UD": {
        "courier_remark": "Added to IST",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "X-DLD1F_UD": {
        "courier_remark": "Added to IST",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "X-DLO1F_UD": {
        "courier_remark": "Added to IST",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "X-DLD0F_UD": {
        "courier_remark": "Added to IST",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "X-DLL0F_UD": {
        "courier_remark": "Added to IST",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "X-DLL2F_UD": {
        "courier_remark": "Added to IST",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "X-DLL1F_UD": {
        "courier_remark": "Added to IST",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "X-DLD2F_UD": {
        "courier_remark": "Added to IST",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "CS-101_UD": {
        "courier_remark": "Inbound against permanent connection [custody scan]",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "X-DLO0F_UD": {
        "courier_remark": "Added to IST",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "X-ILO1F_UD": {
        "courier_remark": "IST Received",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "X-ILL1F_UD": {
        "courier_remark": "IST Received",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "X-ILO0F_UD": {
        "courier_remark": "IST Received",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "X-ILD0F_UD": {
        "courier_remark": "IST Received",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "X-ILL2F_UD": {
        "courier_remark": "IST Received",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "X-ILO2F_UD": {
        "courier_remark": "IST Received",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "X-ILD1F_UD": {
        "courier_remark": "IST Received",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "X-ILL0F_UD": {
        "courier_remark": "IST Received",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "X-ILD2F_UD": {
        "courier_remark": "IST Received",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "X-DLD1R_RT": {
        "courier_remark": "Added to IST",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-DLO1R_RT": {
        "courier_remark": "Added to IST",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-DLD0R_RT": {
        "courier_remark": "Added to IST",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-DLL2R_RT": {
        "courier_remark": "Added to IST",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-DLL0R_RT": {
        "courier_remark": "Added to IST",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-DLO0R_RT": {
        "courier_remark": "Added to IST",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-DLL1R_RT": {
        "courier_remark": "Added to IST",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-DLD4R_RT": {
        "courier_remark": "Added to IST",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-DLD2R_RT": {
        "courier_remark": "Added to IST",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-DLO2R_RT": {
        "courier_remark": "Added to IST",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-ILD2R_RT": {
        "courier_remark": "IST received",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-ILD1R_RT": {
        "courier_remark": "IST received",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-ILO1R_RT": {
        "courier_remark": "IST received",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-ILD0R_RT": {
        "courier_remark": "IST received",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-ILO0R_RT": {
        "courier_remark": "IST received",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-ILL0R_RT": {
        "courier_remark": "IST received",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-ILL2R_RT": {
        "courier_remark": "IST received",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-ILL1R_RT": {
        "courier_remark": "IST received",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-ILO2R_RT": {
        "courier_remark": "IST received",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-IBD4R_RT": {
        "courier_remark": "Bag incoming at destination city",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-IBD1R_RT": {
        "courier_remark": "Bag incoming at destination city",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-IBD3R_RT": {
        "courier_remark": "Bag incoming at destination city",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-DRO4R_RT": {
        "courier_remark": "Dispatched for RTO",
        "scan_type": "RTO-OO",
        "pickrr_sub_status_code": "",
    },
    "X-DRD4R_RT": {
        "courier_remark": "Dispatched for RTO",
        "scan_type": "RTO-OO",
        "pickrr_sub_status_code": "",
    },
    "X-DRO2R_RT": {
        "courier_remark": "Dispatched for RTO",
        "scan_type": "RTO-OO",
        "pickrr_sub_status_code": "",
    },
    "X-DRD2R_RT": {
        "courier_remark": "Dispatched for RTO",
        "scan_type": "RTO-OO",
        "pickrr_sub_status_code": "",
    },
    "X-DRD1R_RT": {
        "courier_remark": "Dispatched for RTO",
        "scan_type": "RTO-OO",
        "pickrr_sub_status_code": "",
    },
    "X-DRO1R_RT": {
        "courier_remark": "Dispatched for RTO",
        "scan_type": "RTO-OO",
        "pickrr_sub_status_code": "",
    },
    "X-DRD3R_RT": {
        "courier_remark": "Dispatched for RTO",
        "scan_type": "RTO-OO",
        "pickrr_sub_status_code": "",
    },
    "X-DRO3R_RT": {
        "courier_remark": "Dispatched for RTO",
        "scan_type": "RTO-OO",
        "pickrr_sub_status_code": "",
    },
    "RD-PD4_RT": {
        "courier_remark": "Client Wants Open Delivery",
        "scan_type": "RTO UD",
        "pickrr_sub_status_code": "",
    },
    "RD-PD12_RT": {
        "courier_remark": "Damaged content",
        "scan_type": "RTO UD",
        "pickrr_sub_status_code": "",
    },
    "RD-PD7_RT": {
        "courier_remark": "Short Shipment",
        "scan_type": "RTO UD",
        "pickrr_sub_status_code": "",
    },
    "RD-PD8_RT": {
        "courier_remark": "Soft data not available with client",
        "scan_type": "RTO UD",
        "pickrr_sub_status_code": "",
    },
    "RD-PD10_RT": {
        "courier_remark": "Content mismatch",
        "scan_type": "RTO UD",
        "pickrr_sub_status_code": "",
    },
    "RD-PD11_RT": {
        "courier_remark": "Content missing",
        "scan_type": "RTO UD",
        "pickrr_sub_status_code": "",
    },
    "RD-PD17_RT": {
        "courier_remark": "Not attempted",
        "scan_type": "RTO UD",
        "pickrr_sub_status_code": "",
    },
    "RD-PD18_RT": {
        "courier_remark": "Incorrect seller information",
        "scan_type": "RTO UD",
        "pickrr_sub_status_code": "",
    },
    "RD-PD15_RT": {
        "courier_remark": "Client capacity constraint",
        "scan_type": "RTO UD",
        "pickrr_sub_status_code": "",
    },
    "RD-PD3_RT": {
        "courier_remark": "Client/ Seller closed",
        "scan_type": "RTO UD",
        "pickrr_sub_status_code": "",
    },
    "RD-PD20_RT": {
        "courier_remark": "Missing invoice",
        "scan_type": "RTO UD",
        "pickrr_sub_status_code": "",
    },
    "RD-PD21_RT": {
        "courier_remark": "Damaged packing",
        "scan_type": "RTO UD",
        "pickrr_sub_status_code": "",
    },
    "RD-PD22_RT": {
        "courier_remark": "Closure Delay",
        "scan_type": "RTO UD",
        "pickrr_sub_status_code": "",
    },
    "RT-110_DL": {
        "courier_remark": "RTO due to poor packaging",
        "scan_type": "RTD",
        "pickrr_sub_status_code": "",
    },
    "RD-AC1_DL": {
        "courier_remark": "RTO/DTO Delivered",
        "scan_type": "RTD",
        "pickrr_sub_status_code": "",
    },
    "RD-AC_DL": {
        "courier_remark": "RETURN Accepted",
        "scan_type": "RTD",
        "pickrr_sub_status_code": "",
    },
    "DLYRG-125_RT": {
        "courier_remark": "Delay due to Disturbance/Strike",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "DLYLH-109_RT": {
        "courier_remark": "Vehicle Breakdown",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "DLYRG-124_RT": {
        "courier_remark": "Consignment being inspected for duty",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "DLYLH-106_RT": {
        "courier_remark": "Flight delayed/cancelled",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "DLYMR-118_RT": {
        "courier_remark": "Misrouted",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "DLYRG-130_RT": {
        "courier_remark": "Region Specific Off",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "DLYLH-104_RT": {
        "courier_remark": "Flight cancelled",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "DLYRPC-416_RT": {
        "courier_remark": "Hold as per client instrutions",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "DLYRG-127_RT": {
        "courier_remark": "Documentation from shipper is insufficient",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "DLYLH-133_RT": {
        "courier_remark": "Air Offload - Capacity constraint",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "DLYLH-115_RT": {
        "courier_remark": "Air Offload - Security constraint",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "DLYRG-120_RT": {
        "courier_remark": "Under inspection by regulatory authorities",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "DLYRPC-419_RT": {
        "courier_remark": "Not dispatched due to client schedule",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "DLYRPC-417_RT": {
        "courier_remark": "Physical address doesn't match the soft data",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "DLYDC-102_RT": {
        "courier_remark": "Natural Disaster",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "DLYDC-132_RT": {
        "courier_remark": "Out of Delivery Area (ODA)",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "DLYDC-107_RT": {
        "courier_remark": "Office/Institute closed",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "DLYLH-142_RT": {
        "courier_remark": "Delay due to runway closure",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "DLYDC-101_RT": {
        "courier_remark": "Heavy Rain/ Fog",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "DLYLH-139_RT": {
        "courier_remark": "Checkpost/ Clearance Delay",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "DOFF-128_RT": {
        "courier_remark": "Surface Transit Delay",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "DLYLH-135_RT": {
        "courier_remark": "Held for Transit Pass",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "RT-106_RT": {
        "courier_remark": "Packaging intact, content mismatch/missing from client at Origin",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "RT-101_RT": {
        "courier_remark": "Returned as per Client Instructions",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "RT-113_RT": {
        "courier_remark": "Returned as per Security Instructions",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "RT-107_RT": {
        "courier_remark": "Unsuccessful NDR Reattempt",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "RT-109_RT": {
        "courier_remark": "Returned due to poor packaging",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "RT- 108_RT": {
        "courier_remark": "No client instructions to Reattempt",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-DDD3FP_PP": {
        "courier_remark": "Out for pickup",
        "scan_type": "OFP",
        "pickrr_sub_status_code": "",
    },
    "X-ASP_PP": {
        "courier_remark": "Pickup scheduled",
        "scan_type": "OM",
        "pickrr_sub_status_code": "",
    },
    "EOD-77_PU": {
        "courier_remark": "Pickup completed",
        "scan_type": "PP",
        "pickrr_sub_status_code": "",
    },
    "X-NSZ_UD": {
        "courier_remark": "Non-serviceable location",
        "scan_type": "PPF",
        "pickrr_sub_status_code": "NSL",
    },
    "EOD-21_CN": {
        "courier_remark": "Pickup/KYC request cancelled",
        "scan_type": "PPF",
        "pickrr_sub_status_code": "CANC",
    },
    "EOD-68_PP": {
        "courier_remark": "Request for delayed pickup/KYC",
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SNR",
    },
    "FMEOD-152_UD": {
        "courier_remark": "Shipment not ready for pickup",
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SNR",
    },
    "EOD-73_PP": {
        "courier_remark": "Bad/Incomplete address",
        "scan_type": "UD",
        "pickrr_sub_status_code": "AI",
    },
    "EOD-26_PP": {
        "courier_remark": "Consignee unavailable",
        "scan_type": "PPF",
        "pickrr_sub_status_code": "",
    },
    "EOD-121_PP": {
        "courier_remark": "Entry restricted area",
        "scan_type": "UD",
        "pickrr_sub_status_code": "REST",
    },
    "EOD-6O_RT": {
        "courier_remark": "OTP verified cancellation",
        "scan_type": "UD",
        "pickrr_sub_status_code": "CR-OTP",
    },
    "X-DBL1F_UD": {
        "courier_remark": "Added to Bag",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DLYRG-125_UD": {
        "courier_remark": "Delay due to Disturbance/Strike",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DLYLH-109_UD": {
        "courier_remark": "Vehicle Breakdown",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DLYRG-124_UD": {
        "courier_remark": "Consignment being inspected for duty",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DLYLH-106_UD": {
        "courier_remark": "Flight delayed/cancelled",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DLYMR-118_UD": {
        "courier_remark": "Misrouted",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DLYLH-104_UD": {
        "courier_remark": "Flight cancelled",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "RT-104_UD": {
        "courier_remark": "Shipment Damaged",
        "scan_type": "DM",
        "pickrr_sub_status_code": "",
    },
    "DLYRPC-416_UD": {
        "courier_remark": "Hold as per client instrutions",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DLYRG-127_UD": {
        "courier_remark": "Documentation from shipper is insufficient",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DLYLH-133_UD": {
        "courier_remark": "Air Offload - Capacity constraint",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DLYLH-115_UD": {
        "courier_remark": "Air Offload - Security constraint",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DLYRG-120_UD": {
        "courier_remark": "Under inspection by regulatory authorities",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DLYRPC-419_UD": {
        "courier_remark": "Not dispatched due to client schedule",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DLYRPC-417_UD": {
        "courier_remark": "Physical address doesn't match the soft data",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DLYDC-102_UD": {
        "courier_remark": "Natural Disaster",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DLYLH-142_UD": {
        "courier_remark": "Delay due to runway closure",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DLYDC-132_UD": {
        "courier_remark": "Out of Delivery Area (ODA)",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DLYDC-107_UD": {
        "courier_remark": "Office/Institute closed",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DLYDC-101_UD": {
        "courier_remark": "Heavy Rain/ Fog",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DOFF-128_UD": {
        "courier_remark": "Surface Transit Delay",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DLYLH-135_UD": {
        "courier_remark": "Held for Transit Pass",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "S-MAR_UD": {
        "courier_remark": "Package marked for movement by Surface",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DLYLH-139_UD": {
        "courier_remark": "Checkpost/ Clearance Delay",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DLYDC-416_UD": {
        "courier_remark": "Hold as per client instrutions",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "DLYFM-102_UD": {
        "courier_remark": "Hold as per client Instructions (FM)",
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "PNP-102_UD": {
        "courier_remark": "Pickup Dispute - PNPR Initiated",
        "scan_type": "PPF",
        "pickrr_sub_status_code": "OTH",
    },
    "EOD-137_UD": {
        "courier_remark": "OTP code mismatch",
        "scan_type": "UD",
        "pickrr_sub_status_code": "OTH",
    },
    "EOD-138_UD": {
        "courier_remark": "OTP not available",
        "scan_type": "UD",
        "pickrr_sub_status_code": "OTH",
    },
    "LT-100_LT": {
        "courier_remark": "Shipment Lost",
        "scan_type": "LT",
        "pickrr_sub_status_code": "",
    },
    "DLYDG-119_PU": {
        "courier_remark": "Shipment Damaged",
        "scan_type": "DM",
        "pickrr_sub_status_code": "",
    },
    "FMPUR-101_UD": {
        "courier_remark": "Pickup Scheduled",
        "scan_type": "OM",
        "pickrr_sub_status_code": "",
    },
    "X-DDD3FD_RT": {
        "courier_remark": "Out for delivery",
        "scan_type": "RTO-OO",
        "pickrr_sub_status_code": "",
    },
    "X-DBL1F_RT": {
        "courier_remark": "Added to Bag",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-IBD3F_RT": {
        "courier_remark": "Received at destination city",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-ILL1F_RT": {
        "courier_remark": "IST Received",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-ILL2F_RT": {
        "courier_remark": "IST Received",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-DLL2F_RT": {
        "courier_remark": "Added to IST",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-1LL1F_RT": {
        "courier_remark": "Added to IST",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-PIOM_RT": {
        "courier_remark": "Shipment Recieved at Origin Center",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
    "X-PPOM_RT": {
        "courier_remark": "Shipment picked up",
        "scan_type": "RTO-OT",
        "pickrr_sub_status_code": "",
    },
}


def get_pikrr_response_delhivery(data):
    final_tracking_array = []
    for awb, shipment in data.items():
        trackingdict = {}
        pickup_datetime = None
        track_data = shipment
        received_by = ""
        edd_timestamp = None
        from apps.common.services import ist_to_utc_converter

        if track_data.get("ChargedWeight"):
            trackingdict["final_weight"] = float(
                track_data["ChargedWeight"]
            ) / float(1000)
        else:
            trackingdict["final_weight"] = None
        if track_data.get("AWB"):
            waybill = track_data["AWB"]
        else:
            waybill = ""
            delhivery_logger.info(
                {
                    "msg": "Waybill Not Found in Delhivery Response",
                }
            )
        tracking_array = []
        try:
            track_list = track_data.get("Scans", [])
            for track_array in track_list:
                track_array = track_array["ScanDetail"]
                track_dict = {}
                info = ""
                scan_location = ""
                mapper_string = (
                    track_array["StatusCode"] + "_" + track_array["ScanType"]
                )
                if str(mapper_string) in DELHIVERY_MAPPER:
                    scan_type = DELHIVERY_MAPPER[mapper_string]["scan_type"]
                else:
                    continue
                scan_location = track_array["ScannedLocation"]
                info = track_array["Instructions"]
                try:
                    info_timestamp = track_array["StatusDateTime"]
                    if "." in info_timestamp:
                        info_timestamp = info_timestamp.replace(
                            "T", " "
                        ).split(".")[0]
                    else:
                        info_timestamp = info_timestamp.replace("T", " ")
                    info_date = datetime.strptime(
                        info_timestamp, "%Y-%m-%d %H:%M:%S"
                    )
                    info_date = ist_to_utc_converter(info_date)
                except Exception as e:
                    delhivery_logger.info({"err": str(e), "waybill": waybill})
                    # capture_exception(e)
                    info_date = datetime.now()
                    info_date = ist_to_utc_converter(info_date)
                if scan_type == "PP":
                    pickup_datetime = info_date
                track_dict["scan_status"] = info
                track_dict["scan_type"] = scan_type
                track_dict["scan_datetime"] = info_date
                track_dict["scan_location"] = scan_location
                track_dict["pickrr_sub_status_code"] = DELHIVERY_MAPPER[
                    mapper_string
                ]["pickrr_sub_status_code"]
                track_dict["courier_status_code"] = mapper_string
                tracking_array.append(track_dict)
        except Exception as e:
            delhivery_logger.error(
                {
                    "err": str(e),
                    "waybill": str(waybill),
                    "track_data": str(track_data),
                }
            )
            # capture_exception(e)
            pass
        tracking_array.reverse()
        trackingdict["awb"] = waybill
        if tracking_array:
            trackingdict["status"] = tracking_array[0]["scan_status"]
            trackingdict["status_date"] = tracking_array[0]["scan_datetime"]
            trackingdict["status_type"] = tracking_array[0]["scan_type"]
            trackingdict["status_location"] = tracking_array[0][
                "scan_location"
            ]
        if track_data.get("ExpectedDeliveryDate", None) is not None:
            edd_timestamp = track_data["ExpectedDeliveryDate"]
            edd_timestamp = edd_timestamp.replace("T", " ")
            edd_timestamp = datetime.strptime(
                edd_timestamp, "%Y-%m-%d %H:%M:%S"
            )
            edd_timestamp = ist_to_utc_converter(edd_timestamp)
        trackingdict["edd_stamp"] = edd_timestamp
        trackingdict["received_by"] = received_by
        trackingdict["tracking_array"] = tracking_array
        trackingdict["pickup_datetime"] = pickup_datetime
        final_tracking_array.append(trackingdict)
    return final_tracking_array
