from django.apps import AppConfig


class KerryindevConfig(AppConfig):
    name = "apps.kerryindev"
