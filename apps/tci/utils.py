from datetime import datetime

from django.conf import settings

from apps.common.services import ist_to_utc_converter

TCI_MAPPER = {
    "PKP": {"scan_type": "PP", "pickrr_sub_status_code": ""},
    "DIS": {"scan_type": "SHP", "pickrr_sub_status_code": ""},
    "ARR": {"scan_type": "RAD", "pickrr_sub_status_code": ""},
    "OFD": {"scan_type": "OFD", "pickrr_sub_status_code": ""},
    "D00": {"scan_type": "DL", "pickrr_sub_status_code": ""},
    "D01": {"scan_type": "UD", "pickrr_sub_status_code": "SD"},
    "D02": {"scan_type": "UD", "pickrr_sub_status_code": "ODA"},
    "D03": {"scan_type": "UD", "pickrr_sub_status_code": "CNA"},
    "D04": {"scan_type": "UD", "pickrr_sub_status_code": "AI"},
    "D05": {"scan_type": "UD", "pickrr_sub_status_code": "CR"},
    "D06": {"scan_type": "UD", "pickrr_sub_status_code": "CNA"},
    "D07": {"scan_type": "UD", "pickrr_sub_status_code": "CNA"},
    "D08": {"scan_type": "UD", "pickrr_sub_status_code": "CNA"},
    "D09": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D10": {"scan_type": "UD", "pickrr_sub_status_code": "CNA"},
    "D11": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D12": {"scan_type": "UD", "pickrr_sub_status_code": "SD"},
    "D13": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D14": {"scan_type": "UD", "pickrr_sub_status_code": "SD"},
    "D15": {"scan_type": "UD", "pickrr_sub_status_code": "SD"},
    "D16": {"scan_type": "UD", "pickrr_sub_status_code": "AI"},
    "D17": {"scan_type": "UD", "pickrr_sub_status_code": "SD"},
    "D18": {"scan_type": "UD", "pickrr_sub_status_code": "OTH"},
    "D19": {"scan_type": "UD", "pickrr_sub_status_code": "SD"},
    "D20": {"scan_type": "UD", "pickrr_sub_status_code": "OTH"},
    "D21": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D22": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D23": {"scan_type": "RTO", "pickrr_sub_status_code": ""},
    "D26": {"scan_type": "LT", "pickrr_sub_status_code": ""},
    "D27": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D29": {"scan_type": "UD", "pickrr_sub_status_code": "AI"},
    "D30": {"scan_type": "UD", "pickrr_sub_status_code": "REST"},
    "D31": {"scan_type": "UD", "pickrr_sub_status_code": "CNR"},
    "D32": {"scan_type": "UD", "pickrr_sub_status_code": "CNR"},
    "D33": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D34": {"scan_type": "UD", "pickrr_sub_status_code": "AI"},
    "D35": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D36": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D37": {"scan_type": "DM", "pickrr_sub_status_code": ""},
    "D38": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D39": {"scan_type": "UD", "pickrr_sub_status_code": "CD"},
    "D43": {"scan_type": "DM", "pickrr_sub_status_code": ""},
    "D44": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D50": {"scan_type": "UD", "pickrr_sub_status_code": "ODA"},
    "D51": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D52": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D53": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D54": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D55": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D56": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D57": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D58": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D59": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D60": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D61": {"scan_type": "OC", "pickrr_sub_status_code": ""},
    "D62": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D63": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D64": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D68": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D69": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D77": {"scan_type": "UD", "pickrr_sub_status_code": "OPDEL"},
    "D78": {"scan_type": "UD", "pickrr_sub_status_code": "REST"},
    "D80": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D81": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D83": {"scan_type": "DL", "pickrr_sub_status_code": ""},
    "D84": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "D85": {"scan_type": "UD", "pickrr_sub_status_code": "CD"},
    "D86": {"scan_type": "UD", "pickrr_sub_status_code": "OTH"},
    "D24": {"scan_type": "", "pickrr_sub_status_code": ""},
    "D25": {"scan_type": "", "pickrr_sub_status_code": ""},
    "D40": {"scan_type": "", "pickrr_sub_status_code": ""},
    "D41": {"scan_type": "", "pickrr_sub_status_code": ""},
    "D42": {"scan_type": "", "pickrr_sub_status_code": ""},
    "D28": {"scan_type": "", "pickrr_sub_status_code": ""},
    "D45": {"scan_type": "", "pickrr_sub_status_code": ""},
    "D47": {"scan_type": "", "pickrr_sub_status_code": ""},
    "D48": {"scan_type": "", "pickrr_sub_status_code": ""},
    "D79": {"scan_type": "", "pickrr_sub_status_code": ""},
    "D70": {"scan_type": "", "pickrr_sub_status_code": ""},
    "D71": {"scan_type": "", "pickrr_sub_status_code": ""},
    "D72": {"scan_type": "", "pickrr_sub_status_code": ""},
    "D73": {"scan_type": "", "pickrr_sub_status_code": ""},
    "D74": {"scan_type": "", "pickrr_sub_status_code": ""},
    "D75": {"scan_type": "", "pickrr_sub_status_code": ""},
    "D76": {"scan_type": "", "pickrr_sub_status_code": ""},
    "D65": {"scan_type": "", "pickrr_sub_status_code": ""},
    "D66": {"scan_type": "", "pickrr_sub_status_code": ""},
    "D82": {"scan_type": "", "pickrr_sub_status_code": ""},
}


def prepare_track_data_tci(awb, response):
    try:
        tracking_dict = {}
        tracking_dict["awb"] = str(awb)
        res = response["data"]["courier_response"]
        if str(
            res["soap:Envelope"]["soap:Body"][
                "getConsignmentResponseMessageResponse"
            ]["getConsignmentResponseMessageResult"]["ResponseMessage"][
                "Consignment"
            ][
                "ConsignmentNo"
            ]
        ) == str(awb):
            track_array = res["soap:Envelope"]["soap:Body"][
                "getConsignmentResponseMessageResponse"
            ]["getConsignmentResponseMessageResult"]["ResponseMessage"][
                "Consignment"
            ][
                "Message"
            ]
            tracking_array = []
            for detail in track_array:
                track_details = {}
                if str(detail["EventCode"]) in TCI_MAPPER:
                    track_details["scan_type"] = TCI_MAPPER[
                        str(detail["EventCode"])
                    ]
                else:
                    continue
                tci_datetime = detail["EventDate"] + " " + detail["EventTime"]
                req_datetime = datetime.strptime(
                    tci_datetime, "%d/%m/%Y %H:%M"
                )
                req_datetime = ist_to_utc_converter(req_datetime)
                track_details["scan_datetime"] = req_datetime
                track_details["scan_status"] = str(detail["EventDescription"])
                track_details["scan_location"] = detail["EventPlace"]
                tracking_array.append(track_details.copy())
                if track_details["scan_type"] == "PP":
                    tracking_dict["pickup_datetime"] = req_datetime
            try:
                edd_stamp = res["soap:Envelope"]["soap:Body"][
                    "getConsignmentResponseMessageResponse"
                ]["getConsignmentResponseMessageResult"]["ResponseMessage"][
                    "Consignment"
                ][
                    "ScheduleDeliveryDate"
                ]
                edd_stamp = datetime.strptime(edd_stamp, "%d/%m/%Y")
                tracking_dict["edd_stamp"] = edd_stamp
            except Exception as e:
                pass
            if len(tracking_array) > 0:
                tracking_dict["received_by"] = ""
                try:
                    if tracking_array[-1]["scan_type"] == "DL":
                        tracking_dict["received_by"] = (
                            str(res["received_by"])
                            if response["received_by"]
                            else ""
                        )
                except Exception as e:
                    pass
                tracking_dict["status_date"] = tracking_array[-1][
                    "scan_datetime"
                ]
                tracking_dict["status_info"] = tracking_array[-1][
                    "scan_status"
                ]
                tracking_dict["status"] = tracking_array[-1]["scan_type"]
                if tracking_array[-1]["scan_type"] == "NDR":
                    tracking_dict["status_type"] = "OT"
                else:
                    tracking_dict["status_type"] = tracking_array[-1][
                        "scan_type"
                    ]
                tracking_dict["tracking_array"] = tracking_array
        else:
            tracking_dict["err"] = "Track data not found"
    except Exception as e:
        return {"err": str(e)}
    return tracking_dict
