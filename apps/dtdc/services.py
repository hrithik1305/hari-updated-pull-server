import json

import requests
from django.conf import settings

from apps.common.utils import POSTRequest
from apps.dtdc.utils import get_dtdc_tracking_details, get_token
from pickrr_tracker.loggers import dtdc_logger


def tracker(courier_type, waybills: list) -> dict:
    final_tracking_array = []
    final_courier_response = []
    tracking_url = (
        "https://blktracksvc.dtdc.com/dtdc-api/rest/JSONCnTrk/getTrackDetails"
    )
    headers = {
        "Content-Type": "application/json",
        "X-Access-Token": get_token(courier_type),
    }
    try:
        for waybill in waybills:
            data = {"trkType": "cnno", "strcnno": waybill, "addtnlDtl": "Y"}
            request = POSTRequest(
                url=tracking_url, data=data, headers=headers
            )  # is_verify true check
            response = request.send()
            dtdc_logger.info(
                {
                    "url": tracking_url,
                    "data": data,
                    "courier_response": str(response),
                }
            )
            final_courier_response.append(response["data"]["courier_response"])
            if response["is_success"]:
                try:
                    tracking_dict = get_dtdc_tracking_details(
                        response["data"]["courier_response"], waybill
                    )
                except Exception as e:
                    dtdc_logger.error({"err": str(e), "waybill": waybill})
                    tracking_dict = {"err": e}
                final_tracking_array.append(tracking_dict)
        response["data"]["courier_response"] = final_courier_response
        response["data"]["pickrr_response"] = final_tracking_array
        return response

    except Exception as e:
        dtdc_logger.error({"err": str(e), "awbs": waybills})
        return {"err": str(e), "awbs": waybills}
