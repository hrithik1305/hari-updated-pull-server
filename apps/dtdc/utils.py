from datetime import datetime

from django.conf import settings

from pickrr_tracker.loggers import dtdc_logger


def get_token(courier_type: str) -> str:
    credentials_map = {
        "dtdc": settings.DTDC_ACCESS_TOKEN,
        "dtdc_air": settings.DTDC_ACCESS_TOKEN,
        "dtdc_docs_250": settings.DTDC_DOCS_250_ACCESS_TOKEN,
        "dtdc_docs_100": settings.DTDC_DOCS_100_ACCESS_TOKEN,
    }
    return credentials_map[courier_type]


dtdc_code_mapper = {
    "BKD": "PP",
    "OBMN": "OT",
    "CDOUT": "OT",
    "IBMN": "OT",
    "CDIN": "OT",
    "OUTDLV": "OO",
    "NONDLV": "NDR",
    "DLV": "DL",
    "RTO": "RTO",
    "RTOOBMN": "RTO",
    "RTOCDOUT": "RTO",
    "RTOCDIN": "RTO",
    "RTOIBMN": "RTO",
    "RTOOUTDLV": "RTO",
    "RTODLV": "RTD",
}


def get_dtdc_tracking_details(response, waybill):
    tracking_dict = {}
    pickup_datetime = None
    edd_stamp = None
    from apps.common.services import ist_to_utc_converter

    try:
        if (
            str(response["status"]) == "SUCCESS"
            and response["errorDetails"] is None
        ):
            tracking_array = []
            for detail in response["trackDetails"]:
                track_details = {}
                if detail["strCode"] in dtdc_code_mapper:
                    track_details["scan_type"] = dtdc_code_mapper[
                        str(detail["strCode"])
                    ]
                else:
                    continue
                dtdc_datetime = (
                    detail["strActionDate"] + " " + detail["strActionTime"]
                )
                status_time = datetime.strptime(dtdc_datetime, "%d%m%Y %H%M")
                status_time = ist_to_utc_converter(status_time)
                track_details["scan_datetime"] = status_time
                scan_info = str(detail["strAction"])
                if str(detail["strCode"]) == "NONDLV":
                    scan_info = (
                        str(detail["strAction"]) + " - " + detail["sTrRemarks"]
                    )
                track_details["scan_status"] = scan_info
                if str(detail["strDestination"]):
                    location = str(detail["strDestination"])
                else:
                    location = str(detail["strOrigin"])
                track_details["scan_location"] = location
                track_details["pickrr_sub_status_code"] = ""
                track_details["courier_status_code"] = str(detail["strCode"])
                tracking_array.append(track_details.copy())
                if track_details["scan_type"] == "PP":
                    pickup_datetime = status_time
                    tracking_dict["pickup_datetime"] = pickup_datetime
            tracking_array.sort(key=lambda x: x["scan_datetime"], reverse=True)
            try:
                edd_stamp = response["trackHeader"]["strExpectedDeliveryDate"]
                edd_stamp = datetime.strptime(edd_stamp, "%d%m%Y")
                edd_stamp = ist_to_utc_converter(edd_stamp)
                tracking_dict["edd_stamp"] = edd_stamp
            except Exception as e:
                dtdc_logger.error({"err": str(e), "waybill": waybill})
                pass
            if len(tracking_array) > 0:
                tracking_dict["received_by"] = ""
                if tracking_array[0]["scan_type"] == "DL":
                    tracking_dict["received_by"] = (
                        str(response["trackHeader"]["strRemarks"])
                        if response["trackHeader"]["strRemarks"]
                        else ""
                    )
                tracking_dict["status_date"] = tracking_array[0][
                    "scan_datetime"
                ]
                tracking_dict["status_info"] = tracking_array[0]["scan_status"]
                tracking_dict["status"] = tracking_array[0]["scan_type"]
                if response.get("trackHeader") and response["trackHeader"].get(
                    "strWeight"
                ):
                    tracking_dict["final_weight"] = float(
                        response["trackHeader"]["strWeight"]
                    ) / float(1000)
                if tracking_array[0]["scan_type"] == "NDR":
                    tracking_dict["status_type"] = "OT"
                else:
                    tracking_dict["status_type"] = tracking_array[0][
                        "scan_type"
                    ]
                tracking_dict["awb"] = str(waybill)
                tracking_dict["tracking_array"] = tracking_array
        else:
            tracking_dict["awb"] = str(waybill)
            tracking_dict["err"] = str(response["errorDetails"])
        return tracking_dict
    except Exception as e:
        dtdc_logger.error({"err": str(e), "waybill": waybill})
        return {"awb": str(waybill), "err": str(e), "response": response}
