from django.apps import AppConfig


class ParceldoConfig(AppConfig):
    name = "apps.parceldo"
