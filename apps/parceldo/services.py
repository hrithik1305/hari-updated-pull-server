import json

import requests
from django.conf import settings

from apps.common.utils import POSTRequest
from apps.parceldo.utils import get_parceldo_tracking_details
from pickrr_tracker.loggers import parceldo_logger


def tracker(courier_type, waybills: list) -> dict:
    final_tracking_array = []
    tracking_url = "https://erp.parceldo.com/LCMSv1/api/trackShipmentStatus"
    headers = {
        "Content-Type": "application/json",
        "token": settings.PARCELDO_TOKEN,
    }
    final_tracking_array = []
    final_courier_response = []
    try:
        for waybill in waybills:

            data = {
                "clientCode": "PKR",
                "waybills": str(waybill),
                "trackBy": "waybill",
            }
            request = POSTRequest(
                url=tracking_url, data=data, headers=headers
            )  # is_verify true check
            response = request.send()
            parceldo_logger.info(
                {
                    "url": tracking_url,
                    "data": data,
                    "courier_response": str(response),
                }
            )
            final_courier_response.append(response["data"]["courier_response"])
            if response["is_success"]:
                try:
                    tracking_dict = get_parceldo_tracking_details(
                        response["data"]["courier_response"], waybill
                    )
                except Exception as e:
                    parceldo_logger.error({"err": str(e), "waybill": waybill})
                    tracking_dict = {"err": e}
                final_tracking_array.append(tracking_dict)
        response["data"]["courier_response"] = final_courier_response
        response["data"]["pickrr_response"] = final_tracking_array
        return response

    except Exception as e:
        parceldo_logger.error({"awbs": waybills, "err": str(e)})
        return {"err": str(e)}
