import requests

from apps.amaze.utils import get_amaze_tracking_details
from apps.common.utils import GETRequest
from pickrr_tracker.loggers import amaze_logger


def tracker(courier_type, waybills: list) -> dict:
    headers = {"Content-Type": "application/json"}
    try:
        final_tracking_array = []
        final_courier_response = []
        for waybill in waybills:
            tracking_url = (
                "https://hub.asnmcare.com/api/getaconsignment/%s"
                % str(waybill)
            )
            request = GETRequest(url=tracking_url, headers=headers)
            response = request.send()
            amaze_logger.info(
                {
                    "url": tracking_url,
                    "courier_response": str(response),
                    "awb": waybill,
                }
            )
            final_courier_response.append(response["data"]["courier_response"])
            if response["is_success"]:
                try:
                    tracking_dict = get_amaze_tracking_details(
                        response["data"]["courier_response"], waybill
                    )
                except Exception as e:
                    amaze_logger.error({"err": str(e), "waybill": waybill})
                    tracking_dict = {"err": e}
                final_tracking_array.append(tracking_dict)
        response["data"]["courier_response"] = final_courier_response
        response["data"]["pickrr_response"] = final_tracking_array
        return response
    except Exception as e:
        amaze_logger.info({"awbs": waybills, "err": str(e)})
        return {"err": str(e)}
