import json
from datetime import datetime

from django.conf import settings

from pickrr_tracker.loggers import udaan_logger


def get_token(courier_type: str) -> dict:
    credentials_map = {
        "udaan": {"token": settings.UDAAN_TOKEN},
        "udaan_b2b": {"token": settings.UDAAN_B2B_TOKEN},
    }
    return credentials_map[courier_type]


udaan_code_mapper = {
    "FW_PICKUP_CREATED": {"scan_type": "OM", "pickrr_sub_status_code": ""},
    "FW_OUT_FOR_PICKUP": {"scan_type": "OFP", "pickrr_sub_status_code": ""},
    "FW_PICKED_UP": {"scan_type": "PP", "pickrr_sub_status_code": ""},
    "FW_PICKUP_FAILED_Seller Not Attempted - Vehicle Breakdown/Traffic": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "NA",
    },
    "FW_PICKUP_FAILED_Seller Not Attempted - Vehicle full": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "NA",
    },
    "FW_PICKUP_FAILED_Seller Not Attempted - No Entry Restriction": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "NSL",
    },
    "FW_PICKUP_FAILED_Pickup Attempted - Address not found. Seller not contactable": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SU",
    },
    "FW_PICKUP_FAILED_Seller Not Attempted - Shortage of time": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "NA",
    },
    "FW_PICKUP_FAILED_Seller Not Attempted - Tech Issue": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SC",
    },
    "FW_PICKUP_FAILED_Attempted - Shop Closed": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SC",
    },
    "FW_PICKUP_FAILED_Attempted - Order not packed": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SNR",
    },
    "FW_PICKUP_FAILED_Attempted - Vehicle full": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "NA",
    },
    "FW_PICKUP_FAILED_Attempted - Poor Packaging": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "DAM",
    },
    "FW_PICKUP_FAILED_Attempted - Tech Issue": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "REJ",
    },
    "FW_PICKUP_FAILED_Attempted - Packet Id not matching": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "REJ",
    },
    "FW_PICKUP_FAILED_Attempted - Closed due to COVID Impact": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SC",
    },
    "FW_PICKUP_FAILED_Seller not attempted - Closed due to COVID Impact": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SC",
    },
    "FW_PICKED_NOT_VERIFIED": {
        "scan_type": "PP",
        "pickrr_sub_status_code": "",
    },
    "FW_HUB_INSCAN": {"scan_type": "SHP", "pickrr_sub_status_code": ""},
    "FW_HUB_OUTSCAN": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "FW_RAD": {"scan_type": "RAD", "pickrr_sub_status_code": ""},
    "FW_OUT_FOR_DELIVERY": {"scan_type": "OO", "pickrr_sub_status_code": ""},
    "FW_DELIVERY_ATTEMPTED_Market Shut - Covid Virus": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNA",
    },
    "FW_DELIVERY_ATTEMPTED_Buyer not reachable over phone": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNA",
    },
    "FW_DELIVERY_ATTEMPTED_Buyer not available/Shop closed/Out of station": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNA",
    },
    "FW_DELIVERY_ATTEMPTED_Cash not ready": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNR",
    },
    "FW_DELIVERY_ATTEMPTED_Address not found": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "AI",
    },
    "FW_DELIVERY_ATTEMPTED_Open delivery": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "OPDEL",
    },
    "FW_DELIVERY_ATTEMPTED_Buyer rejected - shipment tampered": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CR",
    },
    "FW_DELIVERY_ATTEMPTED_Shipment RTO": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "OTH",
    },
    "FW_DELIVERY_ATTEMPTED_Buyer expecting Credit order/ GST or Invoice issue": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CI",
    },
    "FW_DELIVERY_ATTEMPTED_Issue with previous order/ RVP": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CI",
    },
    "FW_DELIVERY_ATTEMPTED_Buyer refused to sign PoD/ Runsheet": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CI",
    },
    "FW_DELIVERY_ATTEMPTED_Incorrect Pincode": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "AI",
    },
    "FW_DELIVERY_ATTEMPTED_Delivery charge issue": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNR",
    },
    "CANCELLED": {"scan_type": "OC", "pickrr_sub_status_code": ""},
    "FW_DELIVERED": {"scan_type": "DL", "pickrr_sub_status_code": ""},
    "RT_RTO_MARKED": {"scan_type": "RTO", "pickrr_sub_status_code": ""},
    "RTO_MARKED": {"scan_type": "RTO", "pickrr_sub_status_code": ""},
    "RT_HUB_INSCAN": {"scan_type": "RTO-OT", "pickrr_sub_status_code": ""},
    "RT_HUB_OUTSCAN": {"scan_type": "RTO-OT", "pickrr_sub_status_code": ""},
    "RT_RAD": {"scan_type": "RTO-OT", "pickrr_sub_status_code": ""},
    "RT_OUT_FOR_DELIVERY": {
        "scan_type": "RTO-OO",
        "pickrr_sub_status_code": "",
    },
    "RT_DELIVERED": {"scan_type": "RTD", "pickrr_sub_status_code": ""},
}


def get_pikrr_response_udaan(waybill, response):
    tracking_dict = {}
    pickup_datetime = None
    edd_stamp = None
    from apps.common.services import ist_to_utc_converter

    try:
        if (
            str(response["responseCode"]) == "UE_1001"
            and response["responseMessage"] == "Request processed Successfully"
        ):
            tracking_array = []
            for detail in response["response"]["externalShipmentScans"]:
                track_details = {}
                status_string = (
                    detail["shipmentMovementType"]
                    + "_"
                    + detail["shipmentState"]
                )
                if str(status_string) in udaan_code_mapper:
                    track_details["scan_type"] = udaan_code_mapper[
                        status_string
                    ]["scan_type"]
                else:
                    continue
                udaan_datetime = int(detail["timestamp"] / 1000)
                req_datetime = datetime.fromtimestamp(udaan_datetime)
                req_datetime = ist_to_utc_converter(req_datetime)
                track_details["scan_datetime"] = req_datetime
                track_details["scan_status"] = str(detail["shipmentState"])
                track_details["scan_location"] = detail["city"]
                track_details["pickrr_sub_status_code"] = udaan_code_mapper[
                    status_string
                ]["pickrr_sub_status_code"]
                track_details["courier_status_code"] = str(status_string)
                tracking_array.append(track_details.copy())
                if track_details["scan_type"] == "PP":
                    pickup_datetime = req_datetime
                    tracking_dict["pickup_datetime"] = pickup_datetime
            tracking_array.sort(key=lambda x: x["scan_datetime"], reverse=True)
            tracking_dict["edd_stamp"] = None
            if response.get("eta"):
                edd_stamp = response["eta"]
                edd_stamp = int(edd_stamp / 1000)
                edd_stamp = datetime.fromtimestamp(edd_stamp)
                edd_stamp = ist_to_utc_converter(edd_stamp)
                tracking_dict["edd_stamp"] = edd_stamp
            if len(tracking_array) > 0:
                tracking_dict["received_by"] = ""
                if tracking_array[0]["scan_type"] == "DL":
                    tracking_dict["received_by"] = (
                        str(response["received_by"])
                        if response.get("received_by")
                        else ""
                    )
                tracking_dict["status_date"] = tracking_array[0][
                    "scan_datetime"
                ]

                tracking_dict["status_info"] = tracking_array[0]["scan_status"]
                tracking_dict["status"] = tracking_array[0]["scan_type"]
                tracking_dict["status_type"] = tracking_array[0]["scan_type"]
                tracking_dict["awb"] = waybill
                tracking_dict["tracking_array"] = tracking_array
        else:
            tracking_dict["err"] = str(response["responseMessage"])
            tracking_dict["awb"] = waybill
        return tracking_dict
    except Exception as e:
        udaan_logger.error(
            {
                "err": str(e),
                "waybill": waybill,
                "courier_response": str(response),
            }
        )
        return {tracking_dict["err"]: str(e)}
