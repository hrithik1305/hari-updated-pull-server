from django.apps import AppConfig


class UdaanConfig(AppConfig):
    name = "apps.udaan"
