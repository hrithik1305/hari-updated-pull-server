from django.contrib import admin

from .models import TrackingInfo

admin.site.register(TrackingInfo)
