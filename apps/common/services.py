import json
from datetime import date, datetime, timedelta, timezone, tzinfo

import pymongo
import pytz
import requests
from decouple import config
from django.http import JsonResponse
from rest_framework import status
from rest_framework.response import Response
from sentry_sdk import capture_exception

from apps.common.courier_mapping import courier_str_to_int, courier_tracker_map
from apps.common.dao import (
    create_tracking_event_to_cache,
    create_tracking_event_to_db,
)
from apps.common.models import FailedTracking, TrackingInfo
from apps.common.mongo_utils import (
    batch_document_iterator_v2,
    batch_source_mongo_document_iterator,
    replace_one_document,
    update_multiple_document,
    update_one_document,
)
from apps.common.utils import POSTRequest, dt_str_to_dt_obj
from pickrr_tracker import settings
from pickrr_tracker.loggers import (
    pulldataeventbridge_logger,
    pulldatatov1eventbridge_logger,
    pushevents_logger,
    responseupdate_logger,
    tracking_api_logger,
    trackingsync_logger,
)

from .constants import (
    NEW_STATUS_TO_OLD_MAPPING,
    PICKRR_STATUS_CODE_MAPPING,
    get_track_info_keys,
    get_track_parent_keys,
)

BLOCK_NDR_STRINGS = [
    "ndr call - fe remark correct",
    "call placed to consignee",
    "agent remark verified",
]


def tracker(courier_type, tracking_ids):
    tracker_func = courier_tracker_map(courier_type)
    try:
        response = tracker_func(courier_type, tracking_ids)
        return response
    except Exception as e:
        # capture_exception(e)
        return str(e)


def create_tracking_info_objs(
    courier_type, pickrr_response, courier_response, tracking_id_created_at_map
):
    try:
        data = pickrr_dict_to_db_schema(
            courier_type,
            pickrr_response,
            courier_response,
            tracking_id_created_at_map,
        )
        obj, created = TrackingInfo.objects.get_or_create(awb=data["awb"])
        if data["current_status_date"] != obj.current_status_date:
            obj.pickrr_tracking_id = data["pickrr_tracking_id"]
            obj.courier_type = data["courier_type"]
            obj.current_status = data["current_status"]
            obj.current_status_date = data["current_status_date"]
            obj.current_status_type = data["current_status_type"]
            obj.received_by = data["received_by"]
            obj.edd_stamp = data["edd_stamp"]
            obj.status = data["status"]
            obj.courier_response = data["courier_response"]
            obj.order_created_at = data["order_created_at"]
            obj.save()
    except Exception as e:
        # capture_exception(e)
        return str(e)
    return obj


def pickrr_dict_to_db_schema(
    courier_type, pickrr_response, courier_response, tracking_id_created_at_map
):
    db_schema_dict = {}
    db_schema_dict["awb"] = pickrr_response["awb"]
    db_schema_dict["pickrr_tracking_id"] = pickrr_response["awb"]
    db_schema_dict["courier_type"] = courier_str_to_int(courier_type)
    db_schema_dict["current_status"] = pickrr_response["status"]
    db_schema_dict["current_status_date"] = dt_str_to_dt_obj(
        pickrr_response["status_date"], "%Y-%m-%d %H:%M:%S"
    )
    db_schema_dict["current_status_type"] = pickrr_response["status_type"]
    db_schema_dict["received_by"] = pickrr_response["received_by"]
    db_schema_dict["edd_stamp"] = dt_str_to_dt_obj(
        pickrr_response["edd_stamp"], "%Y-%m-%d %H:%M:%S"
    )
    dt_obj_tracking_array = []
    for i in pickrr_response["tracking_array"]:
        i.update(
            {
                "scan_datetime": dt_str_to_dt_obj(
                    i["scan_datetime"], "%Y-%m-%d %H:%M:%S"
                )
            }
        )
        dt_obj_tracking_array.append(i)
    db_schema_dict["status"] = dt_obj_tracking_array
    db_schema_dict["courier_response"] = courier_response
    db_schema_dict["order_created_at"] = tracking_id_created_at_map[
        pickrr_response["awb"]
    ]
    return db_schema_dict


def create_or_update_failed_trackings(
    courier_type, failed_tracking_ids, tracking_id_created_at_map
):
    for tracking_id in failed_tracking_ids:
        try:
            tracking_obj, created = FailedTracking.objects.get_or_create(
                awb=tracking_id
            )
            tracking_obj.order_created_at = tracking_id_created_at_map[
                tracking_id
            ]
            tracking_obj.courier_type = courier_str_to_int(courier_type)
            if created:
                tracking_obj.attempts = 1
            else:
                tracking_obj.attempts += 1
            tracking_obj.save()
        except Exception as e:
            # capture_exception(e)
            pass


def send_pulled_data_to_async_via_event_bridge(data_to_send_dict: dict):
    from .aws import send_data_to_event_bridge

    to_async_eb_event_bus_source = config("TO_ASYNC_EB_EVENT_BUS_SOURCE")
    to_async_eb_event_bus_detail_type = config(
        "TO_ASYNC_EB_EVENT_BUS_DETAIL_TYPE"
    )
    to_async_eb_event_bus_name = config("TO_ASYNC_EB_EVENT_BUS_NAME")

    response = send_data_to_event_bridge(
        data_to_send_dict=data_to_send_dict,
        event_bus_source=to_async_eb_event_bus_source,
        event_bus_detail_type=to_async_eb_event_bus_detail_type,
        event_bus_name=to_async_eb_event_bus_name,
    )
    pulldataeventbridge_logger.info(
        {"data": str(data_to_send_dict), "response": str(response)}
    )
    return {"success": True}


def send_pulled_data_to_v1_via_event_bridge(data_to_send_dict: dict):
    from .aws import send_data_to_event_bridge

    eb_event_bus_source = config("TO_V1_EB_EVENT_BUS_SOURCE")
    eb_event_bus_detail_type = config("TO_V1_EB_EVENT_BUS_DETAIL_TYPE")
    eb_event_bus_name = config("TO_V1_EB_EVENT_BUS_NAME")

    # Map scan_type to old mapping for v1.
    if data_to_send_dict.get("scan_type") in NEW_STATUS_TO_OLD_MAPPING:
        data_to_send_dict["scan_type"] = NEW_STATUS_TO_OLD_MAPPING[
            data_to_send_dict["scan_type"]
        ]
    response = send_data_to_event_bridge(
        data_to_send_dict=data_to_send_dict,
        event_bus_source=eb_event_bus_source,
        event_bus_detail_type=eb_event_bus_detail_type,
        event_bus_name=eb_event_bus_name,
    )
    pulldatatov1eventbridge_logger.info(
        {"data": str(data_to_send_dict), "response": str(response)}
    )
    return {"success": True}


def get_list_of_order_awbs_to_sync(
    start_date=None, end_date=None, tracking_id=None
):
    try:
        from decouple import config

        from apps.common.mongo_utils import (
            fetch_documents_from_mongo_using_filters_and_values,
        )

        values_list = {}
        filter_1 = {
            "$and": [
                {"placed_date": {"$gte": start_date}},
                {"placed_date": {"$lte": end_date}},
            ]
        }
        filter_2 = {
            "$and": [
                {
                    "$or": [
                        {"pickrr_tracking_id": tracking_id},
                        {"courier_tracking_id": tracking_id},
                    ]
                },
                {"courier_tracking_id": {"$nin": ["", None]}},
            ]
        }

        if tracking_id:
            filters = filter_2
        else:
            filters = filter_1
        trackinginfo_collection_name = config("SOURCE_COLLECTION_NAME")
        doc_iterator = batch_source_mongo_document_iterator(
            filters=filters,
            collection_name=trackinginfo_collection_name,
        )
        return doc_iterator

    except Exception as e:
        return {"success": False}


def ItemListMaker(reportDocItemList):
    item_list = []
    try:
        for item in reportDocItemList:
            try:
                item_dict = {}
                item_dict["sku"] = item.get("sku", None)
                item_dict["item_tax_percentage"] = 0
                item_dict["item_height"] = None
                item_dict["name"] = None
                item_dict["item_name"] = item.get("item_name", None)
                item_dict["price"] = item.get("price", None)
                item_dict["item_weight"] = None
                item_dict["hsn"] = item.get("hsn_code", None)
                item_dict["shipping_charge"] = None
                item_dict["tax_per"] = 0
                item_dict["variant_title"] = None
                item_dict["quantity"] = item.get("quantity", None)
                item_dict["item_breadth"] = None
                item_dict["item_length"] = None
                item_list.append(item_dict)
            except Exception as e:
                pass
    except Exception as e:
        pass
    return item_list


def report_data_sync_pull(params):
    try:
        from datetime import datetime, timedelta, timezone

        import pytz

        if not params.get("tracking_id"):
            start_date = datetime.strptime(
                str(params["start_date"]), "%Y-%m-%d"
            )
            end_date = datetime.strptime(str(params["end_date"]), "%Y-%m-%d")
            tz = pytz.timezone("UTC")
            start_date = datetime.fromtimestamp(
                (start_date - timedelta(minutes=330)).timestamp(), tz
            )
            # end_date = start_date + timedelta(minutes=20)
            end_date = datetime.fromtimestamp(
                (
                    end_date + timedelta(days=1) - timedelta(minutes=330)
                ).timestamp(),
                tz,
            )

            report_data_to_sync = get_list_of_order_awbs_to_sync(
                start_date, end_date
            )
        else:
            tracking_id = params.get("tracking_id")
            report_data_to_sync = get_list_of_order_awbs_to_sync(
                None, None, tracking_id
            )
        query_list = []
        tracking_col_name = config("TRACKING_INFO_COLLECTION_NAME")
        for reportDoclist in report_data_to_sync:
            for reportDoc in reportDoclist:
                current_status_time = reportDoc.get(
                    "current_status_datetime", None
                )
                if current_status_time is not None:
                    try:
                        current_status_time -= timedelta(minutes=330)
                    except Exception as e:
                        if type(current_status_time) is str:
                            try:
                                x = datetime.strptime(
                                    current_status_time, "%d %b %Y, %H:%M"
                                )
                                x -= timedelta(minutes=330)
                                current_status_time = x
                            except Exception as e:
                                current_status_time = None
                        else:
                            current_status_time = None
                edd_stamp = None
                try:
                    if reportDoc.get("edd_date", None):
                        edd_stamp = datetime.strptime(edd_stamp, "%d %b %Y")
                        edd_stamp.replace(hour=18, minute=29, second=59)
                except Exception as e:
                    pass

                data_to_sync = {}
                try:
                    data_to_sync = {
                        "breadth": reportDoc.get("user_breadth", None),
                        "is_cod": True
                        if reportDoc.get("cod_amount", None) > 0
                        else False,
                        "weight": float(
                            reportDoc.get("user_dead_weight", None)
                        )
                        if reportDoc.get("user_dead_weight", None)
                        else 0,
                        "item_tax_percentage": "0",
                        "web_address": None,
                        "billing_zone": reportDoc.get("order_zone", None),
                        "dispatch_mode": reportDoc.get("order_type"),
                        "client_order_id": reportDoc.get(
                            "client_order_id", None
                        ),
                        "logo": None,
                        "pickrr_order_id": reportDoc.get(
                            "pickrr_order_id", None
                        ),
                        "sku": ItemListMaker(
                            reportDoc.get("line_items", None)
                        )[0]["sku"]
                        if len(
                            ItemListMaker(reportDoc.get("line_items", None))
                        )
                        > 0
                        else None,
                        "item_list": ItemListMaker(
                            reportDoc.get("line_items", None)
                        )
                        if len(
                            ItemListMaker(reportDoc.get("line_items", None))
                        )
                        > 0
                        else None,
                        "user_id": None,
                        "order_type": reportDoc.get("order_type", None),
                        "hsn_code": None,
                        "company_name": reportDoc.get("company_name", None),
                        "product_name": ItemListMaker(
                            reportDoc.get("line_items")
                        )[0]["item_name"]
                        if len(ItemListMaker(reportDoc.get("line_items"))) > 0
                        else None,
                        # "status": {
                        #     "current_status_time": current_status_time,
                        #     "current_status_type": "OP",
                        #     "received_by": reportDoc.get("received_by", None),
                        #     "current_status_body": reportDoc.get(
                        #         "tracking_status", None
                        #     ),
                        #     "current_status_location": reportDoc.get(
                        #         "latest_location", None
                        #     ),
                        #     "current_status_val": None,
                        # },
                        "info": {
                            "from_state": reportDoc.get("pickup_state", None),
                            "invoice_value": reportDoc.get(
                                "invoice_value", None
                            ),
                            "from_name": reportDoc.get("pickup_name", None),
                            "to_address": reportDoc.get("drop_address", None),
                            "to_state": reportDoc.get("drop_state", None),
                            "courier_name": reportDoc.get(
                                "courier_child", None
                            ),
                            "to_pincode": reportDoc.get("drop_pincode", None),
                            "to_address_id": int(
                                reportDoc.get("destination_id", None)
                            )
                            if reportDoc.get("destination_id", None)
                            is not None
                            else None,
                            "to_email": reportDoc.get(
                                "destination_email", None
                            ),
                            "from_email": reportDoc.get("pickup_email", None),
                            "to_city": reportDoc.get("drop_city", None),
                            "source_address_id": reportDoc.get("wh_id", None),
                            "to_name": reportDoc.get(
                                "drop_customer_name", None
                            ),
                            "from_phone_number": reportDoc.get(
                                "pickup_phone_number", None
                            ),
                            "from_city": reportDoc.get("pickup_city", None),
                            "user_id": None,
                            "cod_amount": reportDoc.get("cod_amount", None),
                            "is_open_ndr": False,
                            "to_phone_number": reportDoc.get(
                                "drop_customer_phone", None
                            ),
                            "from_pincode": reportDoc.get(
                                "pickup_pincode", None
                            ),
                            "from_address": reportDoc.get(
                                "pickup_address", None
                            ),
                        },
                        "is_reverse": reportDoc.get("is_reverse", None),
                        "courier_used": reportDoc.get("courier_child", None),
                        "courier_tracking_id": reportDoc.get(
                            "courier_tracking_id", None
                        ),
                        "height": reportDoc.get("user_height", None),
                        "courier_parent_name": reportDoc.get(
                            "courier_used", None
                        ),
                        "client_extra_var": None,
                        "err": None,
                        "ops_profile": reportDoc.get("ops_poc", None),
                        # "track_arr": [],
                        "length": reportDoc.get("user_length", None),
                        "edd_stamp": edd_stamp,
                        "quantity": reportDoc.get("item_quantity", None),
                        "tracking_id": reportDoc.get(
                            "pickrr_tracking_id", None
                        ),
                        "auth_token": reportDoc.get("auth_token", None),
                        "website": reportDoc.get("website", None),
                        "last_update_from": "backfill",
                        "order_created_at": reportDoc.get("placed_date", None),
                        "user_pk": None,
                        "user_email": reportDoc.get("user_email", None),
                        "created_at": datetime.now(),
                        "updated_at": datetime.now(),
                        "courier_input_weight": reportDoc.get(
                            "updated_dead_weight", None
                        ),
                    }
                except Exception as e:
                    pass
                if data_to_sync:
                    queryy = {
                        "courier_tracking_id": reportDoc[
                            "courier_tracking_id"
                        ],
                        "$set": data_to_sync,
                    }
                    query_list.append(queryy)

        update_multiple_document(query_list, tracking_col_name)
        return {"success": True}

    except Exception as e:
        return {"success": False, "err": str(e)}


def tracking_sync(params):
    try:
        err_lis = []
        success_list = []
        error = False
        for batch in params:
            for tracking in batch:
                v1_dict = tracking["json"]
                tracking_id = tracking["tracking_id"]
                v1_dict["tracking_id"] = tracking_id
                client_order_id = (
                    tracking["client_order_id"]
                    + "-PICK-"
                    + str(tracking["user_pk"])
                )
                v1_dict["client_order_id"] = client_order_id
                auth_token = tracking["auth_token"]
                v1_dict["auth_token"] = auth_token
                order_created_at = datetime.strptime(
                    tracking["created_at"], "%d %b %Y, %H:%M"
                )
                v1_dict["order_created_at"] = order_created_at
                if v1_dict.get("status"):
                    if (v1_dict["status"]).get("current_status_time"):
                        v1_dict["status"][
                            "current_status_time"
                        ] = datetime.strptime(
                            v1_dict["status"]["current_status_time"],
                            "%d %b %Y, %H:%M",
                        )
                v1_dict["user_pk"] = tracking["user_pk"]
                v1_dict["created_at"] = datetime.now()
                v1_dict["updated_at"] = datetime.now()
                if v1_dict.get("info") and v1_dict["info"].get("courier_name"):
                    v1_dict.pop("courier_used")
                    v1_dict["courier_used"] = v1_dict["info"]["courier_name"]
                if not v1_dict:
                    err_lis.append(
                        {
                            "err": "tracking_id, client_order_id, auth_token, v1_dict can not be empty",
                            "data": tracking,
                        }
                    )
                    continue
                if v1_dict.get("courier_tracking_id") is not None and len(
                    str(v1_dict.get("courier_tracking_id"))
                ):
                    try:
                        db_res_id = create_tracking_event_to_db(v1_dict)
                        success_list.append(str(db_res_id))
                    except pymongo.errors.DuplicateKeyError as e:
                        # TODO Log Duplicate
                        err_lis.append(
                            {"err": "duplicate awb in mongo", "data": str(e)}
                        )
                        continue
                    # cache_res = create_tracking_event_to_cache(v1_dict)
                    # if "err" in cache_res:
                    #     err_lis.append({"err": cache_res["err"], "data": tracking})
                else:
                    err_lis.append(
                        {
                            "err": "courier_tracking_id is null or blank space",
                            "data": tracking.get("json"),
                        }
                    )
                    continue
                if not db_res_id:
                    err_lis.append(
                        {
                            "err": "Mongodb Error",
                            "data": tracking.get("tracking_id"),
                        }
                    )
                    continue
        if len(err_lis):
            error = True
        trackingsync_logger.info(
            {
                "err_count": len(err_lis),
                "err_list": err_lis,
                "err": error,
                "success_list": success_list,
            }
        )
        return {"success": True} if len(err_lis) == 0 else {"success": False}

    except Exception as e:
        return {"success": False, "err": str(e)}


def push_async_update_tracking(event_dict, manual_update_to_async=False):
    try:
        payload = {}
        res = {}
        if event_dict:
            payload["EDD"] = event_dict["EDD"]
            payload["awb"] = event_dict["awb"]
            payload["received_by"] = event_dict["received_by"]
            payload["scan_type"] = event_dict["scan_type"]
            payload["scan_datetime"] = event_dict["scan_datetime"]
            payload["track_info"] = event_dict["scan_status"]
            payload["track_location"] = event_dict["scan_location"]
            url = "https://async.pickrr.com/track/sync/update/"
            res = POSTRequest(url, [payload])

            if manual_update_to_async:
                try:
                    url = "https://async.pickrr.com/track/sync/update/manual/"
                    headers = {"content-type": "application/json"}
                    res_manual = requests.post(
                        url=url, headers=headers, data=json.dumps([payload])
                    )
                    res_manual_json = res_manual.json()
                    if res_manual_json.get("err"):
                        tracking_api_logger.info(
                            {
                                "err": "manual update on async failed",
                                "mesg": res_manual_json["err"],
                                "awb": event_dict["awb"],
                            }
                        )
                except Exception as e:
                    tracking_api_logger.error(
                        {"err": str(e), "awb": event_dict["awb"]}
                    )
                    pass

        return payload
    except Exception as e:
        event_dict["err"] = str(e)
        error = event_dict
    return event_dict


def prepare_async_single_tracking_event(
    tracking_dict, manual_update_to_async=False
):  # consider_datetime format
    event_dict = {}
    if not bool(tracking_dict):
        return {"err": "tracking_dict empty"}
    if "tracking_array" in tracking_dict and tracking_dict["tracking_array"]:
        event_list = sorted(
            tracking_dict["tracking_array"],
            key=lambda k: k["scan_datetime"],
            reverse=True,
        )
        event_dict.update(event_list[0])
        # push_to_Async_server
    else:
        return {"err": "tracking_array empty"}
    if event_dict:
        if type(event_dict["scan_datetime"]) is not str:
            try:
                event_dict["scan_datetime"] += timedelta(minutes=330)
                event_dict["scan_datetime"] = datetime.strftime(
                    event_dict["scan_datetime"], "%d-%m-%Y %H:%M"
                )
            except Exception as e:
                event_dict["scan_datetime"] = ""
                pushevents_logger.error(
                    {"err": str(e), "awb": tracking_dict["awb"]}
                )
        if tracking_dict.get("edd_stamp"):
            if type(tracking_dict["edd_stamp"]) is str:
                event_dict["EDD"] = tracking_dict["edd_stamp"]
            else:
                try:
                    event_dict["EDD"] += timedelta(minutes=330)
                    event_dict["EDD"] = datetime.strftime(
                        tracking_dict["edd_stamp"], "%d-%m-%Y %H:%M"
                    )
                except Exception as e:
                    event_dict["EDD"] = ""
                    pushevents_logger.info(
                        {"err": str(e), "awb": tracking_dict["awb"]}
                    )
        else:
            event_dict["EDD"] = ""
        if tracking_dict.get("pickup_datetime"):
            if type(tracking_dict["pickup_datetime"]) is str:
                event_dict["pickup_datetime"] = tracking_dict[
                    "pickup_datetime"
                ]
            else:
                try:
                    event_dict["pickup_datetime"] += timedelta(minutes=330)
                    event_dict["pickup_datetime"] = datetime.strftime(
                        tracking_dict["pickup_datetime"], "%d-%m-%Y %H:%M"
                    )
                except Exception as e:
                    event_dict["pickup_datetime"] = ""
                    pushevents_logger.info(
                        {"err": str(e), "awb": tracking_dict["awb"]}
                    )
        else:
            event_dict["pickup_datetime"] = ""
        event_dict["return_waybill"] = (
            tracking_dict["return_waybill"]
            if tracking_dict.get("return_waybill", None) is not None
            else None
        )
        event_dict["received_by"] = (
            tracking_dict["received_by"]
            if tracking_dict.get("received_by", None) is not None
            else None
        )
        event_dict["awb"] = tracking_dict["awb"]
    async_data = push_async_update_tracking(event_dict, manual_update_to_async)
    v1_data = async_data.copy()
    v1_data.update({"pickup_time": event_dict["pickup_datetime"]})
    v1_data.update({"received_by": event_dict["received_by"]})

    res = {
        "async_send_dict": async_data,
        "v1_send_dict": v1_data,
    }
    return res

    # push_async_server_tracker_pull_payload - v1


def create_tracking_info_document_to_mongo_db(tracking_info_dict: dict):
    from decouple import config

    from apps.common.mongo_utils import (
        create_document_to_mongo_db_using_pymongo,
    )

    try:
        obj_id = TrackingInfo.objects.create(**tracking_info_dict)
        return obj_id
    except Exception as e:
        collection_name = config("TRACKING_INFO_COLLECTION_NAME")
        doc_id = create_document_to_mongo_db_using_pymongo(
            tracking_info_dict, collection_name
        )
        return doc_id


def prepare_pull_data(
    courier_list,
    push_events,
    rt_dl_task=False,
    not_picked_within_7=False,
    cancelled_within_30=False,
    is_reverse=False,
):
    courier_data_list_of_list_iterator = get_list_of_awbs_for_data_pull(
        courier_list,
        push_events,
        rt_dl_task,
        not_picked_within_7,
        cancelled_within_30,
        is_reverse,
    )

    if not next(courier_data_list_of_list_iterator):
        return

    for trackinginfo_doc_list in courier_data_list_of_list_iterator:
        courier_account_wise_segregated_lists = dict()
        for trackinginfo_doc in trackinginfo_doc_list:
            awb = trackinginfo_doc["courier_tracking_id"]
            courier = trackinginfo_doc["courier_used"]
            if is_reverse and courier == "delhivery":
                courier = "delhivery_reverse"
            trackinginfo_doc.pop("_id")
            if not awb or not courier:
                continue
            if (
                trackinginfo_doc["courier_used"]
                not in courier_account_wise_segregated_lists
            ):
                courier_account_wise_segregated_lists[courier] = list()
                courier_account_wise_segregated_lists[courier].append(awb)
            else:
                courier_account_wise_segregated_lists[courier].append(awb)
        yield courier_account_wise_segregated_lists


def get_list_of_awbs_for_data_pull(
    courier_list,
    push_events,
    rt_dl_task=False,
    not_picked_within_7=False,
    cancelled_within_30=False,
    is_reverse=False,
):
    from apps.common.dao import get_trackinginfo_documents

    max_waybill_lifetime_days = 180
    expiry_time = datetime.utcnow() - timedelta(days=max_waybill_lifetime_days)

    values_list = {
        "courier_tracking_id": 1,
        "courier_used": 1,
        "is_reverse": 1,
    }
    filter_1 = {
        "$and": [
            {
                "status.current_status_type": {
                    "$nin": ["RTO", "DL", "RTO-OT", "RTD"]
                }
            },
            {"courier_used": {"$nin": courier_list}},
            {"order_created_at": {"$gte": expiry_time}},
            {"is_reverse": False},
        ],
    }

    filter_2 = {
        "$and": [
            {
                "status.current_status_type": {
                    "$nin": ["RTO", "DL", "RTO-OT", "RTD"]
                }
            },
            {"courier_used": {"$in": courier_list}},
            {"order_created_at": {"$gte": expiry_time}},
            {"is_reverse": False},
        ],
    }
    eval_time = datetime.now() - timedelta(days=7)
    filter_3 = {
        "$or": [
            {"status.current_status_type": {"$in": ["RTO", "RTO-OT"]}},
            {
                "$and": [
                    {"status.current_status_type": {"$in": ["DL", "RTD"]}},
                    {"status.current_status_time": {"$gte": eval_time}},
                    {"is_reverse": False},
                ],
            },
        ],
    }

    filter_4 = {
        "$and": [
            {"status.current_status_type": {"$in": ["OP", "OM"]}},
            {"order_created_at": {"$gte": eval_time}},
            {"is_reverse": False},
        ],
    }

    cancellation_time_frame = datetime.now() - timedelta(days=30)
    filter_5 = {
        "$and": [
            {"status.current_status_type": "OC"},
            {"order_created_at": {"$gte": cancellation_time_frame}},
            {"is_reverse": False},
        ],
    }
    reverse_time_frame = datetime.now() - timedelta(days=180)
    filter_6 = {
        "$and": [
            {"is_reverse": True},
            {"order_created_at": {"$gte": reverse_time_frame}},
            {"status.current_status_type": {"$nin": ["DL", "RTD"]}},
        ]
    }

    filter_7 = {
        "$and": [
            {"is_reverse": True},
            {"order_created_at": {"$gte": reverse_time_frame}},
            {"status.current_status_type": {"$in": ["DL", "RTD"]}},
        ]
    }

    if push_events is True:
        filters = filter_1
    else:
        if is_reverse and rt_dl_task:
            filters = filter_7
        elif rt_dl_task:
            filters = filter_3
        elif not_picked_within_7:
            filters = filter_4
        elif cancelled_within_30:
            filters = filter_5
        elif is_reverse:
            filters = filter_6
        else:
            filters = filter_2

    tracking_info_docs = list()
    tracking_info_doc_iterator = get_trackinginfo_documents(
        values_list=values_list, filters=filters
    )

    return tracking_info_doc_iterator


def fetch_updates_from_courier(
    courier_list,
    push_events=True,
    rt_dl_task=False,
    not_picked_within_7=False,
    cancelled_within_30=False,
    is_reverse=False,
):
    from apps.common.tasks import task_hit_courier_and_store_response_in_db
    from apps.common.utils import split_list_into_batches

    courier_wise_segregated_map_list = prepare_pull_data(
        courier_list,
        push_events,
        rt_dl_task,
        not_picked_within_7,
        cancelled_within_30,
        is_reverse,
    )

    for courier_wise_segregated_lists in courier_wise_segregated_map_list:
        for courier_account in courier_wise_segregated_lists:
            cur_acc_awbs_list = courier_wise_segregated_lists[courier_account]
            batches_of_cur_acc_awbs = split_list_into_batches(
                cur_acc_awbs_list
            )
            for batch in batches_of_cur_acc_awbs:
                task_hit_courier_and_store_response_in_db.delay(
                    batch, courier_account
                )


def validate_track_data(track_data):
    try:
        return {} if track_data else {"err": "error in payload"}
    except Exception as e:
        return {"err": str(e)}


def update_status_db(track_data, tracking_obj, is_update):
    err_list = []
    try:
        if track_data["scan_type"] == "CC":
            pushevents_logger.error({"awb": track_data["awb"], "err": "NA"})
            return {"success": False}
        if ("scan_type" not in track_data) and (not track_data["scan_type"]):
            pushevents_logger.error(
                {"awb": track_data["awb"], "err": "Scan type is missing"}
            )
            return {"success": False}
    except Exception as e:
        pushevents_logger.error({"awb": track_data["awb"], "err": str(e)})
        err_list.append(str(e))
        pass
    try:
        try:
            if track_data["track_info"].lower() in BLOCK_NDR_STRINGS:
                if track_data["scan_type"] == "NDR":
                    track_data.update({"scan_type": "OT"})
        except Exception as e:
            pushevents_logger.error({"awb": track_data["awb"], "err": str(e)})
            err_list.append(str(e))
            pass
        scan_datetime_str = track_data.get("scan_datetime", "")
        scan_datetime_obj = None
        tz = pytz.timezone("UTC")
        if scan_datetime_str:
            try:
                scan_datetime_obj = datetime.strptime(
                    scan_datetime_str, "%d-%m-%Y %H:%M"
                )
                scan_datetime_obj = datetime.fromtimestamp(
                    (scan_datetime_obj - timedelta(minutes=330)).timestamp(),
                    tz,
                )
            except Exception as e:
                pushevents_logger.error(
                    {
                        "awb": track_data["awb"],
                        "err": "scan-datetime not found in tracking event",
                    }
                )
                return {"success": False}
        status_map = {
            "current_status_time": scan_datetime_obj,
            "current_status_type": track_data["scan_type"],
            "current_status_body": track_data["track_info"],
            "current_status_location": track_data["track_location"],
        }
        from decouple import config

        col_name = config("TRACKING_INFO_COLLECTION_NAME")
        prev_event = tracking_obj.get("status", None)
        if prev_event:
            prev_event_status_time = prev_event.get(
                "current_status_time", None
            )
            if prev_event_status_time:
                prev_event_status_time = datetime.fromtimestamp(
                    (prev_event_status_time).timestamp(), tz
                )
                if prev_event_status_time < scan_datetime_obj:
                    is_update = True
                    tracking_obj["status"] = status_map
                    track_array = tracking_obj["track_arr"]
                    track_array.reverse()
                    event_obj = map_status_to_event(status_map)
                    event_obj["pickrr_sub_status_code"] = track_data.get(
                        "pickrr_sub_status_code", ""
                    )
                    event_obj["courier_status_code"] = track_data.get(
                        "courier_status_code", ""
                    )
                    track_array.append(event_obj)
                    track_array.reverse()
                    tracking_obj["track_arr"] = track_array
                    tracking_obj["last_update_from"] = "async"
        else:
            is_update = True
            tracking_obj["status"] = status_map
            event_obj = map_status_to_event(status_map)
            event_obj["pickrr_sub_status_code"] = track_data.get(
                "pickrr_sub_status_code", ""
            )
            event_obj["courier_status_code"] = track_data.get(
                "courier_status_code", ""
            )
            tracking_obj["track_arr"] = list()
            tracking_obj["track_arr"].append(event_obj)
            tracking_obj["last_update_from"] = "async"
        if "EDD" in track_data and track_data["EDD"]:
            is_update = True
            try:
                edd_date_obj = datetime.strptime(
                    track_data["EDD"], "%d-%m-%Y %H:%M"
                )
                tracking_obj["edd_stamp"] = edd_date_obj
            except Exception as e:
                tracking_obj["edd_stamp"] = track_data["EDD"]
            tracking_obj["last_update_from"] = "async"
        query_document = {"_id": tracking_obj["_id"]}
        if is_update:
            tracking_obj["updated_at"] = datetime.now()
            replace_one_document(
                filters=query_document,
                updated_document=tracking_obj,
                col_name=col_name,
            )
        return_dict = {"success": True, "_id": str(tracking_obj["_id"])}
        if len(err_list) > 0:
            return_dict["success"] = False
            pushevents_logger.error(
                {"awb": track_data["awb"], "err": str(return_dict["err"])}
            )
        return return_dict

    except Exception as e:
        err_list.append(str(e))
        pushevents_logger.error({"awb": track_data["awb"], "err": err_list})
        return {"success": False}


def hit_courier_and_store_response_in_db(
    batch, courier_account, call_tracker=True, resp={}
):
    from datetime import datetime

    from decouple import config

    from apps.common.dao import update_or_create_courier_response_obj
    from apps.common.mongo_utils import (
        create_document_to_mongo_db_using_pymongo,
    )
    from apps.notification.services import (
        prepare_tracking_events_data_and_send_notification,
    )

    err_list = []
    sucs_list = []
    courier_list = [
        "dtdc",
        "dtdc_air",
        "fedex",
        "fedex_po",
        "fedex_economy",
        "fedex_so",
        "fedex_3kg",
        "fedex_3kg_surface",
    ]
    courier_response_col_name = config("COURIER_RESPONSE_COLLECTION_NAME")
    tracking_col_name = config("TRACKING_INFO_COLLECTION_NAME")
    courier_res_save_query = []
    query = []

    manual_update_to_async = False
    if call_tracker:
        response = tracker(courier_account, batch)
    else:
        response = resp
        manual_update_to_async = True
    if not ("is_success" in response and response["is_success"]):
        err_list.append({"err": "Something went wrong", "batch": batch})
    if "data" in response and response["data"]:
        if (
            "pickrr_response" in response["data"]
            and "courier_response" in response["data"]
        ):
            for i in range(len(response["data"]["pickrr_response"])):
                pickrr_response_ = response["data"]["pickrr_response"][i]
                if type(response["data"]["courier_response"]) is list:
                    courier_response_ = response["data"]["courier_response"][i]
                elif type(response["data"]["courier_response"]) is dict:
                    if "bluedart" not in courier_account:
                        cur_awb = pickrr_response_["awb"]
                        courier_response_ = response["data"][
                            "courier_response"
                        ][cur_awb]
                    else:
                        courier_response_ = response["data"][
                            "courier_response"
                        ]
                else:
                    courier_response_ = response["data"]["courier_response"]
                if "awb" in pickrr_response_:
                    cur_dict = dict(
                        pickrr_response=pickrr_response_,
                        courier_response=courier_response_,
                        tracking_id=pickrr_response_["awb"],
                        courier_tracking_id=pickrr_response_["awb"],
                        updated_at=datetime.now(),
                        courier_used=courier_account,
                    )
                    try:
                        courier_res_save_query = (
                            update_or_create_courier_response_obj(
                                cur_dict,
                                courier_response_col_name,
                                courier_res_save_query,
                            )
                        )
                        query = update_track_arr_in_tracking_info_model(
                            pickrr_response_, query
                        )

                        if courier_account in courier_list:
                            send_bulk_pull_update_async(pickrr_response_)
                        else:
                            update_async_server(
                                pickrr_response_, manual_update_to_async
                            )
                        sucs_list.append(
                            {
                                "awb": pickrr_response_["awb"],
                                "courier_account": courier_account,
                            }
                        )
                    except Exception as e:
                        err_list.append(
                            {
                                "awb": pickrr_response_["awb"],
                                "err": str(e),
                                "courier_account": courier_account,
                            }
                        )
                else:
                    err_list.append(
                        {
                            "err": "AWB not found",
                            "pickrr_response": pickrr_response_,
                            "courier_account": courier_account,
                        }
                    )
            # update_multiple_courier_response(courier_res_save_query, col_name)
            update_multiple_document(
                courier_res_save_query, courier_response_col_name
            )
            update_multiple_document(query, tracking_col_name)
        else:
            responseupdate_logger.error(
                {"err": "Pickrr response not found", "batch": batch}
            )
            err_list.append(
                {"err": "Pickrr response not found", "batch": batch}
            )
    else:
        err_list.append(
            {
                "err": "data not found",
                "batch": batch,
                "courier_account": courier_account,
            }
        )
        responseupdate_logger.error(
            {
                "err": "data not found",
                "batch": batch,
                "courier_account": courier_account,
            }
        )
    responseupdate_logger.info({"err_list": err_list, "sucs_list": sucs_list})
    return {"success": True} if len(err_list) == 0 else {"success": False}


def update_track_arr_in_tracking_info_model(latest_pickrr_response, query):
    courier_tracking_id = latest_pickrr_response["awb"]
    track_arr = latest_pickrr_response["tracking_array"]
    if "final_weight" in latest_pickrr_response:
        courier_input_weight = latest_pickrr_response["final_weight"]
    else:
        courier_input_weight = ""
    if track_arr:
        latest_event = track_arr[0]
        latest_status = map_event_to_status(latest_event)
        query = prepare_update_tracking_info_using_awb_query(
            track_arr,
            latest_status,
            courier_tracking_id,
            courier_input_weight,
            query,
        )
    return query


def map_event_to_status(latest_event):
    latest_status = dict()
    latest_status["current_status_time"] = latest_event.get(
        "scan_datetime", ""
    )
    latest_status["current_status_type"] = latest_event.get("scan_type", "")
    latest_status["current_status_body"] = latest_event.get("scan_status", "")
    latest_status["pickrr_sub_status_code"] = (
        latest_event.get("pickrr_sub_status_code", "") or ""
    )
    latest_status["courier_status_code"] = (
        latest_event.get("courier_status_code", "") or ""
    )
    latest_status["current_status_location"] = latest_event.get(
        "scan_location", ""
    )
    return latest_status


def map_status_to_event(status_dict):
    event_dict = dict()
    event_dict["scan_datetime"] = status_dict.get("current_status_time", "")
    event_dict["scan_type"] = status_dict.get("current_status_type", "")
    event_dict["scan_status"] = status_dict.get("current_status_body", "")
    event_dict["scan_location"] = status_dict.get(
        "current_status_location", ""
    )
    return event_dict


def prepare_update_tracking_info_using_awb_query(
    track_arr: dict,
    latest_status: dict,
    courier_tracking_id: str,
    courier_input_weight,
    query,
):
    from datetime import datetime

    from decouple import config

    filters = {"courier_tracking_id": {"$eq": courier_tracking_id}}
    update_dict = {
        "courier_tracking_id": courier_tracking_id,
        "$set": {
            "status.current_status_time": latest_status["current_status_time"],
            "status.current_status_type": latest_status["current_status_type"],
            "status.current_status_body": latest_status["current_status_body"],
            "status.pickrr_sub_status_code": latest_status[
                "pickrr_sub_status_code"
            ],
            "status.courier_status_code": latest_status["courier_status_code"],
            "status.current_status_location": latest_status[
                "current_status_location"
            ],
            "track_arr": track_arr,
            "courier_input_weight": courier_input_weight,
            "updated_at": datetime.now(),
            "last_update_from": "pull",
        },
    }
    col_name = config("TRACKING_INFO_COLLECTION_NAME")
    query.append(update_dict)
    return query

    # update_one_document(
    #     filters=filters, update_dict=update_dict, col_name=col_name
    # )


def update_async_server(latest_pickrr_response, manual_update_to_async=False):
    from apps.common.tasks import (
        send_pulled_data_to_async,
        send_pulled_data_to_v1,
    )

    res = prepare_async_single_tracking_event(
        latest_pickrr_response, manual_update_to_async
    )
    if "err" in res:
        return res

    if not manual_update_to_async:
        v1_event_update_dict = res["v1_send_dict"]
        async_event_update_dict = res["async_send_dict"]

        # Send single event to v1.
        v1_res = send_pulled_data_to_v1(v1_event_update_dict)
        # Send single event to async.
        event_bridge_dict = {"event_list": [async_event_update_dict]}
        async_res = send_pulled_data_to_async(event_bridge_dict)
        return {"success": True, "v1_res": v1_res, "async_res": async_res}

    return {"success": True}


def converter(o):
    if isinstance(o, datetime):
        return o.strftime("%d %b %Y, %H:%M")


def send_bulk_pull_update_async(pickrr_response):

    url = "www.async.pickrr.com/track/pull_sync/"
    res = {"response": pickrr_response}
    data = json.dumps(res, default=converter)
    request = POSTRequest(url=url, data=data)
    response = request.send()
    return response


def ist_to_utc_converter(ist_time):
    if ist_time is None or ist_time == "":
        return None
    utc_time = ist_time - timedelta(hours=5, minutes=30)
    return utc_time


def tracker_v2(tracking_ids, client_order_id_search=False):
    try:
        response = []
        if not client_order_id_search:
            filters = {
                "$or": [
                    {"courier_tracking_id": {"$in": tracking_ids}},
                    {"tracking_id": {"$in": tracking_ids}},
                ]
            }
        else:
            filters = {"client_order_id": {"$in": tracking_ids}}
        values = {
            "user_pk": 0,
            "last_update_from": 0,
            "updated_at": 0,
            "user_id": 0,
            "ops_profile": 0,
            "billing_zone": 0,
            "info.user_id": 0,
        }

        found_tracking_ids = []
        trackinginfo_collection_name = config("TRACKING_INFO_COLLECTION_NAME")
        track_info_objs_list = batch_document_iterator_v2(
            filters=filters,
            values=values,
            col_name=trackinginfo_collection_name,
        )
        for track_list in track_info_objs_list:
            for track in track_list:
                try:
                    if not track:
                        continue

                    if not track.get("tracking_id"):
                        continue

                    found_tracking_ids.append(str(track.get("tracking_id")))

                    track = validate_tracking_json(track)

                    if "edd_stamp" in track and track["edd_stamp"]:
                        track["edd_stamp"] = prepare_edd_stamp_for_tracker_v2(
                            track["edd_stamp"]
                        )

                    track = filter_tracking_params_for_tracker_v2(track)

                    courier_type = track["courier_used"]
                    courier_type = get_or_prepare_courier_type_for_tracker_v2(
                        str(track.get("courier_used")), track.get("is_reverse")
                    )

                    pickrr_res = tracker(
                        tracking_ids=[track["courier_tracking_id"]],
                        courier_type=courier_type,
                    )
                    if (
                        "is_success" not in pickrr_res
                        or not pickrr_res["is_success"]
                        or "err" in pickrr_res
                    ):
                        tracking_api_logger.info(
                            {
                                "tracking_id": track["courier_tracking_id"],
                                "err": pickrr_res["msg"]
                                if "msg" in pickrr_res
                                else "",
                                "Exception_err": pickrr_res["err"]
                                if "err" in pickrr_res
                                else "",
                            }
                        )
                        try:
                            track_res = hit_async_for_track(
                                track["courier_tracking_id"]
                            )
                            track_res["fetched_from"] = "async"
                            response.append(track_res)
                            continue
                        except Exception as e:
                            track["fetched_from"] = "mongo"
                            response.append(track)
                            continue
                    if not (
                        "data" in pickrr_res
                        and pickrr_res["data"]
                        and "pickrr_response" in pickrr_res["data"]
                        and pickrr_res["data"]["pickrr_response"]
                        and type(pickrr_res["data"]["pickrr_response"]) is list
                        and len(pickrr_res["data"]["pickrr_response"])
                        and "tracking_array"
                        in pickrr_res["data"]["pickrr_response"][0]
                        and type(
                            pickrr_res["data"]["pickrr_response"][0][
                                "tracking_array"
                            ]
                        )
                        is list
                        and len(
                            pickrr_res["data"]["pickrr_response"][0][
                                "tracking_array"
                            ]
                        )
                    ):
                        tracking_api_logger.info(
                            {
                                "tracking_id": track["courier_tracking_id"],
                                "err": "tracking_array empty",
                            }
                        )
                        track_res = hit_async_for_track(
                            track["courier_tracking_id"]
                        )
                        track_res["fetched_from"] = "async"
                        response.append(track_res)
                        continue
                    try:
                        hit_courier_and_store_response_in_db(
                            batch=[track["courier_tracking_id"]],
                            courier_account=track["courier_used"],
                            call_tracker=False,
                            resp=pickrr_res,
                        )
                    except Exception as e:
                        tracking_api_logger.info(
                            {
                                "tracking_id": track["courier_tracking_id"],
                                "err": "save in mongo and async fail",
                            }
                        )
                        pass
                    pickrr_res = pickrr_res["data"]["pickrr_response"][0]
                    track["status"] = map_event_to_status(
                        pickrr_res["tracking_array"][0]
                    )
                    track["status"]["current_status_time"] = track["status"][
                        "current_status_time"
                    ].replace(tzinfo=timezone.utc)
                    if (
                        track["status"]["current_status_type"]
                        in NEW_STATUS_TO_OLD_MAPPING
                    ):
                        track["status"][
                            "current_status_type"
                        ] = NEW_STATUS_TO_OLD_MAPPING[
                            track["status"]["current_status_type"]
                        ]
                    elif track["status"]["current_status_type"] == "UD":
                        track["status"]["current_status_type"] = "OT"
                    track["status"]["current_status_val"] = ""
                    track["track_arr"] = pickrr_res["tracking_array"]
                    track["track_arr"] = map_track_arr(track["track_arr"])
                    track["track_arr"] = track["track_arr"][::-1]
                    if "courier_parent_name" in track:
                        track["courier_used"] = track["courier_parent_name"]
                    track["fetched_from"] = "pull"
                    response.append(track)
                except Exception as e:
                    tracking_api_logger.error({"err": str(e)})
                    pass
        rem_track_ids = [
            item for item in tracking_ids if item not in found_tracking_ids
        ]
        for track_id in rem_track_ids:
            try:
                track_res = hit_async_for_track(track_id)
                track_res["fetched_from"] = "async"
                response.append(track_res)
            except Exception as e:
                temp_dict = {
                    "err": "Tracking ID not found",
                    "tracking_id": track_id,
                }
                response.append(temp_dict)
                continue
        if len(response) > 1:
            return {"response_list": response}
        return response[0]
    except Exception as e:
        return {"err": str(e)}


def map_track_arr(track_list):
    from collections import OrderedDict

    return_arr = []
    temp_dict = OrderedDict()
    for track_dict in track_list:
        try:
            status_dict = {
                "status_body": track_dict.get("scan_status"),
                "status_time": track_dict.get("scan_datetime").replace(
                    tzinfo=timezone.utc
                ),
                "pickrr_status": PICKRR_STATUS_CODE_MAPPING[
                    track_dict.get("scan_type")
                ],
                "status_location": track_dict.get("scan_location"),
                "courier_status_code": track_dict.get("courier_status_code"),
                "pickrr_sub_status_code": track_dict.get(
                    "pickrr_sub_status_code"
                ),
            }
            # if track_dict['scan_type'] in temp_dict:
            #     temp_dict[track_dict['scan_type']].append(status_dict)
            # else:
            #     temp_dict[track_dict['scan_type']] = [status_dict]
            return_dict = {}
            return_dict["status_name"] = track_dict.get("scan_type")
            if return_dict["status_name"] == "UD":
                return_dict["status_name"] = "NDR"
            return_dict["status_array"] = [status_dict]
            return_arr.append(return_dict)
        except Exception as e:
            continue
    # for k,v in temp_dict.items():
    #     return_dict = {}
    #     return_dict['status_name'] = k
    #     return_dict['status_array'] = v
    #     return_arr.append(return_dict)

    return return_arr


def check_show_details_client(auth_token):
    url = "https://async.pickrr.com/track/check/show/details/client/"
    headers = {"Content-Type": "application/json", "Authorization": auth_token}
    try:
        response = requests.get(url=url, headers=headers).json()
        if "is_success" in response:
            return response["is_success"]
        else:
            return False
    except Exception as e:
        return False


def validate_tracking_json(track_dict):
    try:
        for key in get_track_parent_keys():
            if key not in track_dict:
                if key == "info":
                    track_dict[key] = {}
                elif key == "status":
                    track_dict[key] = {
                        "current_status_time": "",
                        "current_status_type": "",
                        "received_by": "",
                        "current_status_body": "",
                        "current_status_location": "",
                        "current_status_val": "",
                    }
                else:
                    track_dict[key] = ""
        for key in get_track_info_keys():
            if key not in track_dict["info"]:
                track_dict["info"].update({key: ""})
    except Exception as e:
        pass
    return track_dict


def hit_async_for_track(tracking_id):
    try:
        url = "https://async.pickrr.com/track/tracking/?tracking_id=" + str(
            tracking_id
        )
        headers = {"Content-Type": "application/json"}
        response = requests.get(url=url, headers=headers).json()
        return response
    except Exception as e:
        pass


def prepare_edd_stamp_for_tracker_v2(edd_stamp):
    try:
        if type(edd_stamp) == str:
            edd_stamp_date_obj = datetime.strptime(edd_stamp, "%d %b %Y")
            edd_stamp = edd_stamp_date_obj.replace(
                hour=18,
                minute=29,
                second=59,
                tzinfo=timezone.utc,
            )
        else:
            edd_stamp = ist_to_utc_converter(
                edd_stamp.replace(tzinfo=timezone.utc)
            )
    except Exception as e:
        edd_stamp = ""
        pass
    return edd_stamp


def filter_tracking_params_for_tracker_v2(track_dict):
    track_dict["show_details"] = True
    if "label_logo" in track_dict:
        track_dict["logo"] = track_dict.get("label_logo")
        del track_dict["label_logo"]
    if "auth_token" in track_dict and track_dict["auth_token"]:
        auth_token = track_dict["auth_token"]
        if auth_token in ["6c96b95bb99c767660312f5fd97c558732735"]:
            track_dict["edd_stamp"] = track_dict["edd_stamp"] + timedelta(
                days=1
            )
        track_dict["auth_token"] = ""
        if not check_show_details_client(auth_token):
            track_dict["show_details"] = False
            track_dict["company_name"] = ""
            track_dict["logo"] = ""
        del track_dict["auth_token"]
    track_dict["web_address"] = ""
    if "_id" in track_dict:
        del track_dict["_id"]
    try:
        track_dict["info"]["to_phone_number"] = ""
        track_dict["info"]["to_address"] = ""
        track_dict["info"]["invoice_value"] = ""
        track_dict["info"]["to_email"] = ""
        track_dict["info"]["to_name"] = ""
    except Exception as e:
        pass
    return track_dict


def get_or_prepare_courier_type_for_tracker_v2(courier_type, is_reverse):
    try:
        if is_reverse is True and "delhivery" in courier_type:
            courier_type = courier_type + str("_reverse")
    except Exception as e:
        pass
    return courier_type


def get_pincode_data(pincodes):
    try:
        filters = {"pincode": {"$in": pincodes}}
        pincode_collection_name = config("PINCODE_COLLECTION_NAME")
        pincode_objs_list = batch_document_iterator_v2(
            filters=filters,
            col_name=pincode_collection_name,
        )
        response_list = []
        # found_pincodes = []
        for pincode_obj in pincode_objs_list:
            for pincode_dict in pincode_obj:
                # found_pincodes.append(pincode_dict['pincode'])
                if "_id" in pincode_dict:
                    del pincode_dict["_id"]
                response_list.append(pincode_dict)
        return_dict = {"is_success": True, "data": response_list, "err": ""}
        # rem_pincodes = [p for p in pincodes if not p in found_pincodes]
        # for pin in rem_pincodes:
        if len(response_list) < 1:
            return_dict["is_success"] = False
            return_dict["err"] = "Pincode not found"
            return return_dict
        return return_dict
    except Exception as e:
        return {"is_success": False, "data": [], "err": str(e)}
