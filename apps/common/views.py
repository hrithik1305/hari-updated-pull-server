from django.http import JsonResponse
from rest_framework import status
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.common.tasks import track_pull_update_webhook_async
from pickrr_tracker.loggers import pushevents_logger

from .utils import read_post_json

# Create your views here.


class TrackPushSyncView(APIView):
    """
    API to sync track data from async on push event,
    """

    @staticmethod
    def validate_request_from_async_server(request):
        data = request.data
        try:
            if (
                data["source"] != "async-server"
                or data["detail-type"] != "pushupdatesfromcourier"
                or not data["detail"]
            ):
                return False
        except KeyError:
            return False
        return True

    def post(self, request: Request):
        """
        track_data = {
            "awb": "",
            "scan_type": "",
            "scan_datetime": "",
            "track_info": "",
            "track_location": "",
            "received_by": "",
            "pickup_datetime": "",
            "EDD": "",
            "pickrr_status": "",
            "pickrr_sub_status_code": "",
            "courier_status_code": ""
        }
        """
        try:
            if not self.validate_request_from_async_server(request):
                return Response({"success": True})
            track_data = request.data
            track_data = track_data["detail"]
            res = track_pull_update_webhook_async.delay(track_data)
            return Response({"success": True})
        except Exception as e:
            pushevents_logger.error({"err": str(e)})
            return Response({"err": str(e)})


class TrackPushBulkSyncView(APIView):
    """
    API to sync track data from async on push event for bulk,
    """

    def post(self, request: Request):
        try:
            if request.method != "POST":
                return Response({"err": "Invalid Request"})
            track_data_list = read_post_json(request.body)
            from .tasks import track_pull_update_bulk_webhook_async

            track_pull_update_bulk_webhook_async.delay(track_data_list)
            return Response({"success": True})
        except Exception as e:
            return Response({"err": str(e)})
