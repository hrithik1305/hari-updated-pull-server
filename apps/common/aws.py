import json

import boto3
from botocore.client import Config
import decouple

eb_aws_region_name = decouple.config("EB_REGION_NAME")
eb_aws_access_key = decouple.config("EB_ACCESS_KEY")
eb_aws_secret_access_key = decouple.config("EB_SECRET_ACCESS_KEY")

config_eb = Config(connect_timeout=5, retries={"max_attempts": 1})
client = boto3.client(
    "events",
    region_name=eb_aws_region_name,
    aws_access_key_id=eb_aws_access_key,
    aws_secret_access_key=eb_aws_secret_access_key,
    config=config_eb,
)

def send_data_to_event_bridge(
    data_to_send_dict: dict,
    event_bus_source: str,
    event_bus_detail_type: str,
    event_bus_name: str,
):
    data_to_send_str = json.dumps(data_to_send_dict)
    response = client.put_events(
        Entries=[
            {
                "Source": event_bus_source,
                "DetailType": event_bus_detail_type,
                "Detail": data_to_send_str,
                "EventBusName": event_bus_name,
            }
        ]
    )

    return {"eventbrige_resp": str(response)}
