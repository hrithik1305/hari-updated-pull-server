import urllib

import requests
from celery import app
from rest_framework import serializers, status
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.common.courier_mapping import courier_tracker_map
from apps.common.services import get_pincode_data, tracker, tracker_v2
from apps.common.tasks import *
from apps.common.utils import read_post_json

from .serializers import (
    BulkAsyncManualUpdateSerializer,
    BulkTrackerSerializer,
    PincodeSerializer,
    TrackingSerializer,
    UpdateAsyncManualClientSerializer,
)


class BulkTrackerView(APIView):
    def post(self, request, format=None):
        serializer = BulkTrackerSerializer(data=request.data)
        if serializer.is_valid():
            courier_type = serializer.validated_data.get("courier_type")
            tracking_ids = serializer.validated_data.get("tracking_ids")
            # orders_created_at = serializer.validated_data.get(
            #     "orders_created_at"
            # )
            res = tracker(courier_type, tracking_ids)
            return Response(res)

        return Response(
            {"is_success": False, "msg": None, "data": serializer.errors}
        )


class TrackingSyncView(APIView):
    """
    API to receive data from v1 on order placing,
    """

    def post(self, request: Request):
        params = read_post_json(request.body)
        from apps.common.tasks import tracking_sync_task

        tracking_sync_task.delay(params)
        # res = service.tracking_sync(params)
        return Response({"success": True}, status=status.HTTP_200_OK)


class ReportSyncPullView(APIView):
    """
    API to sync data from report mongo to pull mongo
    """

    def post(self, request: Request):
        params = read_post_json(request.body)
        from apps.common.tasks import report_mongo_sync_pull_mongo_task

        report_mongo_sync_pull_mongo_task.delay(params)
        # res = service.tracking_sync(params)
        return Response({"success": True}, status=status.HTTP_200_OK)


class DemoTaskSchedulerView(APIView):
    """
    Demo API to start pulling data on server
    """

    def get(self, request: Request):
        from apps.common.services import fetch_updates_from_courier

        fetch_updates_from_courier()
        return Response({"success": True}, status=status.HTTP_200_OK)


class BulkTrackerV2View(APIView):
    def get(self, request, format=None):
        serializer = TrackingSerializer(data=request.query_params)
        if serializer.is_valid():
            validated_data_dict = serializer.validated_data

            tracking_ids = list()
            client_order_id_search = False
            if validated_data_dict.get("client_order_id"):
                # tracking_ids = validated_data_dict.get("client_order_id")
                # client_order_id_search = True
                client_order_id = validated_data_dict.get("client_order_id")
                client_order_ids_list_str_comma_separated = ""
                for order_id in client_order_id:
                    client_order_ids_list_str_comma_separated += order_id
                    client_order_ids_list_str_comma_separated += ","
                client_order_ids_list_str_comma_separated = (
                    client_order_ids_list_str_comma_separated[:-1]
                )
                url = (
                    "https://async.pickrr.com/track/tracking/?client_order_id="
                    + str(client_order_ids_list_str_comma_separated)
                )
                headers = {"content-type": "application/json"}
                res = requests.get(url=url, headers=headers, timeout=120)
                return Response(res.json())
            elif validated_data_dict.get("tracking_id"):
                tracking_ids = validated_data_dict.get("tracking_id")
            else:
                return Response(
                    {"err": "Invalid Filters"}, status=status.HTTP_200_OK
                )

            tracking_ids = tracking_ids[0].split(",")
            if len(tracking_ids) > 30:
                return Response(
                    {
                        "err": "Can't process more than 30 tracking_ids at a time"
                    }
                )
            res = tracker_v2(tracking_ids, client_order_id_search)
            return Response(res)
        return Response(
            {"is_success": False, "msg": None, "data": serializer.errors}
        )


class PincodeServiceView(APIView):
    def get(self, request):
        try:
            serializer = PincodeSerializer(data=request.query_params)
            if serializer.is_valid():
                data = serializer.validated_data
                pincode = data.get("pincode")
                if pincode > 999999 or pincode < 100000:
                    return Response(
                        {
                            "is_success": False,
                            "data": [],
                            "err": "Invalid Pincode",
                        }
                    )
                res = get_pincode_data([pincode])
                return Response(res)
            else:
                return Response(
                    {"is_success": False, "data": [], "err": serializer.errors}
                )
        except Exception as e:
            return Response({"is_success": False, "data": [], "err": str(e)})


class BulkUpdateToAsyncManualView(APIView):
    def get(self, request: Request):
        from datetime import datetime, timedelta

        from apps.common.tasks import task_bulk_update_async_manual

        try:
            serialized_data = BulkAsyncManualUpdateSerializer(
                data=request.query_params
            )
            if serialized_data.is_valid():
                data = serialized_data.validated_data
                from_date_str = data.get("from_date")
                to_date_str = data.get("to_date")
                try:
                    from_date_obj = datetime.strptime(
                        from_date_str, "%Y-%m-%d"
                    )
                    to_date_obj = datetime.strptime(to_date_str, "%Y-%m-%d")
                except Exception as e:
                    return Response(
                        {
                            "is_success": False,
                            "err": "Date Format should be YYYY-MM-DD",
                        }
                    )
                if from_date_obj > to_date_obj:
                    return Response(
                        {
                            "is_success": False,
                            "err": "From Date should be before To Date",
                        }
                    )

                to_date_obj += timedelta(days=1)
                to_date_obj -= timedelta(microseconds=1)

                if (to_date_obj - from_date_obj).days > 2:
                    return Response(
                        {
                            "is_success": False,
                            "err": "Max Date Range is 2 days",
                        }
                    )

                from_date_obj_str = datetime.strftime(
                    from_date_obj, "%d-%b-%Y (%H:%M:%S.%f)"
                )
                to_date_obj_str = datetime.strftime(
                    to_date_obj, "%d-%b-%Y (%H:%M:%S.%f)"
                )

                task_bulk_update_async_manual.delay(
                    from_date_obj_str, to_date_obj_str
                )

                return Response(
                    {
                        "is_success": True,
                        "from_date": from_date_obj,
                        "to_date": to_date_obj,
                        "err": None,
                    }
                )

            else:
                return Response(
                    {"is_success": False, "err": serialized_data.errors}
                )
        except Exception as e:
            return Response({"is_success": False, "err": str(e)})


class UpdateAsyncManualClient(APIView):
    def get(self, request: Request):
        try:
            from datetime import datetime, timedelta

            from apps.common.tasks import task_update_async_manual_client

            serialized_data = UpdateAsyncManualClientSerializer(
                data=request.query_params
            )

            if serialized_data.is_valid():
                data = serialized_data.validated_data
                from_date_str = data.get("from_date")
                to_date_str = data.get("to_date")
                auth_token = data.get("auth_token")
                try:
                    from_date_obj = datetime.strptime(
                        from_date_str, "%Y-%m-%d"
                    )
                    to_date_obj = datetime.strptime(to_date_str, "%Y-%m-%d")
                except Exception as e:
                    return Response(
                        {
                            "is_success": False,
                            "err": "Date Format should be YYYY-MM-DD",
                        }
                    )
                if from_date_obj > to_date_obj:
                    return Response(
                        {
                            "is_success": False,
                            "err": "From Date should be before To Date",
                        }
                    )

                to_date_obj += timedelta(days=1)
                to_date_obj -= timedelta(microseconds=1)

                from_date_obj_str = datetime.strftime(
                    from_date_obj, "%d-%b-%Y (%H:%M:%S.%f)"
                )
                to_date_obj_str = datetime.strftime(
                    to_date_obj, "%d-%b-%Y (%H:%M:%S.%f)"
                )

                kwargs = {
                    "from_date_str": from_date_obj_str,
                    "to_date_str": to_date_obj_str,
                    "auth_token": auth_token,
                }

                task_update_async_manual_client.delay(**kwargs)

                return Response(
                    {"is_success": True, "err": None},
                    status=status.HTTP_200_OK,
                )

            else:
                return Response(
                    {"is_success": False, "err": serialized_data.errors}
                )

        except Exception as e:
            return {"err": str(e), "is_success": False}
