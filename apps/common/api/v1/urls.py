from django.urls import include, path

from .views import (
    BulkTrackerV2View,
    BulkTrackerView,
    BulkUpdateToAsyncManualView,
    DemoTaskSchedulerView,
    PincodeServiceView,
    ReportSyncPullView,
    TrackingSyncView,
    UpdateAsyncManualClient,
)

urlpatterns = [
    path("bulk-tracker/", BulkTrackerView.as_view(), name="bulk_tracker"),
    path("sync/", TrackingSyncView.as_view()),
    path("demo-task-scheduler-api/", DemoTaskSchedulerView.as_view()),
    path("report-sync-pull/", ReportSyncPullView.as_view()),
    path(
        "bulk-tracker/v2/", BulkTrackerV2View.as_view(), name="bulk_tracker_v2"
    ),
    path(
        "pincode/service/",
        PincodeServiceView.as_view(),
        name="pincode_service",
    ),
    path(
        "bulk-update-to-async-manual/", BulkUpdateToAsyncManualView.as_view()
    ),
    path("update-async-manual/client/", UpdateAsyncManualClient.as_view()),
]
