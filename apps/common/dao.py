import json

from apps.common.redis import (
    check_redis_connection,
    delete_from_cache,
    update_the_cache,
)


def create_tracking_event_to_db(v1_dict):
    from decouple import config

    from apps.common.mongo_utils import (
        create_document_to_mongo_db_using_pymongo,
    )

    try:
        col_name = config("TRACKING_INFO_COLLECTION_NAME")
        obj_id = create_document_to_mongo_db_using_pymongo(v1_dict, col_name)
        return obj_id
    except Exception as e:
        # TODO: log this error
        raise e


def create_tracking_event_to_cache(v1_dict):
    try:
        if v1_dict is None:
            v1_dict = {}
        if check_redis_connection():
            update_the_cache(v1_dict["tracking_id"], json.dumps(v1_dict), 7)
            if v1_dict["client_order_id"]:
                update_the_cache(
                    v1_dict["client_order_id"],
                    json.dumps(v1_dict["tracking_id"]),
                    15,
                )
        return {"success": True}
    except Exception as e:
        delete_from_cache(v1_dict["tracking_id"])
        delete_from_cache(v1_dict["client_order_id"])
        return {"err": str(e)}


def get_trackinginfo_documents(values_list: dict, filters: dict):
    from decouple import config

    from apps.common.mongo_utils import (
        fetch_documents_from_mongo_using_filters_and_values,
    )

    trackinginfo_collection_name = config("TRACKING_INFO_COLLECTION_NAME")
    doc_iterator = fetch_documents_from_mongo_using_filters_and_values(
        filters=filters,
        values=values_list,
        collection_name=trackinginfo_collection_name,
    )
    return doc_iterator


def update_or_create_courier_response_obj(
    cur_dict, col_name, courier_res_save_query
):
    """
    prepares query list
    """
    from decouple import config

    from apps.common.mongo_utils import update_or_create_document_in_mongo

    try:
        courier_response_col_name = config("COURIER_RESPONSE_COLLECTION_NAME")
        # filters = {"courier_tracking_id": cur_dict["courier_tracking_id"]}
        update_dict = {
            "courier_tracking_id": cur_dict["courier_tracking_id"],
            "$set": cur_dict,
        }
        courier_res_save_query.append(update_dict)
        # doc_id = update_or_create_document_in_mongo(
        #     update_dict, courier_response_col_name, filters
        # )
        return courier_res_save_query
    except Exception as e:
        raise e


def get_tracking_info_doc_using_tracking_id(
    tracking_id, values, collection_name
):
    from apps.common.mongo_utils import (
        fetch_single_document_from_mongo_using_filters_and_values,
    )

    filters = {"tracking_id": {"$eq": tracking_id}}
    doc = fetch_single_document_from_mongo_using_filters_and_values(
        filters, values, collection_name
    )
    if doc:
        return doc
    else:
        return None
