from datetime import datetime, timedelta

from celery import shared_task
from celery.contrib import rdb
from django.http import JsonResponse
from django.template.defaultfilters import date
from rest_framework import status
from rest_framework.response import Response

from apps.common.services import validate_track_data
from pickrr_tracker.loggers import pushevents_logger

from .models import TrackingInfo
from .services import update_status_db


@shared_task
def send_pulled_data_to_async(data_to_send_dict: dict):
    from .services import send_pulled_data_to_async_via_event_bridge

    return send_pulled_data_to_async_via_event_bridge(data_to_send_dict)


@shared_task
def send_pulled_data_to_v1(data_to_send_dict: dict):
    from .services import send_pulled_data_to_v1_via_event_bridge

    return send_pulled_data_to_v1_via_event_bridge(data_to_send_dict)


@shared_task
def tracking_sync_task(params):
    from .services import tracking_sync

    res = tracking_sync(params)
    return res


@shared_task
def report_mongo_sync_pull_mongo_task(params):
    from .services import report_data_sync_pull

    res = report_data_sync_pull(params)
    return res


@shared_task
def track_pull_update_webhook_async(track_data):
    err_list = []
    try:
        is_update = False
        if not ("EDD" in track_data and track_data["EDD"]):
            track_data["EDD"] = ""
        if not ("scan_datetime" in track_data and track_data["scan_datetime"]):
            track_data["scan_datetime"] = ""

        validations = validate_track_data(track_data)
        if "err" in validations:
            pushevents_logger.error(
                {"err": validations, "awb": str(track_data["awb"])}
            )
            return {"success": False}
        # if (("scan_type" in track_data)and track_data["scan_type"]and (track_data["scan_type"] in ["PP","CC"])):
        # return {"success": True}
        from decouple import config

        from .dao import get_tracking_info_doc_using_tracking_id

        col_name = config("TRACKING_INFO_COLLECTION_NAME")

        tracking_id = track_data["awb"]
        tracking_obj = get_tracking_info_doc_using_tracking_id(
            tracking_id, {}, col_name
        )
        if not tracking_obj:
            pushevents_logger.error({"err": "Tracking object not found"})
            return {"success": False}
        return update_status_db(track_data, tracking_obj, is_update)
    except Exception as e:
        pushevents_logger.error({"err": str(e), "awb": str(track_data["awb"])})
        err_list.append(str(e))
        pass
    pushevents_logger.error(
        {"err": str(err_list), "awb": str(track_data["awb"])}
    )
    return {"success": False}


@shared_task
def track_pull_update_bulk_webhook_async(
    track_data_list,
):  # Currently not using this Endpoint, Return only success true or false and log response to prevent celery extra space
    errors_dict = {}
    from .services import validate_track_data

    for track_data in track_data_list:
        try:
            if "EDD" in track_data and track_data["EDD"]:
                track_data["EDD"] = datetime.strptime(
                    track_data["EDD"], "%d-%m-%Y %H:%M"
                )
            else:
                track_data["EDD"] = None
            if "scan_datetime" in track_data and track_data["scan_datetime"]:
                track_data["scan_datetime"] = datetime.strptime(
                    track_data["scan_datetime"], "%d-%m-%Y %H:%M"
                )

            validations = validate_track_data(track_data)
            if "err" in validations:
                errors_dict[track_data["awb"]] = {
                    "error": validations["err"],
                    "data": track_data,
                }
                pass
            if (
                ("scan_type" in track_data)
                and track_data["scan_type"]
                and (track_data["scan_type"] in ["PP", "CC"])
            ):
                pass
            track_pull_update_webhook_async.delay(track_data)

        except Exception as e:
            errors_dict[track_data["awb"]] = {
                "error": str(e),
                "data": track_data,
            }
            pass

    # if errors_dict:
    #     send_email('Errors in aync bulk push track webhook',
    #                str(errors_dict),
    #                ['laxman@pickrr.com', 'gaurav@pickrr.com'])


@shared_task
def task_hit_courier_and_store_response_in_db(batch, courier_account):
    from apps.common.services import hit_courier_and_store_response_in_db

    return hit_courier_and_store_response_in_db(batch, courier_account)


@shared_task
def pull_updates_from_courier_cronjob():
    from apps.common.services import fetch_updates_from_courier

    courier_list = [
        "dtdc",
        "dtdc_air",
        "fedex",
        "fedex_po",
        "fedex_economy",
        "fedex_so",
        "fedex_3kg",
        "fedex_3kg_surface",
    ]
    try:
        fetch_updates_from_courier(courier_list)
        return {"success": True}
    except Exception as e:
        return {"success": False}


@shared_task
def pull_updates_from_courier_without_push_event_cronjob():
    from apps.common.services import fetch_updates_from_courier

    courier_list = [
        "dtdc",
        "dtdc_air",
        "fedex",
        "fedex_po",
        "fedex_economy",
        "fedex_so",
        "fedex_3kg",
        "fedex_3kg_surface",
    ]
    push_event = False
    try:
        fetch_updates_from_courier(courier_list, push_event)
        return {"success": True}
    except Exception as e:
        return {"success": False}


@shared_task
def pull_updates_for_delivered_return_events():
    from apps.common.services import fetch_updates_from_courier

    courier_list = []
    push_event = False
    rt_dl_task = True
    try:
        fetch_updates_from_courier(courier_list, push_event, rt_dl_task)
        return {"success": True}
    except Exception as e:
        return {"success": False}


@shared_task
def pull_updates_for_not_picked_orders_within_7_days():
    from apps.common.services import fetch_updates_from_courier

    courier_list = []
    push_event = False
    rt_dl_task = False
    not_picked_within_7 = True

    try:
        fetch_updates_from_courier(
            courier_list, push_event, rt_dl_task, not_picked_within_7
        )
    except Exception as e:
        pass


@shared_task
def pull_updates_for_cancelled_orders_within_30_days():
    from apps.common.services import fetch_updates_from_courier

    courier_list = []
    push_event = False
    rt_dl_task = False
    not_picked_within_7 = False
    cancelled_within_30 = True

    try:
        fetch_updates_from_courier(
            courier_list,
            push_event,
            rt_dl_task,
            not_picked_within_7,
            cancelled_within_30,
        )
    except Exception as e:
        pass


@shared_task
def pull_updates_for_reverse_tracking_id():
    from apps.common.services import fetch_updates_from_courier

    courier_list = []
    push_event = False
    rt_dl_task = False
    not_picked_within_7 = False
    cancelled_within_30 = False
    is_reverse = True

    try:
        fetch_updates_from_courier(
            courier_list,
            push_event,
            rt_dl_task,
            not_picked_within_7,
            cancelled_within_30,
            is_reverse,
        )
    except Exception as e:
        pass


@shared_task
def pull_updates_for_delivered_reverse_tracking_id():
    from apps.common.services import fetch_updates_from_courier

    courier_list = []
    push_event = False
    rt_dl_task = True
    not_picked_within_7 = False
    cancelled_within_30 = False
    is_reverse = True

    try:
        fetch_updates_from_courier(
            courier_list,
            push_event,
            rt_dl_task,
            not_picked_within_7,
            cancelled_within_30,
            is_reverse,
        )
    except Exception as e:
        pass


@shared_task
def task_bulk_update_async_manual(from_date_obj_ist_str, to_date_obj_ist_str):
    from decouple import config

    from apps.common.mongo_utils import batch_document_iterator_v2
    from apps.common.services import ist_to_utc_converter, tracker_v2

    from_date_obj_ist = datetime.strptime(
        from_date_obj_ist_str, "%d-%b-%Y (%H:%M:%S.%f)"
    )
    to_date_obj_ist = datetime.strptime(
        to_date_obj_ist_str, "%d-%b-%Y (%H:%M:%S.%f)"
    )
    from_date_obj_utc = ist_to_utc_converter(from_date_obj_ist)
    to_date_obj_utc = ist_to_utc_converter(to_date_obj_ist)

    col_name = config("TRACKING_INFO_COLLECTION_NAME")
    filters = {
        "status.current_status_time": {
            "$gte": from_date_obj_utc,
            "$lte": to_date_obj_utc,
        }
    }
    values = {"courier_tracking_id": 1}

    batch_doc_itr = batch_document_iterator_v2(filters, col_name, values)

    for batch in batch_doc_itr:
        list_of_ids = []
        for doc in batch:
            list_of_ids.append(doc.get("courier_tracking_id"))
        tracker_v2(list_of_ids)
    return


@shared_task
def task_update_async_manual_client(**kwargs):
    from decouple import config

    from apps.common.constants import UPDATE_ASYNC_MANUAL_CLIENTS_LIST
    from apps.common.mongo_utils import batch_document_iterator_v2
    from apps.common.services import ist_to_utc_converter, update_async_server

    auth_token = kwargs.get("auth_token")
    from_date_str = kwargs.get("from_date_str")
    to_date_str = kwargs.get("to_date_str")

    today_now = datetime.now()

    to_date_obj_ist = datetime(
        year=today_now.year, month=today_now.month, day=today_now.day
    )
    from_date_obj_ist = to_date_obj_ist - timedelta(days=1)

    to_date_obj_utc = ist_to_utc_converter(to_date_obj_ist)
    from_date_obj_utc = ist_to_utc_converter(from_date_obj_ist)

    auth_token_list = UPDATE_ASYNC_MANUAL_CLIENTS_LIST

    col_name = config("TRACKING_INFO_COLLECTION_NAME")

    if not (auth_token and from_date_str and to_date_str):
        filters = {
            "$and": [
                {"auth_token": {"$in": auth_token_list}},
                {
                    "status.current_status_time": {
                        "$gte": from_date_obj_utc,
                        "$lte": to_date_obj_utc,
                    }
                },
            ]
        }
    else:
        from_date_obj_ist = datetime.strptime(
            from_date_str, "%d-%b-%Y (%H:%M:%S.%f)"
        )
        to_date_obj_ist = datetime.strptime(
            to_date_str, "%d-%b-%Y (%H:%M:%S.%f)"
        )
        from_date_obj_utc = ist_to_utc_converter(from_date_obj_ist)
        to_date_obj_utc = ist_to_utc_converter(to_date_obj_ist)

        filters = {
            "$and": [
                {"auth_token": {"$eq": auth_token}},
                {
                    "status.current_status_time": {
                        "$gte": from_date_obj_utc,
                        "$lte": to_date_obj_utc,
                    }
                },
            ]
        }

    values = {"courier_tracking_id": 1}

    batch_doc_itr = batch_document_iterator_v2(filters, col_name, values)

    list_of_ids = []

    for batch in batch_doc_itr:
        for doc in batch:
            list_of_ids.append(doc.get("courier_tracking_id"))

    courier_response_col_name = config("COURIER_RESPONSE_COLLECTION_NAME")
    filters_2 = {"courier_tracking_id": {"$in": list_of_ids}}
    values_2 = {"pickrr_response": 1}

    batch_doc_itr_2 = batch_document_iterator_v2(
        filters_2, courier_response_col_name, values_2
    )

    for batch2 in batch_doc_itr_2:
        for doc2 in batch2:
            cur_pickrr_resp = doc2.get("pickrr_response")
            update_async_server(cur_pickrr_resp, manual_update_to_async=True)

    return
