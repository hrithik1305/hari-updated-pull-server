from apps.common.utils import POSTRequest

from .utils import get_request_data


def tracker(
    courier_type: str, waybills: list, unique_id="", attempt=""
) -> dict:
    final_courier_response = []
    final_tracking_array = []
    for waybill in waybills:
        courier_response, pickrr_response = get_request_data(
            courier_type, waybill, unique_id, attempt
        )
        final_tracking_array.append(pickrr_response)
        final_courier_response.append(courier_response)
    return {
        "is_success": True,
        "msg": None,
        "data": {
            "pickrr_response": final_tracking_array,
            "courier_response": final_courier_response,
        },
    }
    # url = "https://ws.fedex.com:443/web-services/track"
    # request = POSTRequest(
    #     url=url, data=request_data, json_dumps=False, xml=True
    # )
    # return request.send()
