from django.apps import AppConfig


class FedexConfig(AppConfig):
    name = "apps.fedex"
