import copy
import re

import requests
from dateutil import parser
from dateutil.relativedelta import relativedelta
from django.conf import settings

from apps.common.utils import xml_to_dict
from pickrr_tracker.loggers import fedex_logger


def get_request_data(courier_type: str, awb, unique_id, attempt):
    credentials_map = {
        "fedex": fedex_tracker,
        "fedex_po": fedex_tracker,
        "fedex_3kg": fedex_tracker,
        "fedex_3kg_surface": fedex_tracker,
        "fedex_economy": fedex_surface_tracker,
        "fedex_so": fedex_surface_tracker,
    }
    return credentials_map[courier_type](awb, unique_id, attempt)


def generate_fedex_tracker_body(awb, unique_id="", attempt=""):
    key = settings.FEDEX_KEY
    password = settings.FEDEX_PASSWORD
    account_number = settings.FEDEX_ACCOUNT_NUMBER
    meter_number = settings.FEDEX_METER_NUMBER
    if unique_id:
        body = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v12="http://fedex.com/ws/track/v12">
                       <soapenv:Header/>
                       <soapenv:Body>
                          <v12:TrackRequest>
                             <v12:WebAuthenticationDetail>
                                <v12:UserCredential>
                                   <v12:Key>%s</v12:Key>
                                   <v12:Password>%s</v12:Password>
                                </v12:UserCredential>
                             </v12:WebAuthenticationDetail>
                             <v12:ClientDetail>
                                <v12:AccountNumber>%s</v12:AccountNumber>
                                <v12:MeterNumber>%s</v12:MeterNumber>
                             </v12:ClientDetail>
                             <v12:TransactionDetail>
                                <v12:CustomerTransactionId>Track By Number_v14</v12:CustomerTransactionId>
                                <v12:Localization>
                                   <v12:LanguageCode>EN</v12:LanguageCode>
                                   <v12:LocaleCode>US</v12:LocaleCode>
                                </v12:Localization>
                             </v12:TransactionDetail>
                             <v12:Version>
                                <v12:ServiceId>trck</v12:ServiceId>
                                <v12:Major>12</v12:Major>
                                <v12:Intermediate>0</v12:Intermediate>
                                <v12:Minor>0</v12:Minor>
                             </v12:Version>
                             <v12:SelectionDetails>
                                <v12:CarrierCode>FDXE</v12:CarrierCode>
                                <v12:PackageIdentifier>
                                   <v12:Type>TRACKING_NUMBER_OR_DOORTAG</v12:Type>
                                   <v12:Value>%s</v12:Value>
                                </v12:PackageIdentifier>
                                <v12:TrackingNumberUniqueIdentifier>%s</v12:TrackingNumberUniqueIdentifier>
                                <v12:SecureSpodAccount/>
                             </v12:SelectionDetails>
                             <v12:ProcessingOptions>INCLUDE_DETAILED_SCANS</v12:ProcessingOptions>
                          </v12:TrackRequest>
                       </soapenv:Body>
                    </soapenv:Envelope>""" % (
            key,
            password,
            account_number,
            meter_number,
            awb,
            unique_id,
        )
    else:
        body = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v12="http://fedex.com/ws/track/v12">
                       <soapenv:Header/>
                       <soapenv:Body>
                          <v12:TrackRequest>
                             <v12:WebAuthenticationDetail>
                                <v12:UserCredential>
                                   <v12:Key>%s</v12:Key>
                                   <v12:Password>%s</v12:Password>
                                </v12:UserCredential>
                             </v12:WebAuthenticationDetail>
                             <v12:ClientDetail>
                                <v12:AccountNumber>%s</v12:AccountNumber>
                                <v12:MeterNumber>%s</v12:MeterNumber>
                             </v12:ClientDetail>
                             <v12:TransactionDetail>
                                <v12:CustomerTransactionId>Track By Number_v14</v12:CustomerTransactionId>
                                <v12:Localization>
                                   <v12:LanguageCode>EN</v12:LanguageCode>
                                   <v12:LocaleCode>US</v12:LocaleCode>
                                </v12:Localization>
                             </v12:TransactionDetail>
                             <v12:Version>
                                <v12:ServiceId>trck</v12:ServiceId>
                                <v12:Major>12</v12:Major>
                                <v12:Intermediate>0</v12:Intermediate>
                                <v12:Minor>0</v12:Minor>
                             </v12:Version>
                             <v12:SelectionDetails>
                                <v12:CarrierCode>FDXE</v12:CarrierCode>
                                <v12:PackageIdentifier>
                                   <v12:Type>TRACKING_NUMBER_OR_DOORTAG</v12:Type>
                                   <v12:Value>%s</v12:Value>
                                </v12:PackageIdentifier>
                                <v12:ShipmentAccountNumber>%s</v12:ShipmentAccountNumber>
                                <v12:SecureSpodAccount/>
                             </v12:SelectionDetails>
                             <v12:ProcessingOptions>INCLUDE_DETAILED_SCANS</v12:ProcessingOptions>
                          </v12:TrackRequest>
                       </soapenv:Body>
                    </soapenv:Envelope>""" % (
            key,
            password,
            account_number,
            meter_number,
            awb,
            account_number,
        )
    return body


def generate_fedex_surface_tracker_body(awb, unique_id="", attempt=""):
    key = settings.FEDEX_SURFACE_KEY
    password = settings.FEDEX_SURFACE_PASSWORD
    account_number = settings.FEDEX_SURFACE_ACCOUNT_NUMBER
    meter_number = settings.FEDEX_SURFACE_METER_NUMBER
    if unique_id:
        body = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v12="http://fedex.com/ws/track/v12">
                       <soapenv:Header/>
                       <soapenv:Body>
                          <v12:TrackRequest>
                             <v12:WebAuthenticationDetail>
                                <v12:UserCredential>
                                   <v12:Key>%s</v12:Key>
                                   <v12:Password>%s</v12:Password>
                                </v12:UserCredential>
                             </v12:WebAuthenticationDetail>
                             <v12:ClientDetail>
                                <v12:AccountNumber>%s</v12:AccountNumber>
                                <v12:MeterNumber>%s</v12:MeterNumber>
                             </v12:ClientDetail>
                             <v12:TransactionDetail>
                                <v12:CustomerTransactionId>Track By Number_v14</v12:CustomerTransactionId>
                                <v12:Localization>
                                   <v12:LanguageCode>EN</v12:LanguageCode>
                                   <v12:LocaleCode>US</v12:LocaleCode>
                                </v12:Localization>
                             </v12:TransactionDetail>
                             <v12:Version>
                                <v12:ServiceId>trck</v12:ServiceId>
                                <v12:Major>12</v12:Major>
                                <v12:Intermediate>0</v12:Intermediate>
                                <v12:Minor>0</v12:Minor>
                             </v12:Version>
                             <v12:SelectionDetails>
                                <v12:CarrierCode>FDXE</v12:CarrierCode>
                                <v12:PackageIdentifier>
                                   <v12:Type>TRACKING_NUMBER_OR_DOORTAG</v12:Type>
                                   <v12:Value>%s</v12:Value>
                                </v12:PackageIdentifier>
                                <v12:TrackingNumberUniqueIdentifier>%s</v12:TrackingNumberUniqueIdentifier>
                                <v12:SecureSpodAccount/>
                             </v12:SelectionDetails>
                             <v12:ProcessingOptions>INCLUDE_DETAILED_SCANS</v12:ProcessingOptions>
                          </v12:TrackRequest>
                       </soapenv:Body>
                    </soapenv:Envelope>""" % (
            key,
            password,
            account_number,
            meter_number,
            awb,
            unique_id,
        )
    else:
        body = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v12="http://fedex.com/ws/track/v12">
                       <soapenv:Header/>
                       <soapenv:Body>
                          <v12:TrackRequest>
                             <v12:WebAuthenticationDetail>
                                <v12:UserCredential>
                                   <v12:Key>%s</v12:Key>
                                   <v12:Password>%s</v12:Password>
                                </v12:UserCredential>
                             </v12:WebAuthenticationDetail>
                             <v12:ClientDetail>
                                <v12:AccountNumber>%s</v12:AccountNumber>
                                <v12:MeterNumber>%s</v12:MeterNumber>
                             </v12:ClientDetail>
                             <v12:TransactionDetail>
                                <v12:CustomerTransactionId>Track By Number_v14</v12:CustomerTransactionId>
                                <v12:Localization>
                                   <v12:LanguageCode>EN</v12:LanguageCode>
                                   <v12:LocaleCode>US</v12:LocaleCode>
                                </v12:Localization>
                             </v12:TransactionDetail>
                             <v12:Version>
                                <v12:ServiceId>trck</v12:ServiceId>
                                <v12:Major>12</v12:Major>
                                <v12:Intermediate>0</v12:Intermediate>
                                <v12:Minor>0</v12:Minor>
                             </v12:Version>
                             <v12:SelectionDetails>
                                <v12:CarrierCode>FDXE</v12:CarrierCode>
                                <v12:PackageIdentifier>
                                   <v12:Type>TRACKING_NUMBER_OR_DOORTAG</v12:Type>
                                   <v12:Value>%s</v12:Value>
                                </v12:PackageIdentifier>
                                <v12:ShipmentAccountNumber>%s</v12:ShipmentAccountNumber>
                                <v12:SecureSpodAccount/>
                             </v12:SelectionDetails>
                             <v12:ProcessingOptions>INCLUDE_DETAILED_SCANS</v12:ProcessingOptions>
                          </v12:TrackRequest>
                       </soapenv:Body>
                    </soapenv:Envelope>""" % (
            key,
            password,
            account_number,
            meter_number,
            awb,
            account_number,
        )

    return body


def fedex_tracker(awb, unique_id="", attempt=""):
    try:
        # key = 'vCq6NGJfsw7zQncD'
        # password = '4PWt1fG0wt3ft0tGZbxDDtce9'
        # account_number = '733096624'
        # meter_number = '109191767'
        key = "F9k3AaSk4kNrAwhm"
        password = "9I7Sl02PHLqHaC46EaRM2hDie"
        account_number = "662334154"
        meter_number = "252587643"
        url = "https://ws.fedex.com:443/web-services/track"
        if unique_id:
            body = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v12="http://fedex.com/ws/track/v12">
                           <soapenv:Header/>
                           <soapenv:Body>
                              <v12:TrackRequest>
                                 <v12:WebAuthenticationDetail>
                                    <v12:UserCredential>
                                       <v12:Key>%s</v12:Key>
                                       <v12:Password>%s</v12:Password>
                                    </v12:UserCredential>
                                 </v12:WebAuthenticationDetail>
                                 <v12:ClientDetail>
                                    <v12:AccountNumber>%s</v12:AccountNumber>
                                    <v12:MeterNumber>%s</v12:MeterNumber>
                                 </v12:ClientDetail>
                                 <v12:TransactionDetail>
                                    <v12:CustomerTransactionId>Track By Number_v14</v12:CustomerTransactionId>
                                    <v12:Localization>
                                       <v12:LanguageCode>EN</v12:LanguageCode>
                                       <v12:LocaleCode>US</v12:LocaleCode>
                                    </v12:Localization>
                                 </v12:TransactionDetail>
                                 <v12:Version>
                                    <v12:ServiceId>trck</v12:ServiceId>
                                    <v12:Major>12</v12:Major>
                                    <v12:Intermediate>0</v12:Intermediate>
                                    <v12:Minor>0</v12:Minor>
                                 </v12:Version>
                                 <v12:SelectionDetails>
                                    <v12:CarrierCode>FDXE</v12:CarrierCode>
                                    <v12:PackageIdentifier>
                                       <v12:Type>TRACKING_NUMBER_OR_DOORTAG</v12:Type>
                                       <v12:Value>%s</v12:Value>
                                    </v12:PackageIdentifier>
                                    <v12:TrackingNumberUniqueIdentifier>%s</v12:TrackingNumberUniqueIdentifier>
                                    <v12:SecureSpodAccount/>
                                 </v12:SelectionDetails>
                                 <v12:ProcessingOptions>INCLUDE_DETAILED_SCANS</v12:ProcessingOptions>
                              </v12:TrackRequest>
                           </soapenv:Body>
                        </soapenv:Envelope>""" % (
                key,
                password,
                account_number,
                meter_number,
                awb,
                unique_id,
            )
        else:
            body = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v12="http://fedex.com/ws/track/v12">
                           <soapenv:Header/>
                           <soapenv:Body>
                              <v12:TrackRequest>
                                 <v12:WebAuthenticationDetail>
                                    <v12:UserCredential>
                                       <v12:Key>%s</v12:Key>
                                       <v12:Password>%s</v12:Password>
                                    </v12:UserCredential>
                                 </v12:WebAuthenticationDetail>
                                 <v12:ClientDetail>
                                    <v12:AccountNumber>%s</v12:AccountNumber>
                                    <v12:MeterNumber>%s</v12:MeterNumber>
                                 </v12:ClientDetail>
                                 <v12:TransactionDetail>
                                    <v12:CustomerTransactionId>Track By Number_v14</v12:CustomerTransactionId>
                                    <v12:Localization>
                                       <v12:LanguageCode>EN</v12:LanguageCode>
                                       <v12:LocaleCode>US</v12:LocaleCode>
                                    </v12:Localization>
                                 </v12:TransactionDetail>
                                 <v12:Version>
                                    <v12:ServiceId>trck</v12:ServiceId>
                                    <v12:Major>12</v12:Major>
                                    <v12:Intermediate>0</v12:Intermediate>
                                    <v12:Minor>0</v12:Minor>
                                 </v12:Version>
                                 <v12:SelectionDetails>
                                    <v12:CarrierCode>FDXE</v12:CarrierCode>
                                    <v12:PackageIdentifier>
                                       <v12:Type>TRACKING_NUMBER_OR_DOORTAG</v12:Type>
                                       <v12:Value>%s</v12:Value>
                                    </v12:PackageIdentifier>
                                    <v12:ShipmentAccountNumber>%s</v12:ShipmentAccountNumber>
                                    <v12:SecureSpodAccount/>
                                 </v12:SelectionDetails>
                                 <v12:ProcessingOptions>INCLUDE_DETAILED_SCANS</v12:ProcessingOptions>
                              </v12:TrackRequest>
                           </soapenv:Body>
                        </soapenv:Envelope>""" % (
                key,
                password,
                account_number,
                meter_number,
                awb,
                account_number,
            )
        if attempt:
            attempt += 1
        else:
            attempt = 1
        response = requests.post(url, data=body, timeout=10)
        fedex_logger.info(
            {"url": url, "data": body, "courier_response": str(response)}
        )
        # print response.content
        tracking_dict = prepare_tracking_dict_fedex(awb, response)
        if "err" in tracking_dict:
            if attempt < 2:
                unique_id = re.search(
                    r"<TrackingNumberUniqueIdentifier>(.*?)</TrackingNumberUniqueIdentifier>",
                    str(response.content),
                ).group(1)
                return fedex_tracker(awb, unique_id, attempt)
            return {}, {}
        else:
            # RTO_TRACK
            try:
                if attempt < 2:
                    if tracking_dict["status_type"] == "RTO":
                        if (
                            "return_waybill" in tracking_dict
                            and tracking_dict["return_waybill"]
                        ):
                            rto_dict = fedex_tracker(
                                tracking_dict["return_waybill"], "", attempt
                            )
                            if rto_dict["status_type"] == "DL":
                                tracking_dict["status_type"] = "RTD"
                            for tracker_array_dict in rto_dict[
                                "tracking_array"
                            ]:
                                if tracker_array_dict["scan_type"] == "OO":
                                    tracker_array_dict["scan_type"] = "RTO-OO"
                                elif tracker_array_dict["scan_type"] == "DL":
                                    tracker_array_dict["scan_type"] = "RTD"
                                else:
                                    tracker_array_dict["scan_type"] = "RTO-OT"
                            if (
                                "received_by" in rto_dict
                                and rto_dict["received_by"]
                            ):
                                tracking_dict["received_by"] = rto_dict[
                                    "received_by"
                                ]
                            for rto_dict_iter in rto_dict["tracking_array"]:
                                if (
                                    rto_dict_iter
                                    not in tracking_dict["tracking_array"]
                                ):
                                    tracking_dict["tracking_array"].append(
                                        rto_dict_iter
                                    )
                            tracking_dict["status_location"] = rto_dict[
                                "status_location"
                            ]
                            tracking_dict["status_date"] = rto_dict[
                                "status_date"
                            ]
            except Exception as e:
                fedex_logger.info({"err": str(e), "waybill": awb})
                pass
            response_json = xml_to_dict(response.content)
            if response_json["is_success"]:
                return response_json["data"]["courier_response"], tracking_dict
            return response_json["msg"], tracking_dict
    except Exception as e:
        fedex_logger.error({"err": str(e), "waybill": awb})
        return {}, {}


def prepare_tracking_dict_fedex(awb, response):
    try:
        tracking_dict = {}
        tracking_dict["awb"] = str(awb)
        from xml.etree import ElementTree

        from apps.common.services import ist_to_utc_converter

        namespaces = {
            "soap": "http://schemas.xmlsoap.org/soap/envelope/",
            "a": "http://fedex.com/ws/track/v12",
        }
        dom = ElementTree.fromstring(response.content)

        message = dom.findall(
            "./soap:Body" "/a:TrackReply" "/a:Notifications" "/a:Message",
            namespaces,
        )

        HighestSeverity = dom.findall(
            "./soap:Body" "/a:TrackReply" "/a:HighestSeverity",
            namespaces,
        )
        if str(HighestSeverity[0].text) == "SUCCESS":
            try:
                date = dom.findall(
                    "./soap:Body"
                    "/a:TrackReply"
                    "/a:CompletedTrackDetails"
                    "/a:TrackDetails"
                    "/a:StatusDetail"
                    "/a:CreationTime",
                    namespaces,
                )[0].text
                tracking_dict["status_date"] = parser.parse(date).replace(
                    tzinfo=None
                )
                tracking_dict["status_date"] = ist_to_utc_converter(
                    tracking_dict["status_date"]
                )
                try:
                    tracking_dict["status_location"] = dom.findall(
                        "./soap:Body"
                        "/a:TrackReply"
                        "/a:CompletedTrackDetails"
                        "/a:TrackDetails"
                        "/a:StatusDetail"
                        "/a:Location"
                        "/a:City",
                        namespaces,
                    )[0].text
                except Exception as e:
                    fedex_logger.info(str(awb) + "error:" + str(e))
                    tracking_dict["status_location"] = "NA"
                tracking_dict["status_info"] = dom.findall(
                    "./soap:Body"
                    "/a:TrackReply"
                    "/a:CompletedTrackDetails"
                    "/a:TrackDetails"
                    "/a:StatusDetail"
                    "/a:Description",
                    namespaces,
                )[0].text
                tracking_dict["status"] = dom.findall(
                    "./soap:Body"
                    "/a:TrackReply"
                    "/a:CompletedTrackDetails"
                    "/a:TrackDetails"
                    "/a:StatusDetail"
                    "/a:Code",
                    namespaces,
                )[0].text
            except Exception as e:
                fedex_logger.info({"err": str(e), "waybill": awb})
                date = dom.findall(
                    "./soap:Body"
                    "/a:TrackReply"
                    "/a:CompletedTrackDetails"
                    "/a:TrackDetails"
                    "/a:Events"
                    "/a:Timestamp",
                    namespaces,
                )[0].text
                tracking_dict["status_date"] = parser.parse(date).replace(
                    tzinfo=None
                )
                tracking_dict["status_date"] = ist_to_utc_converter(
                    tracking_dict["status_date"]
                )
                try:
                    tracking_dict["status_location"] = dom.findall(
                        "./soap:Body"
                        "/a:TrackReply"
                        "/a:CompletedTrackDetails"
                        "/a:TrackDetails"
                        "/a:Events"
                        "/a:Address"
                        "/a:City",
                        namespaces,
                    )[0].text
                except Exception as e:
                    fedex_logger.info({"err": str(e), "waybill": awb})
                    tracking_dict["status_location"] = "NA"
                try:
                    tracking_dict["status_info"] = dom.findall(
                        "./soap:Body"
                        "/a:TrackReply"
                        "/a:CompletedTrackDetails"
                        "/a:TrackDetails"
                        "/a:Events"
                        "/a:StatusExceptionDescription",
                        namespaces,
                    )[0].text
                except Exception as e:
                    fedex_logger.info({"err": str(e), "waybill": awb})
                    tracking_dict["status_info"] = dom.findall(
                        "./soap:Body"
                        "/a:TrackReply"
                        "/a:CompletedTrackDetails"
                        "/a:TrackDetails"
                        "/a:Events"
                        "/a:EventDescription",
                        namespaces,
                    )[0].text
                tracking_dict["status"] = dom.findall(
                    "./soap:Body"
                    "/a:TrackReply"
                    "/a:CompletedTrackDetails"
                    "/a:TrackDetails"
                    "/a:Events"
                    "/a:EventType",
                    namespaces,
                )[0].text
            tracking_dict["return_waybill"] = ""
            tracking_dict["received_by"] = ""
            tracking_array = []
            TrackDetails = dom.findall(
                "./soap:Body"
                "/a:TrackReply"
                "/a:CompletedTrackDetails"
                "/a:TrackDetails"
                "/a:Events",
                namespaces,
            )
            try:
                date = (
                    TrackDetails[0]
                    .findall("{http://fedex.com/ws/track/v12}Timestamp")[0]
                    .text
                )
                tracking_dict["status_date"] = parser.parse(date).replace(
                    tzinfo=None
                )
                tracking_dict["status_date"] = ist_to_utc_converter(
                    tracking_dict["status_date"]
                )
            except Exception as e:
                fedex_logger.info({"err": str(e), "waybill": awb})
                pass
            try:
                package_weight_value = dom.findall(
                    "./soap:Body"
                    "/a:TrackReply"
                    "/a:CompletedTrackDetails"
                    "/a:TrackDetails"
                    "/a:PackageWeight"
                    "/a:Value",
                    namespaces,
                )[0].text
                package_weight_unit = dom.findall(
                    "./soap:Body"
                    "/a:TrackReply"
                    "/a:CompletedTrackDetails"
                    "/a:TrackDetails"
                    "/a:PackageWeight"
                    "/a:Units",
                    namespaces,
                )[0].text
                shipment_weight_value = dom.findall(
                    "./soap:Body"
                    "/a:TrackReply"
                    "/a:CompletedTrackDetails"
                    "/a:TrackDetails"
                    "/a:ShipmentWeight"
                    "/a:Value",
                    namespaces,
                )[0].text
                shipment_weight_unit = dom.findall(
                    "./soap:Body"
                    "/a:TrackReply"
                    "/a:CompletedTrackDetails"
                    "/a:TrackDetails"
                    "/a:ShipmentWeight"
                    "/a:Units",
                    namespaces,
                )[0].text
                final_weight = (
                    package_weight_value
                    if package_weight_value > shipment_weight_value
                    else shipment_weight_value
                )
            except Exception as e:
                fedex_logger.info({"err": str(e), "waybill": awb})
                final_weight = 0

            flag = 0
            # RTOOO_flag = 0
            is_delivered = False
            for event in reversed(TrackDetails):
                exception_string = ""
                try:
                    exception_string = event.findall(
                        "{http://fedex.com/ws/track/v12}StatusExceptionDescription"
                    )[0].text
                    event_Type = event.findall(
                        "{http://fedex.com/ws/track/v12}EventType"
                    )[0].text
                    if exception_string.lower() in [
                        "at local fedex facility"
                    ] and event_Type not in ["DD", "DE"]:
                        continue
                except Exception as e:
                    fedex_logger.info({"err": str(e), "waybill": awb})
                    try:
                        exception_string = event.findall(
                            "{http://fedex.com/ws/track/v12}EventDescription"
                        )[0].text
                        event_Type = event.findall(
                            "{http://fedex.com/ws/track/v12}EventType"
                        )[0].text
                        if exception_string.lower() in [
                            "at local fedex facility"
                        ] and event_Type not in ["DD", "DE", "OD"]:
                            continue
                    except Exception as e:
                        fedex_logger.info({"err": str(e), "waybill": awb})
                        pass
                if is_delivered:
                    try:
                        exception_string = event.findall(
                            "{http://fedex.com/ws/track/v12}StatusExceptionDescription"
                        )[0].text
                        if exception_string.lower() in [
                            "tendered to authorized agent for final delivery",
                            "on fedex vehicle for delivery",
                            "at local fedex facility",
                            "at local fedex facility,package not due for delivery",
                        ]:
                            break
                    except Exception as e:
                        fedex_logger.info({"err": str(e), "waybill": awb})
                        try:
                            exception_string = event.findall(
                                "{http://fedex.com/ws/track/v12}EventDescription"
                            )[0].text
                            if exception_string.lower() in [
                                "tendered to authorized agent for final delivery",
                                "on fedex vehicle for delivery",
                                "at local fedex facility",
                                "at local fedex facility,package not due for delivery",
                            ]:
                                break
                        except Exception as e:
                            fedex_logger.info({"err": str(e), "waybill": awb})
                            pass

                track_dict = {}

                event_Type = event.findall(
                    "{http://fedex.com/ws/track/v12}EventType"
                )[0].text
                if event_Type in ["PU"]:
                    track_dict["scan_type"] = "PP"
                    add_to_fedex_tracking_dict(track_dict, event)
                    tracking_dict["pickup_datetime"] = track_dict[
                        "scan_datetime"
                    ]
                elif event_Type in ["DL"]:
                    is_delivered = True
                    track_dict["scan_type"] = "DL"
                    add_to_fedex_tracking_dict(track_dict, event)
                    try:
                        tracking_dict["received_by"] = dom.findall(
                            "./soap:Body"
                            "/a:TrackReply"
                            "/a:CompletedTrackDetails"
                            "/a:TrackDetails"
                            "/a:DeliverySignatureName",
                            namespaces,
                        )[0].text
                    except Exception as e:
                        fedex_logger.info({"err": str(e), "waybill": awb})
                        pass
                    if filter_fedex_statuses_after_DL(
                        tracking_dict, event_Type, event
                    ):
                        track_dict["pickrr_sub_status_code"] = ""
                        track_dict["courier_status_code"] = ""
                        tracking_array.append(track_dict.copy())
                        break
                elif event_Type in ["DD", "DE"] or (
                    len(exception_string) > 0
                    and exception_string.lower()
                    in [
                        "tendered to authorized agent for final delivery",
                        "in transit,tendered to authorized agent for final delivery",
                        "customer wants open delivery",
                    ]
                ):
                    track_dict["scan_type"] = "NDR"
                    add_to_fedex_tracking_dict(track_dict, event)
                elif event_Type in ["OD"]:
                    # RTOOO_flag += 1
                    track_dict["scan_type"] = "OO"
                    add_to_fedex_tracking_dict(track_dict, event)
                elif event_Type in ["RS"]:
                    track_dict["scan_type"] = "RTO"
                    add_to_fedex_tracking_dict(track_dict, event)
                    try:
                        tracking_dict["received_by"] = dom.findall(
                            "./soap:Body"
                            "/a:TrackReply"
                            "/a:CompletedTrackDetails"
                            "/a:TrackDetails"
                            "/a:DeliverySignatureName",
                            namespaces,
                        )[0].text
                    except Exception as e:
                        fedex_logger.info({"err": str(e), "waybill": awb})
                        pass
                    try:
                        tracking_dict["return_waybill"] = dom.findall(
                            "./soap:Body"
                            "/a:TrackReply"
                            "/a:CompletedTrackDetails"
                            "/a:TrackDetails"
                            "/a:OtherIdentifiers"
                            "/a:PackageIdentifier"
                            "/a:Value",
                            namespaces,
                        )[0].text
                    except Exception as e:
                        fedex_logger.info({"err": str(e), "waybill": awb})
                        pass
                elif event_Type in [
                    "CA",
                    "EP",
                    "OC",
                    "OX",
                    "AC",
                    "RR",
                    "RM",
                    "RC",
                    "RP",
                    "LP",
                    "RG",
                    "RD",
                    "SP",
                    "TR",
                    "CC",
                    "CD",
                    "CP",
                    "EA",
                    "SP",
                    "RC",
                    "SH",
                    "CU",
                    "BR",
                    "TP",
                    "PD",
                ]:
                    continue
                else:
                    track_dict["scan_type"] = "OT"
                    add_to_fedex_tracking_dict(track_dict, event)
                    if "pickup_datetime" not in tracking_dict:
                        tracking_dict["pickup_datetime"] = track_dict[
                            "scan_datetime"
                        ]
                track_dict["pickrr_sub_status_code"] = ""
                track_dict["courier_status_code"] = ""
                tracking_array.append(track_dict.copy())
                """
                if RTOOO_flag > 1:
                    track_dict['scan_type'] = "RTO-OO"
                """
            tracking_array.sort(key=lambda x: x["scan_datetime"], reverse=True)
            if tracking_dict["status"] in ["DL"]:
                tracking_dict["status_type"] = "DL"
            else:
                if len(tracking_array) > 0:
                    if tracking_array[0]["scan_type"] == "NDR":
                        tracking_dict["status_type"] = "OT"
                    else:
                        tracking_dict["status_type"] = tracking_array[0][
                            "scan_type"
                        ]
                        tracking_dict["status"] = tracking_array[0][
                            "scan_type"
                        ]

            try:
                track_dict = {}
                tracking_dict["return_waybill"] = dom.findall(
                    "./soap:Body"
                    "/a:TrackReply"
                    "/a:CompletedTrackDetails"
                    "/a:TrackDetails"
                    "/a:OtherIdentifiers"
                    "/a:PackageIdentifier"
                    "/a:Value",
                    namespaces,
                )[0].text
                if tracking_dict["return_waybill"] != str(awb):
                    track_dict["scan_type"] = "RTO"
                    try:
                        tracking_dict["received_by"] = dom.findall(
                            "./soap:Body"
                            "/a:TrackReply"
                            "/a:CompletedTrackDetails"
                            "/a:TrackDetails"
                            "/a:DeliverySignatureName",
                            namespaces,
                        )[0].text
                    except Exception as e:
                        fedex_logger.info({"err": str(e), "waybill": awb})
                        pass
                    track_dict["scan_status"] = tracking_dict["status_info"]
                    track_dict["scan_datetime"] = tracking_dict["status_date"]
                    track_dict["scan_location"] = tracking_dict[
                        "status_location"
                    ]
                    track_dict["pickrr_sub_status_code"] = ""
                    track_dict["courier_status_code"] = ""
                    tracking_array.append(track_dict.copy())
            except Exception as e:
                fedex_logger.info({"err": str(e), "waybill": awb})
                pass

            tracking_dict["tracking_array"] = tracking_array
            tracking_dict["final_weight"] = final_weight
            tracking_dict = fix_tracking_dict(tracking_dict, "fedex")
        else:
            tracking_dict["err"] = message[0].text
        return tracking_dict
    except Exception as e:
        fedex_logger.info({"err": str(e), "waybill": awb})
        return {"err": str(e)}


def add_to_fedex_tracking_dict(track_dict, event):
    from apps.common.services import ist_to_utc_converter

    track_dict["scan_status"] = event.findall(
        "{http://fedex.com/ws/track/v12}EventDescription"
    )[0].text
    try:
        track_dict["scan_status"] += (
            ","
            + event.findall(
                "{http://fedex.com/ws/track/v12}StatusExceptionDescription"
            )[0].text
        )
    except Exception as e:
        pass
    date = event.findall("{http://fedex.com/ws/track/v12}Timestamp")[0].text
    track_dict["scan_datetime"] = parser.parse(date).replace(tzinfo=None)
    track_dict["scan_datetime"] = ist_to_utc_converter(
        track_dict["scan_datetime"]
    )
    try:
        track_dict["scan_location"] = (
            event.findall("{http://fedex.com/ws/track/v12}Address")[0]
            .findall("{http://fedex.com/ws/track/v12}City")[0]
            .text
        )
    except Exception as e:
        track_dict["scan_location"] = ""


def filter_fedex_statuses_after_DL(tracking_dict, event_Type, event):
    result = False
    from apps.common.services import ist_to_utc_converter

    if event_Type in ["DL"]:
        if tracking_dict["status_info"].lower() in [
            "tendered to authorized agent for final delivery",
            "on fedex vehicle for delivery",
            "at local fedex facility",
            "at local fedex facility,package not due for delivery",
        ]:
            result = True
            tracking_dict["status_info"] = event.findall(
                "{http://fedex.com/ws/track/v12}EventDescription"
            )[0].text
            tracking_dict["status"] = event_Type
            date = event.findall("{http://fedex.com/ws/track/v12}Timestamp")[
                0
            ].text
            tracking_dict["status_date"] = parser.parse(date).replace(
                tzinfo=None
            )
            tracking_dict["status_date"] = ist_to_utc_converter(
                tracking_dict["status_date"]
            )
            try:
                tracking_dict["status_location"] = (
                    event.findall("{http://fedex.com/ws/track/v12}Address")[0]
                    .findall("{http://fedex.com/ws/track/v12}City")[0]
                    .text
                )
            except Exception as e:
                tracking_dict["status_location"] = ""
        else:
            result = False
    return result


def fedex_surface_tracker(awb, unique_id="", attempt=""):
    try:
        # key = 'js9RE30O6xC9oJky'
        # password = '0OdUCEHJTrrwAt5QBNpSUjzZh'
        # account_number = '629874712'
        # meter_number = '114564266'
        key = "NlIj1ZfMvqMZwUcz"
        password = "8kVFDECsRJBqShT8ywXc8Xuhc"
        account_number = "633418233"
        meter_number = "252587685"
        url = "https://ws.fedex.com:443/web-services/track"
        if unique_id:
            body = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v12="http://fedex.com/ws/track/v12">
                           <soapenv:Header/>
                           <soapenv:Body>
                              <v12:TrackRequest>
                                 <v12:WebAuthenticationDetail>
                                    <v12:UserCredential>
                                       <v12:Key>%s</v12:Key>
                                       <v12:Password>%s</v12:Password>
                                    </v12:UserCredential>
                                 </v12:WebAuthenticationDetail>
                                 <v12:ClientDetail>
                                    <v12:AccountNumber>%s</v12:AccountNumber>
                                    <v12:MeterNumber>%s</v12:MeterNumber>
                                 </v12:ClientDetail>
                                 <v12:TransactionDetail>
                                    <v12:CustomerTransactionId>Track By Number_v14</v12:CustomerTransactionId>
                                    <v12:Localization>
                                       <v12:LanguageCode>EN</v12:LanguageCode>
                                       <v12:LocaleCode>US</v12:LocaleCode>
                                    </v12:Localization>
                                 </v12:TransactionDetail>
                                 <v12:Version>
                                    <v12:ServiceId>trck</v12:ServiceId>
                                    <v12:Major>12</v12:Major>
                                    <v12:Intermediate>0</v12:Intermediate>
                                    <v12:Minor>0</v12:Minor>
                                 </v12:Version>
                                 <v12:SelectionDetails>
                                    <v12:CarrierCode>FDXE</v12:CarrierCode>
                                    <v12:PackageIdentifier>
                                       <v12:Type>TRACKING_NUMBER_OR_DOORTAG</v12:Type>
                                       <v12:Value>%s</v12:Value>
                                    </v12:PackageIdentifier>
                                    <v12:TrackingNumberUniqueIdentifier>%s</v12:TrackingNumberUniqueIdentifier>
                                    <v12:SecureSpodAccount/>
                                 </v12:SelectionDetails>
                                 <v12:ProcessingOptions>INCLUDE_DETAILED_SCANS</v12:ProcessingOptions>
                              </v12:TrackRequest>
                           </soapenv:Body>
                        </soapenv:Envelope>""" % (
                key,
                password,
                account_number,
                meter_number,
                awb,
                unique_id,
            )
        else:
            body = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v12="http://fedex.com/ws/track/v12">
                           <soapenv:Header/>
                           <soapenv:Body>
                              <v12:TrackRequest>
                                 <v12:WebAuthenticationDetail>
                                    <v12:UserCredential>
                                       <v12:Key>%s</v12:Key>
                                       <v12:Password>%s</v12:Password>
                                    </v12:UserCredential>
                                 </v12:WebAuthenticationDetail>
                                 <v12:ClientDetail>
                                    <v12:AccountNumber>%s</v12:AccountNumber>
                                    <v12:MeterNumber>%s</v12:MeterNumber>
                                 </v12:ClientDetail>
                                 <v12:TransactionDetail>
                                    <v12:CustomerTransactionId>Track By Number_v14</v12:CustomerTransactionId>
                                    <v12:Localization>
                                       <v12:LanguageCode>EN</v12:LanguageCode>
                                       <v12:LocaleCode>US</v12:LocaleCode>
                                    </v12:Localization>
                                 </v12:TransactionDetail>
                                 <v12:Version>
                                    <v12:ServiceId>trck</v12:ServiceId>
                                    <v12:Major>12</v12:Major>
                                    <v12:Intermediate>0</v12:Intermediate>
                                    <v12:Minor>0</v12:Minor>
                                 </v12:Version>
                                 <v12:SelectionDetails>
                                    <v12:CarrierCode>FDXE</v12:CarrierCode>
                                    <v12:PackageIdentifier>
                                       <v12:Type>TRACKING_NUMBER_OR_DOORTAG</v12:Type>
                                       <v12:Value>%s</v12:Value>
                                    </v12:PackageIdentifier>
                                    <v12:ShipmentAccountNumber>%s</v12:ShipmentAccountNumber>
                                    <v12:SecureSpodAccount/>
                                 </v12:SelectionDetails>
                                 <v12:ProcessingOptions>INCLUDE_DETAILED_SCANS</v12:ProcessingOptions>
                              </v12:TrackRequest>
                           </soapenv:Body>
                        </soapenv:Envelope>""" % (
                key,
                password,
                account_number,
                meter_number,
                awb,
                account_number,
            )
        if attempt:
            attempt += 1
        else:
            attempt = 1
        response = requests.post(url, data=body, timeout=10)
        fedex_logger.info(
            {"url": url, "data": body, "courier_response": str(response)}
        )
        # print response.content
        tracking_dict = prepare_tracking_dict_fedex(awb, response)
        if "err" in tracking_dict:
            if ("list index out of range" in tracking_dict["err"]) and (
                attempt == 1
            ):
                unique_id = re.search(
                    r"<TrackingNumberUniqueIdentifier>(.*?)</TrackingNumberUniqueIdentifier>",
                    str(response.content),
                ).group(1)
                return fedex_surface_tracker(awb, str(unique_id), attempt)
            return {}, {}
        else:
            # RTO_TRACK
            try:
                if attempt < 2:
                    if tracking_dict["status_type"] == "RTO":
                        if (
                            "return_waybill" in tracking_dict
                            and tracking_dict["return_waybill"]
                        ):
                            rto_dict = fedex_tracker(
                                tracking_dict["return_waybill"], "", attempt
                            )
                            if rto_dict["status_type"] == "DL":
                                tracking_dict["status_type"] = "RTD"
                            for tracker_array_dict in rto_dict[
                                "tracking_array"
                            ]:
                                if tracker_array_dict["scan_type"] == "OO":
                                    tracker_array_dict["scan_type"] = "RTO-OO"
                                elif tracker_array_dict["scan_type"] == "DL":
                                    tracker_array_dict["scan_type"] = "RTD"
                                else:
                                    tracker_array_dict["scan_type"] = "RTO-OT"
                            if (
                                "received_by" in rto_dict
                                and rto_dict["received_by"]
                            ):
                                tracking_dict["received_by"] = rto_dict[
                                    "received_by"
                                ]
                            for rto_dict_iter in rto_dict["tracking_array"]:
                                if (
                                    rto_dict_iter
                                    not in tracking_dict["tracking_array"]
                                ):
                                    tracking_dict["tracking_array"].append(
                                        rto_dict_iter
                                    )
                            tracking_dict["status_location"] = rto_dict[
                                "status_location"
                            ]
                            tracking_dict["status_date"] = rto_dict[
                                "status_date"
                            ]
            except Exception as e:
                fedex_logger.info({"err": str(e), "waybill": awb})
                pass
            response_json = xml_to_dict(response.content)
            if response_json["is_success"]:
                return response_json["data"]["courier_response"], tracking_dict
            return response_json["msg"], tracking_dict
    except Exception as e:
        fedex_logger.error({"err": str(e), "waybill": awb})
        return {}, {}


def fix_tracking_dict(tracking_dict, courier):
    orig_tracking_dict = copy.deepcopy(tracking_dict)
    try:
        status_OO = None
        last_dispatch_date = 0
        track_info = ""
        ndr_track = {}
        events_list = copy.deepcopy(tracking_dict["tracking_array"])
        events_list.sort(key=lambda x: x["scan_datetime"])
        events_list_len = len(events_list)
        new_events_list = []
        ot_strings = [
            "call placed to consignee",
            "Paid through link",
            "ndr call - fe remark correct",
            "agent remark verified",
        ]
        ot_strings = [x.lower() for x in ot_strings]
        for ind in range(events_list_len):
            if courier == "aramex":
                if "Attempted Delivery" in events_list[ind]["scan_status"]:
                    new_event = copy.deepcopy(events_list[ind])
                    new_event["scan_status"] = new_event["scan_status"][21:]
                    new_event["scan_type"] = "NDR"
                    new_events_list.append(new_event)
                    status_OO = None
            elif courier == "delhivery_reverse":
                if (
                    "Out for pickup" in events_list[ind]["scan_status"]
                    and events_list_len > ind
                    and "Pick Up Completed"
                    not in events_list[ind + 1]["scan_status"]
                ):
                    events_list[ind + 1]["scan_type"] = "NDR"
            else:
                ndr_strings = [
                    "Out of Delivery Area (ODA)",
                    "Buyer refused order",
                    "Damaged Product",
                    "ODA Shipment",
                    "OUT OF DELIVERY AREA",
                    "Out of Delivery Area",
                    "Reached Maximum attempt count",
                    "CONSIGNEE OUT OF STATION",
                    "CONSIGNEE REFUSED TO ACCEPT",
                    "DELIVERY ATTEMPTED-PREMISES CLOSED",
                    "CONSIGNEE'S ADDRESS INCOMPLETE/LANDMARK NEEDED",
                    "CONSIGNEE'S ADDRESS INCORRECT",
                    "Bulk Order, Refused By Consignee",
                    "WRONG PINCODE, WILL IMPACT DELIVERY",
                    "CONSIGNEE NOT AVAILABLE",
                    "CONSIGNEE HAS GIVEN BDE HAL ADDRESS",
                    "CONSIGNEE'S ADDRESS UNLOCATABLE/LANDMARK NEEDED",
                    "Customer Requested Future Delivery: Hal",
                    "Self Collect requested by customer",
                    "Reached out to customer for Self Collect",
                    "BULK ORDER, REFUSED BY CONSIGNEE",
                    "NO SUCH CONSIGNEE AT THE GIVEN ADDRESS",
                    "NECESSARY CHARGES PENDING FROM CONSIGNEE",
                    "CUSTOMER REQUESTED FUTURE DELIVERY",
                    "CONSIGNEE REFUSED-WRONG ORDER",
                    "NEED DEPARTMENT NAME/EXTENTION NUMBER",
                    "NO SUCH CONSIGNEE AT THE GIVEN ADDRESS",
                    "FAKE BOOKING/FAKE ADDRESS",
                    "OUT OF DELIVERY AREA",
                    "Customer Refused to accept/Order Cancelled - OTP Verified",
                    "CONSIGNEE'S ADDRESS INCORRECT/INCORRECT",
                    "No Entry/ Entry restricted",
                    "Returned as per Client Instructions",
                    "OTP verified cancellation",
                    "Consignee to collect from branch",
                    "Consignee refused to accept/order cancelled",
                    "Consignee Unavailable",
                ]
                # "Contact Customer Service","Undelivered Shipment Held At Location", Removed ndr strings
                ndr_strings = [x.lower() for x in ndr_strings]
                if events_list[ind]["scan_status"].lower() in ndr_strings:
                    oda_dict = {}
                    oda_dict["scan_type"] = "NDR"
                    oda_dict["scan_datetime"] = events_list[ind][
                        "scan_datetime"
                    ] + relativedelta(minutes=2)
                    oda_dict["scan_status"] = events_list[ind]["scan_status"]
                    oda_dict["scan_location"] = events_list[ind][
                        "scan_location"
                    ]
                    events_list.append(oda_dict)
                elif events_list[ind]["scan_type"] == "OO":
                    status_OO = 1
                elif events_list[ind]["scan_type"] == "OT" and status_OO:
                    events_list[ind]["scan_type"] = "NDR"
                    status_OO = None
            if events_list[ind]["scan_status"].lower() in ot_strings:
                events_list[ind]["scan_type"] = "OT"
        events_list = events_list + new_events_list
        events_list.sort(key=lambda x: x["scan_datetime"], reverse=True)
        tracking_dict["tracking_array"] = events_list
        return tracking_dict
    except Exception as e:
        return orig_tracking_dict
