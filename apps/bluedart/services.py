import json
from datetime import datetime

from django.conf import settings

from apps.common.utils import GETRequest
from pickrr_tracker.loggers import bluedart_logger

from .utils import BLUEDART_MAPPER


def get_pikrr_response_bluedart(
    courier_type, data, awb_array, rto_flag, forward_tracking_history
):
    try:
        data = json.loads(data)
        final_tracking_array = []
        shipments = data["ShipmentData"]["Shipment"]
        if type(shipments) != list:
            shipments = [shipments]
        for shipment in shipments:
            try:
                trackingdict = {}
                if str(shipment["WaybillNo"]) in awb_array:
                    track_data = shipment
                    status_scan_type = track_data["StatusType"]
                    return_waybill = ""
                    try:
                        trackingdict["final_weight"] = float(
                            track_data["Weight"]
                        )
                    except Exception as e:
                        trackingdict["final_weight"] = None
                        bluedart_logger.info(
                            {"err": str(e), "waybill": track_data["WaybillNo"]}
                        )
                    try:
                        waybill = track_data["WaybillNo"]
                    except Exception:
                        waybill = ""
                    tracking_array = []
                    if forward_tracking_history:
                        tracking_array += forward_tracking_history
                    track_list = []
                    if track_data.get("Scans", None) is not None:
                        track_list = track_data["Scans"]["ScanDetail"]
                        if type(track_list) != list:
                            track_list = [track_list]
                    for track_array in track_list:
                        track_dict = {}
                        mapper_string = (
                            str(track_array["ScanCode"])
                            + "-"
                            + str(track_array["ScanGroupType"])
                        )
                        info_timestamp = (
                            track_array["ScanDate"]
                            + " "
                            + track_array["ScanTime"]
                        )
                        info_date = datetime.strptime(
                            info_timestamp, "%d-%b-%Y %H:%M"
                        )
                        from apps.common.services import ist_to_utc_converter

                        info_date = ist_to_utc_converter(info_date)
                        if str(mapper_string) in BLUEDART_MAPPER:
                            scan_type = BLUEDART_MAPPER[str(mapper_string)][
                                "scan_type"
                            ]
                            if rto_flag is True:
                                if scan_type == "DL":
                                    scan_type = "RTD"
                                elif scan_type == "OO":
                                    scan_type = "RTO-OO"
                                else:
                                    scan_type = "RTO-OT"
                            if scan_type == "PP":
                                trackingdict["pickup_datetime"] = info_date
                            track_dict["scan_type"] = scan_type
                        else:
                            continue
                        scan_location = track_array["ScannedLocation"]
                        info = track_array["Scan"]
                        track_dict["scan_status"] = info
                        track_dict["scan_datetime"] = info_date
                        track_dict["scan_location"] = scan_location
                        track_dict["pickrr_sub_status_code"] = BLUEDART_MAPPER[
                            str(mapper_string)
                        ]["pickrr_sub_status_code"]
                        track_dict["courier_status_code"] = mapper_string
                        tracking_array.append(track_dict)

                    if rto_flag is True:
                        return tracking_array
                    trackingdict["return_waybill"] = (
                        shipment["NewWaybillNo"]
                        if shipment.get("NewWaybillNo")
                        else None
                    )
                    if trackingdict["return_waybill"] and not rto_flag:
                        return_awb = trackingdict["return_waybill"]
                        return_waybill = [return_awb]
                        tracking_array = tracker(
                            courier_type, return_waybill, True, tracking_array
                        )

                    trackingdict["awb"] = waybill
                    expected_delivery = None
                    try:
                        expected_delivery = datetime.strptime(
                            track_data["ExpectedDeliveryDate"], "%d %B %Y"
                        )
                        expected_delivery = ist_to_utc_converter(
                            expected_delivery
                        )
                    except Exception as e:
                        bluedart_logger.info(
                            {"err": str(e), "waybill": track_data["WaybillNo"]}
                        )
                        pass
                    trackingdict["edd_stamp"] = expected_delivery
                    received_by = ""
                    try:
                        if status_scan_type == "DL":
                            received_by = track_data["ReceivedBy"]
                    except Exception as e:
                        bluedart_logger.info(
                            {"err": str(e), "waybill": track_data["WaybillNo"]}
                        )
                        pass
                    if tracking_array:
                        tracking_array = sorted(
                            tracking_array,
                            key=lambda k: k["scan_datetime"],
                            reverse=True,
                        )
                        trackingdict["status"] = tracking_array[0][
                            "scan_status"
                        ]
                        trackingdict["status_date"] = tracking_array[0][
                            "scan_datetime"
                        ]
                        trackingdict["status_type"] = tracking_array[0][
                            "scan_type"
                        ]
                        trackingdict["status_location"] = tracking_array[0][
                            "scan_location"
                        ]

                    trackingdict["received_by"] = received_by
                    trackingdict["tracking_array"] = tracking_array
                    final_tracking_array.append(trackingdict)
            except Exception as e:
                waybill = shipment["WaybillNo"]
                bluedart_logger.error(
                    {"err": str(e), "waybill": waybill, "shipment": shipment}
                )
                final_tracking_array.append(
                    {"tracking_id": waybill, "err": str(e)}
                )
            return final_tracking_array
    except Exception as e:
        bluedart_logger.error({"err": str(e), "waybills": str(awb_array)})
        return {"waybills": str(awb_array), "err": str(e)}


def tracker(
    courier_type, waybills: list, rto_flag=False, forward_tracking_history=None
) -> dict:
    params = {
        "handler": "tnt",
        "action": "custawbquery",
        "loginid": settings.BLUEDART_LOGIN_ID,
        "awb": "awb",
        "numbers": ",".join(waybills),
        "format": "xml",
        "lickey": settings.BLUEDART_LIC_KEY,
        "verno": "1.3",
        "scan": "1",
    }
    url = "http://api.bluedart.com/servlet/RoutingServlet"
    request = GETRequest(url=url, params=params, xml=True)
    response = request.send()
    bluedart_logger.info(
        {"url": url, "params": params, "courier_response": str(response)}
    )
    try:
        if response["is_success"]:
            if rto_flag:
                return get_pikrr_response_bluedart(
                    courier_type,
                    response["data"]["courier_response"],
                    waybills,
                    rto_flag,
                    forward_tracking_history,
                )
            else:
                response["data"][
                    "pickrr_response"
                ] = get_pikrr_response_bluedart(
                    courier_type,
                    response["data"]["courier_response"],
                    waybills,
                    rto_flag,
                    forward_tracking_history,
                )
                response["data"]["courier_response"] = json.loads(
                    response["data"]["courier_response"]
                )
        return response
    except Exception as e:
        bluedart_logger.error({"err": str(e), "awbs": waybills})
        return {"tracking_id": str(waybills), "err": str(e)}
