from django.conf import settings

from apps.common.utils import GETRequest
from pickrr_tracker.loggers import shadowfax_logger

from .utils import (
    get_pikrr_response_shadowfax,
    get_reverse_pikrr_response_shadowfax,
    get_token_and_url,
)


def tracker(courier_type, waybills: list) -> dict:
    try:
        final_tracking_array = []
        final_courier_response = []
        for waybill in waybills:
            data = get_token_and_url(courier_type, waybill)
            url = data["url"]
            headers = {
                "content-type": "application/json",
                "Authorization": "Token {}".format(data["token"]),
            }
            request = GETRequest(url=url, headers=headers)
            response = request.send()
            shadowfax_logger.info(
                {
                    "url": url,
                    "courier_response": str(response),
                    "awb": waybill,
                }
            )
            final_courier_response.append(response["data"]["courier_response"])
            try:
                if response["is_success"]:
                    try:
                        if courier_type in ["shadowfax", "shadowfax_bulk"]:
                            tracking_dict = get_pikrr_response_shadowfax(
                                response["data"]["courier_response"], waybill
                            )
                        else:
                            tracking_dict = (
                                get_reverse_pikrr_response_shadowfax(
                                    response["data"]["courier_response"],
                                    waybill,
                                )
                            )
                    except Exception as e:
                        shadowfax_logger.error(
                            {"err": str(e), "waybill": waybill}
                        )
                        tracking_dict = {"err": e}
                    final_tracking_array.append(tracking_dict)
            except Exception as e:
                shadowfax_logger.info({"err": str(e), "awbs": waybills})
        response["data"]["courier_response"] = final_courier_response
        response["data"]["pickrr_response"] = final_tracking_array
        return response
    except Exception as e:
        shadowfax_logger.info({"awbs": waybills, "err": str(e)})
        return {"err": str(e)}
