import dateutil.parser
from django.conf import settings


def get_token_and_url(courier_type: str, awb) -> dict:
    credentials_map = {
        "shadowfax": {
            "token": settings.SHADOWFAX_TOKEN,
            "url": "http://dale.shadowfax.in/api/v4/clients/orders/{}/track/?format=json".format(
                awb
            ),
        },
        "shadowfax_bulk": {
            "token": settings.SHADOWFAX_BULK_TOKEN,
            "url": "http://dale.shadowfax.in/api/v4/clients/orders/{}/track/?format=json".format(
                awb
            ),
        },
        "shadowfax_bulk_reverse": {
            "token": settings.SHADOWFAX_BULK_REVERSE_TOKEN,
            "url": "https://reverse.shadowfax.in/api/v4/clients/requests/{}".format(
                awb
            ),
        },
        "shadowfax_reverse": {
            "token": settings.SHADOWFAX_REVERSE_TOKEN,
            "url": "https://reverse.shadowfax.in/api/v4/clients/requests/{}".format(
                awb
            ),
        },
    }
    return credentials_map[courier_type]


NEW_STATUS_TO_OLD_MAPPING = {
    "OFP": "OP",
    "PPF": "OP",
    "LT": "OT",
    "RTO-OO": "RTO",
    "RTO-OT": "RTO",
    "DM": "OT",
    "RAD": "OT",
    "SHP": "OT",
    "UD": "NDR",
    "RTO UD": "RTO",
}


shadowfax_reverse_bulk_code_mapper = {
    "New": {"scan_type": "OP", "pickrr_sub_status_code": ""},
    "Assigned For Customer Pickup": {
        "scan_type": "OM",
        "pickrr_sub_status_code": "",
    },
    "Out For Pickup": {"scan_type": "OP", "pickrr_sub_status_code": ""},
    "Picked": {"scan_type": "PP", "pickrr_sub_status_code": ""},
    "QC Failed": {"scan_type": "OP", "pickrr_sub_status_code": "CI"},
    "cid": {"scan_type": "OP", "pickrr_sub_status_code": "CI"},
    "Not Contactable": {"scan_type": "OP", "pickrr_sub_status_code": "CNA"},
    "Not Attempted": {"scan_type": "OP", "pickrr_sub_status_code": "SD"},
    "pickup_on_hold": {"scan_type": "OP", "pickrr_sub_status_code": "NSL"},
    "cancelled": {"scan_type": "OC", "pickrr_sub_status_code": ""},
    "Received": {"scan_type": "RTO", "pickrr_sub_status_code": ""},
    "Received at Return DC": {
        "scan_type": "RTO",
        "pickrr_sub_status_code": "",
    },
    "Undelivered": {"scan_type": "RTO", "pickrr_sub_status_code": ""},
    "Returned To Client": {"scan_type": "RTD", "pickrr_sub_status_code": ""},
    "Lost": {"scan_type": "OT", "pickrr_sub_status_code": ""},
}

sfx_mapper2 = {
    "new": {"scan_type": "OM", "pickrr_sub_status_code": ""},
    "assigned_for_pickup": {"scan_type": "OM", "pickrr_sub_status_code": ""},
    "ofp": {"scan_type": "OFP", "pickrr_sub_status_code": ""},
    "recd_at_rev_hub": {"scan_type": "SHP", "pickrr_sub_status_code": ""},
    "received_at_pickup_hub": {
        "scan_type": "OT",
        "pickrr_sub_status_code": "",
    },
    "In_Manifest": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "in_transit": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "received_at_via": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "recd_at_fwd_dc": {"scan_type": "OT", "pickrr_sub_status_code": ""},
    "recd_at_fwd_hub": {"scan_type": "RAD", "pickrr_sub_status_code": ""},
    "assigned_for_delivery": {
        "scan_type": "RAD",
        "pickrr_sub_status_code": "",
    },
    "ofd": {"scan_type": "OO", "pickrr_sub_status_code": ""},
    "cancelled_by_customer": {"scan_type": "OC", "pickrr_sub_status_code": ""},
    "seller_not_contactable": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SU",
    },
    "pickup_not_attempted": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "NA",
    },
    "reopen_ndr": {"scan_type": "UD", "pickrr_sub_status_code": "OTH"},
    "seller_initiated_delay": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "",
    },
    "cid": {"scan_type": "UD", "pickrr_sub_status_code": "CD"},
    "rts_in_process": {"scan_type": "RTO-OT", "pickrr_sub_status_code": ""},
    "rts_d": {"scan_type": "RTD", "pickrr_sub_status_code": ""},
    "rts_nd": {"scan_type": "RTO UD", "pickrr_sub_status_code": ""},
    "lost": {"scan_type": "LT", "pickrr_sub_status_code": ""},
    "delivered": {"scan_type": "DL", "pickrr_sub_status_code": ""},
    "item_delivered_at": {"scan_type": "DL", "pickrr_sub_status_code": ""},
}

sfx_mapper1 = {
    "pickup_on_hold_Customer want pick-up from Non-Serviceable area": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "NSL",
    },
    "pickup_on_hold_Pincode Address Mismatch": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "AI",
    },
    "pickup_on_hold_Incorrect contact number": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SU",
    },
    "pickup_on_hold_Address issue": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "AI",
    },
    "pickup_on_hold_Customer wants replacement/refund first": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "OTH",
    },
    "pickup_on_hold_Incomplete information received from client": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "OTH",
    },
    "pickup_on_hold_Mandatory check not available": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "OTH",
    },
    "pickup_on_hold_Pickup already done by other DSP": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "OTH",
    },
    "pickup_on_hold_Shipment is not picked": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "OTH",
    },
    "pickup_on_hold_Successful 3 attempts done": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SNR",
    },
    "pickup_on_hold_Large Shipment": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "NA",
    },
    "pickup_on_hold_Customer wants pickup beyond cut off time": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "OTH",
    },
    "pickup_on_hold_Customer changed his/her mind": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SNR",
    },
    "pickup_on_hold_Doorstep QC Brandbox not available": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "OTH",
    },
    "pickup_on_hold_Doorstep QC Price Tag missing": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "OTH",
    },
    "pickup_on_hold_Doorstep QC Product Damage": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "OTH",
    },
    "pickup_on_hold_Doorstep QC Product Mismatch": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "OTH",
    },
    "pickup_on_hold_COVID Restricted Area": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "NSL",
    },
    "pickup_on_hold_Seller wants pickup beyond cut off time": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "SNR",
    },
    "pickup_on_hold_Non Serviceable Area": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "NSL",
    },
    "pickup_on_hold_Unable to pickup in given slot": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "NA",
    },
    "pickup_on_hold_Doorstep QC Used Item": {
        "scan_type": "PPF",
        "pickrr_sub_status_code": "OTH",
    },
    "on_hold_Incorrect/incomplete contact info.": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNA",
    },
    "on_hold_Pincode/address mismatch": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "AI",
    },
    "on_hold_Customer wants open delivery": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "OPDEL",
    },
    "on_hold_Customer shifted from given address": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "AI",
    },
    "on_hold_Non Serviceable Area": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "ODA",
    },
    "on_hold_Customer wants delivery on another address": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "AI",
    },
    "on_hold_High Volume Shipment": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "SD",
    },
    "on_hold_Three succesful attempts done": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "OTH",
    },
    "on_hold_Customer wants delivery beyond cut-off date": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CI",
    },
    "on_hold_Payment Issue": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNR",
    },
    "on_hold_High Ageing": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "OTH",
    },
    "on_hold_Successful attempts done": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "OTH",
    },
    "on_hold_COVID Restricted Area": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "REST",
    },
    "nc_Customer not contactable": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNA",
    },
    "nc_Residence or Office Closed": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "CNA",
    },
    "na_Not attempted": {"scan_type": "UD", "pickrr_sub_status_code": "SD"},
    "na_Vehicle Breakdown": {
        "scan_type": "UD",
        "pickrr_sub_status_code": "SD",
    },
    "na_Heavy Rain": {"scan_type": "UD", "pickrr_sub_status_code": "SD"},
}


def get_pikrr_response_shadowfax(response, waybill):
    tracking_dict = {}
    edd_stamp = None
    try:
        if str(response["message"]) == "Success":
            tracking_array = []
            from apps.common.services import ist_to_utc_converter

            for detail in response["tracking_details"]:
                track_details = {}
                if detail["status_id"] in [
                    "pickup_on_hold",
                    "on_hold",
                    "nc",
                    "na",
                ]:
                    mapper_string = (
                        str(detail["status_id"]) + "_" + str(detail["remarks"])
                    )
                    if sfx_mapper1.get(mapper_string) and sfx_mapper1[
                        mapper_string
                    ].get("scan_type"):
                        scan_type = sfx_mapper1[mapper_string]["scan_type"]
                    else:
                        continue
                    if (
                        NEW_STATUS_TO_OLD_MAPPING.get(scan_type, None)
                        is not None
                    ):
                        scan_type = NEW_STATUS_TO_OLD_MAPPING[scan_type]
                elif str(detail["status_id"]) in sfx_mapper2:
                    if sfx_mapper2.get(
                        str(detail["status_id"])
                    ) and sfx_mapper2[str(detail["status_id"])].get(
                        "scan_type"
                    ):
                        scan_type = sfx_mapper2[str(detail["status_id"])][
                            "scan_type"
                        ]
                    else:
                        continue
                    if (
                        NEW_STATUS_TO_OLD_MAPPING.get(scan_type, None)
                        is not None
                    ):
                        scan_type = NEW_STATUS_TO_OLD_MAPPING[scan_type]
                else:
                    continue
                track_details["scan_type"] = scan_type
                status_time = dateutil.parser.parse(
                    str(detail["created"])
                ).replace(tzinfo=None)
                status_time = ist_to_utc_converter(status_time)
                track_details["scan_datetime"] = status_time
                track_details["scan_status"] = (
                    str(detail["remarks"]) if detail["remarks"] else ""
                )
                track_details["scan_location"] = (
                    str(detail["location"]) if detail["location"] else ""
                )
                tracking_array.append(track_details.copy())
                if track_details["scan_type"] == "PP":
                    tracking_dict["pickup_datetime"] = status_time

            tracking_array.sort(key=lambda x: x["scan_datetime"], reverse=True)
            if len(tracking_array) > 0:
                tracking_dict["received_by"] = ""
                if tracking_array[0]["scan_type"] == "DL":
                    tracking_dict["received_by"] = (
                        str(
                            response["order_details"]["delivery_details"][
                                "name"
                            ]
                        )
                        if response["order_details"]["delivery_details"][
                            "name"
                        ]
                        else ""
                    )
                tracking_dict["status_date"] = tracking_array[0][
                    "scan_datetime"
                ]
                tracking_dict["status_location"] = tracking_array[0][
                    "scan_location"
                ]
                tracking_dict["status_info"] = tracking_array[0]["scan_status"]
                tracking_dict["status"] = tracking_array[0]["scan_type"]
                if tracking_array[0]["scan_type"] == "NDR":
                    tracking_dict["status_type"] = "OT"
                else:
                    tracking_dict["status_type"] = tracking_array[0][
                        "scan_type"
                    ]
                tracking_dict["tracking_array"] = tracking_array
                tracking_dict["edd_stamp"] = edd_stamp
                tracking_dict["awb"] = response["order_details"]["awb_number"]
        else:
            tracking_dict["err"] = str(response["message"])
        return tracking_dict
    except Exception as e:
        return {"err": str(e)}


def get_reverse_pikrr_response_shadowfax(response, waybill):
    tracking_dict = {}
    edd_stamp = None
    try:
        if (
            "client_order_number" in response
            and response["client_order_number"]
        ):
            tracking_array = []
            from apps.common.services import ist_to_utc_converter

            for detail in response["pickup_request_state_histories"]:
                track_details = {}
                if detail["state"] in shadowfax_reverse_bulk_code_mapper:
                    track_details[
                        "scan_type"
                    ] = shadowfax_reverse_bulk_code_mapper[
                        str(detail["state"])
                    ][
                        "scan_type"
                    ]
                else:
                    continue
                status_time = dateutil.parser.parse(
                    str(detail["created_at"])
                ).replace(tzinfo=None)
                status_time = ist_to_utc_converter(status_time)
                track_details["scan_datetime"] = status_time
                track_details["scan_status"] = (
                    str(detail["comment"]) if detail["comment"] else ""
                )
                track_details["scan_location"] = (
                    str(detail["current_location"])
                    if detail["current_location"]
                    else ""
                )
                tracking_array.append(track_details.copy())
                if track_details["scan_type"] == "PP":
                    tracking_dict["pickup_datetime"] = status_time
            tracking_array.sort(key=lambda x: x["scan_datetime"], reverse=True)
            if len(tracking_array) > 0:
                tracking_dict["received_by"] = ""
                if tracking_array[0]["scan_type"] == "DL":
                    tracking_dict["received_by"] = (
                        str(response["seller"]["name"])
                        if response["seller"]["name"]
                        else ""
                    )
                tracking_dict["status_date"] = tracking_array[0][
                    "scan_datetime"
                ]
                tracking_dict["status_location"] = tracking_array[0][
                    "scan_location"
                ]
                tracking_dict["status_info"] = tracking_array[0]["scan_status"]
                tracking_dict["status"] = tracking_array[0]["scan_type"]
                if tracking_array[0]["scan_type"] == "NDR":
                    tracking_dict["status_type"] = "OT"
                else:
                    tracking_dict["status_type"] = tracking_array[0][
                        "scan_type"
                    ]
                tracking_dict["tracking_array"] = tracking_array
                tracking_dict["edd_stamp"] = edd_stamp
                tracking_dict["awb"] = response["order_details"]["awb_number"]
        else:
            if "message" in response and response["message"]:
                tracking_dict["err"] = str(response["message"])
            elif "responseMsg" in response and response["responseMsg"]:
                tracking_dict["err"] = str(response["responseMsg"])
            else:
                tracking_dict["err"] = "Some error occured"
        return tracking_dict
    except Exception as e:
        return {"err": str(e)}
