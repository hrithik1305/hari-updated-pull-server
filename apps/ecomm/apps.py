from django.apps import AppConfig


class EcommConfig(AppConfig):
    name = "apps.ecomm"
